import React from "react";
import {URL, PORT} from '.././Config'

const HostingURLContext = React.createContext(`${URL}`);

export default HostingURLContext;
