import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { MdClose } from "react-icons/md";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
    outline: "none",
    border: "1px solid #eee",
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    // height: "223px",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    flexWrap: "wrap",
  },
}));

const SimpleModal = ({ handleClose, open }) => {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <div className="modal-header">
        <div className="add-item-styl">
          <AiOutlineExclamationCircle
            size={"1.2rem"}
            style={{ marginRight: "10px" }}
          />
          <span>Add New Item</span>
        </div>
        <MdClose
          onClick={handleClose}
          cursor="pointer"
          color="#FFF"
          size={"1.5rem"}
        />
      </div>
      <div>
        <div className="modal-content-styl">
          <div className="content-details-styl">
            <label className="required">SKU Name</label>
            <input placeholder="Enter Value" type="text" />
          </div>
          <div className="content-details-styl">
            <label className="required">SKU Nnumber</label>
            <input placeholder="Enter Value" type="text" />
          </div>

          <div className="content-details-styl">
            <label className="required">Stat Factor</label>
            <input placeholder="Enter Value" type="text" />
          </div>
          <div className="content-details-styl">
            <label className="required">Case Pack</label>
            <input placeholder="Enter Value" type="text" />
          </div>
        </div>
        <div className="modal-btn-styl">
          <button style={{ marginRight: "20px" }}>Save</button>
          <button>Save & Add</button>
        </div>
      </div>
    </div>
  );
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

export default SimpleModal;
