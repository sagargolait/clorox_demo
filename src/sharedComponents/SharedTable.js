import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import { useTable, useExpanded } from "react-table";

const useStyles = makeStyles({
  rowStyles: {
    "&:nth-child(3)": {
      borderBottom: "1px solid #DDDDDD",
    },
    "&:not(:nth-child(3))": {},
  },
});

const StyledTableCellBody2 = withStyles((theme) => ({
  root: {
    borderBottom: "1px solid #eee",
  },
  body: {
    fontSize: 14,
    fontFamily: "Lato",
    fontWeight: "700",
    lineHeight: "14px",
    color: "#000000",
    borderBottom: "none",
    "&:first-child": {
      fontSize: "14px",
      fontFamily: "Lato",
      fontWeight: "normal",
      borderRight: "1px solid #DDDDDD",
      lineHeight: "14px",
      textAlign: "bottom",
      color: "#666666",
    },
    "&:nth-child(2)": {
      fontSize: "14px",
      borderBottom: "none",
      background: "#F9F9F9",
      fontFamily: "Lato",
      borderRight: "1px solid #DDDDDD",
      fontWeight: "normal",
      lineHeight: "14px",
      color: "#666666",
    },
    "&:not(:nth-child(3))": {
      borderBottom: "none",
    },
  },
}))(TableCell);

const StyledTableCellBody1 = withStyles((theme) => ({
  root: {
    borderBottom: "1px solid #eee",
  },
  body: {
    fontSize: 14,
    fontFamily: "Lato",
    fontWeight: "700",
    lineHeight: "14px",
    color: "#000000",
    borderBottom: "none",
    "&:first-child": {
      fontSize: "14px",
      fontFamily: "Lato",
      fontWeight: "normal",
      borderRight: "1px solid #DDDDDD",
      lineHeight: "14px",
      textAlign: "bottom",
      color: "#666666",
    },
    "&:nth-child(2)": {
      fontSize: "14px",
      fontFamily: "Lato",
      borderRight: "1px solid #DDDDDD",
      fontWeight: "normal",
      lineHeight: "14px",
      color: "#666666",
    },
    "&:nth-child(3)": {
      fontSize: "14px",
      fontFamily: "Lato",
      borderRight: "1px solid #DDDDDD",
      fontWeight: "normal",
      lineHeight: "14px",
      color: "#666666",
    },
  },
}))(TableCell);

const StyledTableCellBody = withStyles((theme) => ({
  root: {
    borderBottom: "1px solid #eee",
  },
  body: {
    fontSize: 14,
    fontFamily: "Lato",
    fontWeight: "700",
    lineHeight: "14px",
    color: "#000000",
    "&:first-child": {
      fontSize: "14px",
      fontFamily: "Lato",
      fontWeight: "normal",
      lineHeight: "14px",
      color: "#666666",
    },
  },
}))(TableCell);

const SharedTable = ({
  columns,
  data,
  renderRowSubComponent,
  shipmentTable,
  financeTable,
}) => {
  const classes = useStyles();
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    visibleColumns,
    state: { expanded },
  } = useTable(
    {
      columns,
      data,
    },
    useExpanded
  );

  return (
    <div style={{ overflow: "auto" }}>
      <CssBaseline />
      <MaUTable
        style={{
          margin: "1rem",
          border: "1px solid",
          width: "97%",
        }}
        {...getTableProps()}
      >
        <TableHead style={{ background: "#f9f9f9" }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <TableCell {...column.getHeaderProps()}>
                  {column.render("header")}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <React.Fragment {...row.getRowProps()}>
                <tr className={classes.rowStyles}>
                  {row.cells.map((cell) => {
                    return shipmentTable ? (
                      <StyledTableCellBody1 {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </StyledTableCellBody1>
                    ) : financeTable ? (
                      <StyledTableCellBody2 {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </StyledTableCellBody2>
                    ) : (
                      <StyledTableCellBody {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </StyledTableCellBody>
                    );
                  })}
                </tr>
                {row.isExpanded ? (
                  <tr>
                    <StyledTableCellBody colSpan={visibleColumns.length}>
                      {renderRowSubComponent({ row })}
                    </StyledTableCellBody>
                  </tr>
                ) : null}
              </React.Fragment>
            );
          })}
        </TableBody>
      </MaUTable>
    </div>
  );
};

export default SharedTable;
