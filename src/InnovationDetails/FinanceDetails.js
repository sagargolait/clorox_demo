import React, { useState } from "react";
import { MdFileUpload, MdCloudDownload } from "react-icons/md";
import SharedTable from "../sharedComponents/SharedTable";
import { financeDetailsData } from "./FinanceDetailsData";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";

const FinanceDetails = () => {
  const [timePeriod, setTmePeriod] = useState(true);
  const [activeKeyMetric, setActiveKeyMetric] = React.useState(false);

  const columns = React.useMemo(
    () => [
      {
        id: "no-header",
        header: "",
        accessor: "title",
      },
      {
        id: "no-header1",
        header: "",
        accessor: "subtitle",
      },
      {
        header: "PY1",
        id: "no-header2",

        // accessor: "customer1",
      },
      {
        header: "PY2",
        id: "no-header3",

        // accessor: "customer2",
      },
      {
        header: "PY3",
        id: "no-header4",

        // accessor: "customer3",
      },
      {
        header: "FY1",
        id: "no-header5",

        // accessor: "customer4",
      },
      {
        header: "FY2",
        id: "no-header6",

        // accessor: "customer4",
      },
      {
        header: "FY3",
        id: "no-header7",

        // accessor: "customer4",
      },
      {
        header: "FY4",
        id: "no-header8",

        // accessor: "customer4",
      },
      {
        id: "total",
        header: "Total",
      },
    ],
    []
  );

  const data = financeDetailsData;

  return (
    <div className="financedetails-container">
      <div className="finance-details-topheader">
        <div className={"finance-details-btn"}>
          <button>Monthly</button>
          <button>Yearly</button>
        </div>
        <div className="product-details-header-btns">
          <button style={{ marginRight: "20px" }}>
            <MdCloudDownload size={"1.5rem"} style={{ marginRight: "10px" }} />
            Download Template
          </button>
          <button>
            <MdFileUpload style={{ marginRight: "10px" }} size={"1.5rem"} />
            Upload
          </button>
        </div>
      </div>
      <SharedTable columns={columns} data={data} financeTable />

      <div>
        <span>
          Key Metric Reco
          {activeKeyMetric ? (
            <MdKeyboardArrowUp
              style={{ marginLeft: "20px", cursor: "pointer" }}
              size={"1.5rem"}
              onClick={() => setActiveKeyMetric(!activeKeyMetric)}
            />
          ) : (
            <MdKeyboardArrowDown
              style={{ marginLeft: "20px", cursor: "pointer" }}
              size={"1.5rem"}
              onClick={() => {
                return setActiveKeyMetric(!activeKeyMetric);
              }}
            />
          )}
        </span>
      </div>
    </div>
  );
};

export default FinanceDetails;
