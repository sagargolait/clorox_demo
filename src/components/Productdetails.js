import React from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import SharedTable from "../sharedComponents/SharedTable";
import { PlmMappingData } from "../data/PlmMappingData";
import { SkuMappingData } from "../data/SkuMappingData";
import SimpleModal from "../sharedComponents/SimpleModal";
import {
  MdContentPaste,
  MdShare,
  MdFileUpload,
  MdCloudDownload,
  MdStore,
} from "react-icons/md";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
  },
  tabText: {
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "14px",
    lineHeight: "17px",
    letterSpacing: "0.03em",
    textTransform: "uppercase",
    color: "#005CB9",
  },
}));

const Productdetails = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const SKU_Columns = [
    {
      header: "SKU Name",
      accessor: "sku_name",
    },
    {
      header: "Stat Factor",
      accessor: "stat_factor",
    },
    {
      header: "Case Pack",
      accessor: "case_pack",
    },
    {
      header: "SKU Number",
      accessor: "sku_number",
    },
  ];

  const Plm_Mapping_columns = [
    {
      header: "Material Name",
      accessor: "material_name",
    },
    {
      header: "Material Number",
      accessor: "material_number",
    },
    {
      header: "Stat Factor",
      accessor: "stat_factor",
    },
    {
      header: "Case Pack",
      accessor: "case_pack",
    },
    {
      id: "custom",
      header: "SKU Name",
      Cell: () => {
        //Dropdown to be added instead of input tag
        return <input type="number" placeholder="Enter Value" />;
      },
    },
  ];

  return (
    <div>
      <Tabs
        value={value}
        onChange={handleChange}
        TabIndicatorProps={{ style: { border: "2px solid #005CB9" } }}
        textColor="inherit"
        variant="standard"
        aria-label="full width tabs example"
      >
        <Tab
          disableRipple
          className={classes.tabText}
          label="SKU"
          {...a11yProps(0)}
        />
        <Tab
          className={classes.tabText}
          label="PLM MApping"
          disableRipple
          {...a11yProps(1)}
        />

        <div className="product-details-header-btns">
          <button style={{ marginRight: "20px" }}>
            <MdCloudDownload size={"1.5rem"} style={{ marginRight: "10px" }} />
            Download Template
          </button>
          <button>
            <MdFileUpload style={{ marginRight: "10px" }} size={"1.5rem"} />
            Upload
          </button>
        </div>
      </Tabs>
      <TabPanel
        style={{
          background: "#FFFFFF",
          border: "1px solid #EFEFEF",
          boxSizing: "border-box",
        }}
        value={value}
        index={0}
        dir={theme.direction}
      >
        <SharedTable columns={SKU_Columns} data={SkuMappingData} />

        <button onClick={handleOpen} className="product-detail-add-btn">
          +
        </button>
        {open ? (
          <SimpleModal
            open={open}
            handleOpen={handleOpen}
            handleClose={handleClose}
          />
        ) : null}
      </TabPanel>
      <TabPanel
        style={{
          background: "#FFFFFF",
          border: "1px solid #EFEFEF",
          boxSizing: "border-box",
        }}
        value={value}
        index={1}
        dir={theme.direction}
      >
        <SharedTable columns={Plm_Mapping_columns} data={PlmMappingData} />
      </TabPanel>
    </div>
  );
};

export default Productdetails;
