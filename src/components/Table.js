import React, { useState, useEffect } from "react";
import { useTable, useExpanded } from "react-table";
// import customData from "./brand_data.json";
import {
  IoIosArrowUp,
  IoIosArrowDown,
  RiArrowUpDownFill,
} from "react-icons/io";
import {
  MdDelete,
  MdRemoveCircleOutline,
  MdSwapVert,
  MdReplyAll,
  MdInfo,
} from "react-icons/md";
import { BsFillReplyAllFill } from "react-icons/bs";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import * as Utils from "../utils/utils.js";
import "./Table.css";
import TableMessageBox from "./TableMessageBox.js";
import ReactTooltip from 'react-tooltip';

var head;
var someData = [];
var sumTotal = {};
var delta = {};
const useStyles = makeStyles({
  row: {
    borderRadius: "2px 2px 0px 0px",
    "&:first-child": {},
  },

  rightborder: {
    "&:first-child": {
      borderRight: "1px solid #DDDDDD",
    },
  },
});

const StyledTableCell = withStyles((theme) => ({
  root: {
    padding: "5px",
    borderBottom: "0px !important",
    "&:first-child": {
      borderRight: "1px solid #DDDDDD",
      border: "0",
      width: "25%",
    },
  },
  body: {
    fontSize: 14,
    fontFamily: "Lato",
    fontWeight: "590",
    lineHeight: "14px",
    color: "#000000",
    "&:first-child": {
      borderRight: "1px solid #A9A9A9",
      fontSize: "14px",
      fontFamily: "Lato",
      // fontWeight: "700",
      lineHeight: "14px",
      color: "#666666",
    },
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {},
}))(TableRow);

const EditableCell = ({
  value: initialValue,
  row,
  column: { id },
  updateMyData,
  ...props
}) => {
  // We need to keep and update the state of the cell normally
  const [value, setValue] = React.useState((initialValue / 1000).toFixed(0));

  const onChange = (e) => {
    setValue(e.target.value);
  };

  // We'll only update the external data when the input is blurred
  const onBlur = () => {
    //adding this if condition, because if the user remove 0 and then click outside then we again need to make it 0
    if (value == "") updateMyData(row.original.id, id, 0);
    else updateMyData(row.original.id, id, value * 1000);
  };

  let getID = (id) => {};
  return (
    <>
      <input
        type="text"
        className="editableTableInput1"
        value={value}
        style={{
          textAlign: "center",
          width: "40px",
          height: "28px",
          border: "1px solid #EFEFEF",
          boxSizing: "border-box",
          outline: 0,
          fontFamily: "Lato",
          fontWeight: "590",
          fontSize: "12px",
          color: "#000000",
          padding: 0,
          fontStyle: "italic",
        }}
        onChange={onChange}
        onBlur={onBlur}
        // disabled={row.original.isOpportunityRisk}
        onClick={(id) => {
          getID(id);
        }}
      />
    </>
  );
};

const defaultColumn = {
  Cell1: EditableCell,
};

const Table = ({ path, data, setData }) => {
  const [mypath, setMyPath] = useState(path);
  let [current_page_path_key, set_current_page_path_key] =
    useState("isOpportunityRisk");
  let [current_page_path_key2, set_current_page_path_key2] = useState("");
  const [open, setOpen] = React.useState(false);
  const [close, setClose] = React.useState(false);
  const [message, setMessage] = React.useState("");
  const [msgRowID, setMsgRowID]= React.useState("")

  useEffect(() => {
    setMyPath(path);
  }, [path]);

  useEffect(() => {
    console.log(path);
    if (path == "/planning/plans") {
      set_current_page_path_key("isOpportunityRisk");
      set_current_page_path_key2("");
    }
    if (path == "/planning/opportunity_risk") {
      set_current_page_path_key("isUpsideDown");
      set_current_page_path_key2("isOpportunityRisk");
    }
    if (path == "/planning/upside_downside") {
      set_current_page_path_key("isOpportunityRisk");
      set_current_page_path_key2("isUpsideDown");
    }
  }, [path]);

  const classes = useStyles();

  // const [data, setData] = useState(mydata);

  let testing_head = Object.keys(data[0]);
  testing_head = testing_head.filter((x) => {
    return (
      x != "id" &&
      x != "level" &&
      x != "isEditable" &&
      x != "isSaved" &&
      x != "isPublished" &&
      x != "subRows" &&
      x != "title" &&
      x != "Total"
    );
  });

  testing_head.unshift("title");
  testing_head[testing_head.length] = "Total";
  head = testing_head;

  // console.log(head);

  let columns = React.useMemo(
    () =>
      head.map((x, i) => {
        if (x == "title") {
          return {
            //   // Build our expander column
            id: "expander", // Make sure it has an ID
            Cell: ({ row }) =>
              row.canExpand ? (
                <span
                  className={"title_" + row.original.level}
                  title={""}
                  {...row.getToggleRowExpandedProps({
                    style: {
                      paddingLeft: `${row.depth}rem`,
                      fontFamily: "Lato",
                      // fontWeight: "700",
                      // fontStyle: "",
                      fontSize: "14px",
                      lineHeight: "16.8px",
                      color: "#666666",
                    },
                  })}
                >
                  {row.isExpanded ? (
                    <>
                      <IoIosArrowUp style={{ fontSize: "0.6rem" }} />{" "}
                      {`${row.original.title}`}
                    </>
                  ) : (
                    <>
                      <IoIosArrowDown style={{ fontSize: "0.6rem" }} />{" "}
                      {`${row.original.title}`}
                      {row.original.level != "version" ? (
                        <Tooltip
                          disableFocusListener
                          disableTouchListener
                          placement="right-start"
                          title={`${row.subRows.length - 1} comments`}
                        >
                          <span title={""} className="commentCount">
                            ℹ️
                          </span>
                        </Tooltip>
                      ) : null}
                    </>
                  )}
                </span>
              ) : //added this later when the 2nd row was like not expandable
              row.id != 1 ? (
                <>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      // justifyContent: "center",
                      paddingLeft: "40px",
                    }}
                  >
                    {row.original.isEditable === "button" ? (
                      <span
                        title={""}
                        id={row.original.id}
                        style={{
                          cursor: "pointer",
                          fontFamily: "Lato",
                          fontStyle: "semiBold",
                          fontWeight: "700",
                          fontSize: "14px",
                          lineHeight: "17px",
                          color: "#005CB9",
                          fontStyle: "italic",
                          paddingLeft: "12px",
                        }}
                        onClick={(e) => addComment(e, row)}
                        className={`AddComment_${mypath.split('/')[2]}`}
                      >
                        Add New Comments
                      </span>
                    ) : (
                      <>
                        {" "}
                        <Tooltip
                          title={`${row.original.title}`}
                          placement="top-end"
                        >
                          <input
                            value={row.original.title}
                            type="text"
                            placeholder={`${row.original.type} `}
                            style={{
                              textAlign: "left",
                              height: "28px",
                              boxSizing: "border-box",
                              marginRight: "10px",
                              marginLeft: "16px",
                              padding: "0.2em",
                              fontWeight: "590",
                              fontSize: "12px",
                              color: "#666666",
                              fontStyle: "italic",
                            }}
                            className="testingInput"
                            disabled={row.original.isSaved}
                            onChange={(e) => onChangeHandler(e, row)}
                            onBlur={(e) => {
                              onBlurHandler(e, row);
                            }}
                          />
                        </Tooltip>
                        {mypath == "/planning/plans" ? (
                          <>
                            <span
                              title={"Delete"}
                              style={{ cursor: "pointer" }}
                            >
                              <MdDelete
                                onClick={(e) => deleteRow(row.original)}
                                style={{ marginRight: "0px" }}
                                size={"1.2rem"}
                                color={"#005CB9"}
                              />
                            </span>{" "}
                            <span
                              title={"Move to Opportunity Risk"}
                              style={{ cursor: "pointer" }}
                            >
                              <BsFillReplyAllFill
                                style={{ marginLeft: "2px" }}
                                onClick={() =>
                                  PlanToOpportunityRisk(row.original)
                                }
                                size={"1.2rem"}
                                color={"#005CB9"}
                              />
                            </span>{" "}
                            <span
                              title={"Copy to Upside Downside"}
                              style={{ cursor: "pointer" }}
                            >
                              <MdSwapVert
                                style={{ marginLeft: "2px" }}
                                onClick={() =>{ setMsgRowID(row.original)
                                  setOpen(true) 
                                   }
                                  //PlanToUpsideDown(row.original)
                                }
                                size={"1.2rem"}
                                color={"#005CB9"}
                              />
                            </span>{" "}
                          </>
                        ) : mypath == "/planning/opportunity_risk" ? (
                          <>
                            <span
                              title={"Delete"}
                              style={{ cursor: "pointer" }}
                            >
                              <MdDelete
                                onClick={(e) => deleteRow(row.original)}
                                style={{ marginRight: "0px" }}
                                size={"1.2rem"}
                                color={"#005CB9"}
                              />
                            </span>{" "}
                            <span title={"Move"} style={{ cursor: "pointer" }}>
                              <BsFillReplyAllFill
                                style={{ marginLeft: "5px" }}
                                size={"1.2rem"}
                                color={"#005CB9"}
                                onClick={() => PlanToOpportunityRisk(row.original)}
                              />
                            </span>{" "}
                          </>
                        ) : mypath == "/planning/upside_downside" ? (
                          <>
                            <ReactTooltip id="tooltip_upsidedownside" place="right" type="dark" effect="solid"
                            multiline={true} />
                               
                            <span data-tip={`${row.original.message}`.replaceAll("\n","<br>")} data-for="tooltip_upsidedownside" style={{ cursor: "pointer" }}>
                              <MdInfo
                                // onClick={() => PlanToOpportunityRisk(row.original)}
                                size={"1.2rem"}
                                color={"#005CB9"}
                                // placeholder={row.original.message}
                              />
                            </span>
                            {""}
                            <span title={"Move"} style={{ cursor: "pointer" }}>
                              <MdSwapVert
                                style={{ marginLeft: "5px" }}
                                onClick={() =>{ setMsgRowID(row.original)
                                  setOpen(true) 
                                   }
                                  //PlanToUpsideDown(row.original)
                                }
                                size={"1.2rem"}
                                color={"#005CB9"}
                              />
                            </span>{" "}
                            {/* <span
                              title={"Delete"}
                              style={{ cursor: "pointer" }}
                            >
                              <MdDelete
                                onClick={(e) => deleteRow(row.original)}
                                style={{ marginRight: "5px" }}
                                size={"1.4rem"}
                                color={"#005CB9"}
                              />
                            </span>{" "} */}
                          </>
                        ) : (
                          <p>Loading...</p>
                        )}
                      </>
                    )}
                  </div>
                </>
              ) : (
                <span title={""}>{row.original.title}</span>
              ),
          };
        } else {
          return {
            Header: x.replace("_", " ").replace("FY22", "").replace("FY21", ""),
            accessor: x,
            Cell: ({ value }) =>
              Utils.numberWithCommas((Number(value) / 1000).toFixed(0)),
          };
        }
      }),
    [mypath]
  );

  const PlanToOpportunityRisk = (rowId) => {
    const newData = [...data];
    let isDisabledCount = 0;
    let parentID = rowId.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    let grand_parentID = parentID.split("_");
    grand_parentID = grand_parentID.slice(0, grand_parentID.length - 1);
    grand_parentID = grand_parentID.join("_");

    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });

    newData.forEach(function iter(a) {
      if (rowId.id == a.id) {
        a.isOpportunityRisk = !a.isOpportunityRisk;
        a["isEdited"] = true;
        console.log(a.isOpportunityRisk)
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    newData.forEach(function additionToParentRow(x) {
      if (x.id == parentID) {
        head.map((v) => {
          if (v != "title") {
            x[v] = Number(0);
          }
        });

        if (Array.isArray(x.subRows))
          x.subRows.map((cellsData, index) => {
            if (index != x.subRows.length - 1) {
              if (!cellsData[current_page_path_key] && !cellsData.isDeleted) {
                head.map((h) => {
                  if (h != "title") {
                    x[h] = Number(x[h]) + Number(cellsData[h]);
                  }
                });
              } else {
                isDisabledCount++;
              }
            }

            //making q1, q2, q3 as 0 if everything is deleted or disabled
            if (x.subRows.length - 1 == isDisabledCount) {
              head.map((h) => {
                if (h != "title") {
                  x[h] = Number(0);
                }
              });
            }
          });
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
    });

    //added newly for additon of drivers rows when the view by is Brand
    newData.forEach(function additionToParentRow(y) {
      if (y.level !== "version") {
        if (y.id == grand_parentID) {
          if (Array.isArray(y.subRows)) {
            head.map((columnId) => {
              if (columnId !== "title") {
                y[columnId] = Number(0);
                y.subRows.map((subdata, index) => {
                  if (index !== y.subRows.length) {
                    y[columnId] = y[columnId] + Number(subdata[columnId]);
                  }
                });
              }
            });
          }

          // subdata["Total"] = reqired_head.reduce((a, h) => {
          //   return a + Number(subdata[h]);
          // }, 0);
          // this is for parent Total

          // y["Total"] = reqired_head.reduce((a, h) => {
          //   return a + Number(y[h]);
          // }, 0);
          // })
          // });
        }
      }
      Array.isArray(y.subRows) && y.subRows.forEach(additionToParentRow);
    });

    setData(newData);
    reCalculate()
  };

  const PlanToUpsideDown = (rowId, msg) => {
    const newData = [...data];
    // setOpen(true)
    let isDisabledCount = 0;
    let parentID = rowId.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    let grand_parentID = parentID.split("_");
    grand_parentID = grand_parentID.slice(0, grand_parentID.length - 1);
    grand_parentID = grand_parentID.join("_");

    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });

    newData.forEach(function iter(a) {
      if (rowId.id == a.id) {
        a.isUpsideDown = !a.isUpsideDown;
        a["isEdited"] = true;
        a["message"] = msg;
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    newData.forEach(function additionToParentRow(x) {
      if (x.id == parentID) {
        head.map((v) => {
          if (v != "title") {
            x[v] = Number(0);
          }
        });

        if (Array.isArray(x.subRows))
          x.subRows.map((cellsData, index) => {
            if (index != x.subRows.length - 1) {
              if (!cellsData[current_page_path_key] && !cellsData.isDeleted) {
                head.map((h) => {
                  if (h != "title") {
                    x[h] = Number(x[h]) + Number(cellsData[h]);
                  }
                });
              } else {
                isDisabledCount++;
              }
            }

            //making q1, q2, q3 as 0 if everything is deleted or disabled
            if (x.subRows.length - 1 == isDisabledCount) {
              head.map((h) => {
                if (h != "title") {
                  x[h] = Number(0);
                }
              });
            }
          });
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
    });

    //added newly for additon of drivers rows when the view by is Brand
    newData.forEach(function additionToParentRow(y) {
      if (y.level !== "version") {
        if (y.id == grand_parentID) {
          if (Array.isArray(y.subRows)) {
            head.map((columnId) => {
              if (columnId !== "title") {
                y[columnId] = Number(0);
                y.subRows.map((subdata, index) => {
                  if (index !== y.subRows.length) {
                    y[columnId] = y[columnId] + Number(subdata[columnId]);
                  }
                });
              }
            });
          }

          // subdata["Total"] = reqired_head.reduce((a, h) => {
          //   return a + Number(subdata[h]);
          // }, 0);
          // this is for parent Total

          // y["Total"] = reqired_head.reduce((a, h) => {
          //   return a + Number(y[h]);
          // }, 0);
          // })
          // });
        }
      }
      Array.isArray(y.subRows) && y.subRows.forEach(additionToParentRow);
    });

    setData(newData);
    reCalculate()
  };

  const deleteRow = (rowId) => {
    const newData = [...data];

    let isDeletedCount = 0;
    let parentID = rowId.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    let grand_parentID = parentID.split("_");
    grand_parentID = grand_parentID.slice(0, grand_parentID.length - 1);
    grand_parentID = grand_parentID.join("_");

    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });

    newData.forEach(function iter(a) {
      if (rowId.id == a.id) {
        a.isDeleted = !a.isDeleted;
        a["isEdited"] = true;
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    newData.forEach(function additionToParentRow(x) {
      if (x.id == parentID) {
        head.map((v) => {
          if (v != "title") {
            x[v] = 0; //initializing each parent cell to 0
          }
        });

        if (Array.isArray(x.subRows))
          x.subRows.map((cellsData, index) => {
            //in cellData all the comments are coming
            //in x i hava parent data and in cellData we have subRows of it i.e x
            if (index != x.subRows.length - 1) {
              //not iterating for the last one as its a button (in the comment part only)

              if (!cellsData.isDeleted && !cellsData.isOpportunityRisk) {
                head.map((h) => {
                  if (h != "title") {
                    x[h] = Number(x[h]) + Number(cellsData[h]);
                  }
                });
              } else {
                isDeletedCount++;
              }
            }

            //making q1, q2, q3 as 0 if everything is deleted or disabled
            if (x.subRows.length - 1 == isDeletedCount) {
              head.map((h) => {
                if (h != "title") {
                  x[h] = Number(0);
                }
              });
            }
          });
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
    });

    //added newly for additon of drivers rows when the view by is Brand
    newData.forEach(function additionToParentRow(y) {
      if (y.level !== "version") {
        if (y.id == grand_parentID) {
          if (Array.isArray(y.subRows)) {
            head.map((columnId) => {
              if (columnId !== "title") {
                y[columnId] = Number(0);
                y.subRows.map((subdata, index) => {
                  if (index !== y.subRows.length) {
                    y[columnId] = y[columnId] + Number(subdata[columnId]);
                  }
                });
              }
            });
          }

          // subdata["Total"] = reqired_head.reduce((a, h) => {
          //   return a + Number(subdata[h]);
          // }, 0);
          // this is for parent Total

          // y["Total"] = reqired_head.reduce((a, h) => {
          //   return a + Number(y[h]);
          // }, 0);
          // })
          // });
        }
      }
      Array.isArray(y.subRows) && y.subRows.forEach(additionToParentRow);
    });

    setData(newData);
    reCalculate()
  };

  const updateMyData = (rowId, columnId, value) => {
    let parentID = rowId.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    let grand_parentID = parentID.split("_");
    grand_parentID = grand_parentID.slice(0, grand_parentID.length - 1);
    grand_parentID = grand_parentID.join("_");

    const newData = [...data];
    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });

    newData.forEach(function iter(a) {
      if (rowId == a.id) {
        if (value !== "") {
          a[columnId] = value;
          a["isEdited"] = true;
        }
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    //putting this condition of title as we dont want the title to get added according to comments
    if (columnId != "title") {
      newData.forEach(function additionToParentRow(x) {
        if (x.id == parentID) {
          x[columnId] = 0;

          if (Array.isArray(x.subRows))
            x.subRows.map((subdata, index) => {
              if (index !== x.subRows.length - 1) {
                //added new feature for upside down and opp risk
                if (mypath == "/planning/plans") {
                  if (!subdata[current_page_path_key] && !subdata.isDeleted) {
                    x[columnId] =
                      Number(x[columnId]) + Number(subdata[columnId]);
                  }
                } else {
                  if (subdata[current_page_path_key2] && !subdata.isDeleted) {
                    x[columnId] =
                      Number(x[columnId]) + Number(subdata[columnId]);
                  }
                }

                //end of new feature

                //old working feature
                // if (!subdata.isDeleted && !subdata.isOpportunityRisk)
                //   x[columnId] = Number(x[columnId]) + Number(subdata[columnId]);

                subdata["Total"] = reqired_head.reduce((a, h) => {
                  return a + Number(subdata[h]);
                }, 0);

                // subdata["Total"] =
                //   Number(subdata["Q1"]) +
                //   Number(subdata["Q2"]) +
                //   Number(subdata["Q3"]) +
                //   Number(subdata["Q4"]);
              }

              //this is for parent Total
              x["Total"] = reqired_head.reduce((a, h) => {
                return a + Number(x[h]);
              }, 0);
            });
        }
        Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
      });
    }

    if (columnId != "title") {
      //added newly for additon of drivers rows when the view by is Brand
      newData.forEach(function additionToParentRow(y) {
        if (y.level !== "version") {
          if (y.id == grand_parentID) {
            y[columnId] = Number(0);

            if (Array.isArray(y.subRows))
              y.subRows.map((subdata, index) => {
                if (index !== y.subRows.length) {
                  y[columnId] = y[columnId] + Number(subdata[columnId]);

                  subdata["Total"] = reqired_head.reduce((a, h) => {
                    return a + Number(subdata[h]);
                  }, 0);
                  // this is for parent Total

                  y["Total"] = reqired_head.reduce((a, h) => {
                    return a + Number(y[h]);
                  }, 0);
                }
              });
          }
        }
        Array.isArray(y.subRows) && y.subRows.forEach(additionToParentRow);
      });
    }
    setData(newData);
  };

  const addComment = (x, y) => {
    //if x.tagrget.id is "1_1_1_45"
    //then dd="1_1_1_46"
    let dd = x.target.id.split("_");
    dd[dd.length - 1] = String(Number(dd[dd.length - 1]) + 1);
    dd = dd.join("_");

    //if x.tagrget.id is "1_1_1_45"
    //then parentID = 1_1_1
    let parentID = x.target.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    let preserve_button = y.original;

    const newData = [...data];
    newData.forEach(function iter(a) {
      if (a.id == parentID) {
        if (Array.isArray(a.subRows))
          a.subRows.map((d) => {
            if (d.id == x.target.id) {
              // d.Q1 = "";
              // d.Q2 = "";
              // d.Q3 = "";
              // d.Q4 = "";
              // d.Total = "";

              head.map((h) => {
                d[h] = "";
                d[h] = "";
                d[h] = "";
                d[h] = "";
                d[h] = "";
                d[h] = "";
              });

              d.id = x.target.id;
              d.isDeleted = false;
              if (mypath == "/planning/opportunity_risk")
                d.isOpportunityRisk = true;
              if (mypath == "/planning/plans") d.isOpportunityRisk = false;
              d.isEditable = true;
              d.isEdited = true;
              d.isPublished = false;
              d.isSaved = false;
              d.level = y.original.level;
              d.parentTitle = y.original.parentTitle;
              d.type = "Comments";
            }
          });

        let dummy_obj = {
          id: dd,
          isDeleted: false,
          isOpportunityRisk: false,
          isEditable: "button",
          isEdited: false,
          isPublished: "",
          isSaved: "",
          level: y.original.level,
          parentTitle: y.original.parentTitle,
          type: "Comments",
        };

        head.map((h) => {
          if (h == "title") dummy_obj[h] = y.original.title;
          else {
            dummy_obj[h] = ".";
          }
        });

        a.subRows = [
          ...a.subRows,
          dummy_obj,
          //just commented these to make it dynamic
          // {
          //   "title": y.original.title,
          //   "Q1": ".",
          //   "Q2": ".",
          //   "Q3": ".",
          //   "Q4": ".",
          //   "Total": ".",

          //   "id": dd,
          //   "isDeleted": false,
          //   "isOpportunityRisk": false,
          //   "isEditable": "button",
          //   "isEdited": false,
          //   "isPublished": "",
          //   "isSaved": "",
          //   "level": y.original.level,
          //   "parentTitle": y.original.parentTitle,
          //   "type": "Comments",
          // }
        ];
      }
      setData([...data]);
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    setData([...data]);
  };

  let onChangeHandler = (m, n) => {
    updateMyData(n.original.id, "title", m.target.value);
  };

  let onBlurHandler = (m, n) => {
    // console.log(m.target.value)
    updateMyData(n.original.id, "title", m.target.value);
    //here we are sending "title" as a parameter because we want to update the value of title parameter in the main data
  };

  // let editedData = [];
  // //this function is getting called when the SAVE button is clicked
  // const onSave = () => {
  //   const newData = [...data];

  //   newData.forEach(function iter(a) {
  //     if (a.isEdited) {
  //       editedData.push(a);
  //     } else {
  //     }
  //     Array.isArray(a.subRows) && a.subRows.forEach(iter);
  //   });
  // };

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable(
      {
        columns,
        data,
        defaultColumn,
        updateMyData,
        addComment,
        autoResetExpanded: false,
        initialState: {
          expanded: { 0: true },
        },
      },
      useExpanded
    );

  //calculating delta and sum total
  let running_difference_temp = data[1];
  head.map((h) => {
    if (h !== "title") {
      sumTotal[h] = -Number(running_difference_temp[h]);
      delta[h] = Number(running_difference_temp[h]);
    }
  });

  // head.map((h) => {
  //   if(h!=="title")
  //   {
  //     delta[h] = running_difference_temp[h]
  //   }
  // });

  // console.log(sumTotal)
  // console.log(delta)

  let claculateRunningDifference = () => {
    head.map((head_data) => {
      if (head_data != "title") {
        data.forEach(function iter(a) {
          if (
            a.level == "version" ||
            (a.level == "driver" && !a.isOpportunityRisk && !a.isDeleted)
          ) {
            sumTotal[head_data] =
              Number(sumTotal[head_data]) + Number(a[head_data]);
            // delta[head_data] =
            // delta[head_data]-sumTotal[head_data]
          }
          Array.isArray(a.subRows) && a.subRows.forEach(iter);
        });
      }
    });
  };

  // console.log(sumTotal)
  // console.log(delta)

  let calculatePercentChange = () => {
    let d1 = data[0];
    let d2 = data[1];
    let temp = [];
    head.map((h) => {
      if (h == "title") temp.push("");
      else temp.push((d2[h] / d1[h]) * 100);
    });
    someData = temp;
  };

  let reCalculate=()=>{
    // console.log(current_page_path_key);
    const newData = [...data];
    let isDisabledCount = 0;
    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });


    let driver, brand, comment
    let isBrandDataAvailable=false
    newData.forEach(function additionToParentRow(x){
      if(x.level =="driver"){
        // driver=x
        if(Array.isArray(x.subRows)){
          head.map(h=>{
            if(h!=="title"){
              x[h]=0
              x.subRows.map((bc, bc_index)=>{
                
                if(bc.level =="brand"){
                  bc[h]=0
                  //brand level data
                  bc.subRows.map((c, index)=>{
                    //comment level data
                    // console.log(bc.subRows.length)
                    if(index < bc.subRows.length-1){
                        if(mypath==="/planning/plans") {
                          
                          if (
                              !c[current_page_path_key] &&
                              !c.isDeleted
                            ) {
                              bc[h]=bc[h]+Number(c[h])
                            }
                          } 

                          else {
                            if (
                                c[current_page_path_key2] &&
                                !c.isDeleted
                              ) {
                                bc[h]=bc[h]+Number(c[h])
                              }
                            }
                    }
                  })
                  x[h]=x[h]+Number(bc[h])
                }
    
                if(bc.level =="comment"){
                  //comment level data
                  if(bc_index < x.subRows.length-1){
                    if(mypath==="/planning/plans") {
                      if (
                          !bc[current_page_path_key] &&
                          !bc.isDeleted
                        ) {
                          x[h]=x[h]+Number(bc[h])
                        }
                      } 

                      else {
                        if (
                            bc[current_page_path_key2] &&
                            !bc.isDeleted
                          ) {
                            x[h]=x[h]+Number(bc[h])
                          }
                      }
                  }
                }
              })
            }
          })
         
        }
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
      setData(newData)
      
    })
  }
  useEffect(() => {
    // console.log(current_page_path_key);
    const newData = [...data];
    let isDisabledCount = 0;
    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });


    let driver, brand, comment
    let isBrandDataAvailable=false
    newData.forEach(function additionToParentRow(x){
      if(x.level =="driver"){
        // driver=x
        if(Array.isArray(x.subRows)){
          head.map(h=>{
            if(h!=="title"){
              x[h]=0
              x.subRows.map((bc, bc_index)=>{
                
                if(bc.level =="brand"){
                  bc[h]=0
                  //brand level data
                  bc.subRows.map((c, index)=>{
                    //comment level data
                    // console.log(bc.subRows.length)
                    if(index < bc.subRows.length-1){
                        if(mypath==="/planning/plans") {
                          
                          if (
                              !c[current_page_path_key] &&
                              !c.isDeleted
                            ) {
                              bc[h]=bc[h]+Number(c[h])
                            }
                          } 

                          else {
                            if (
                                c[current_page_path_key2] &&
                                !c.isDeleted
                              ) {
                                bc[h]=bc[h]+Number(c[h])
                              }
                            }
                    }
                  })
                  x[h]=x[h]+Number(bc[h])
                }
    
                if(bc.level =="comment"){
                  //comment level data
                  if(bc_index < x.subRows.length-1){
                    if(mypath==="/planning/plans") {
                      if (
                          !bc[current_page_path_key] &&
                          !bc.isDeleted
                        ) {
                          x[h]=x[h]+Number(bc[h])
                        }
                      } 

                      else {
                        if (
                            bc[current_page_path_key2] &&
                            !bc.isDeleted
                          ) {
                            x[h]=x[h]+Number(bc[h])
                          }
                      }
                  }
                }
              })
            }
          })
         
        }
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
      setData(newData)
      
    })
  }, [current_page_path_key, current_page_path_key2, mypath]);

  return (
    <>
      {claculateRunningDifference()}
      {calculatePercentChange()}
      {/* {CalculateSum()} */}
      <TableMessageBox
        open={open}
        setOpen={setOpen}
        close={close}
        setClose={setClose}
        setMessage={setMessage}
        PlanToUpsideDown={PlanToUpsideDown}
        msgRowID={msgRowID}
      />
      <MaUTable
        style={{
          overflowX: "auto",
          // marginTop:'0rem',
          margin: "0rem 1rem 1rem 1rem",
          // marginBottom:"1rem",
          border: "1px solid",
          width: "97%",
          border: "0 !important",
        }}
        {...getTableProps()}
      >
        <TableHead className="header">
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => {
                return (
                  <>
                    <TableCell {...column.getHeaderProps()}>
                      {column.render("Header")}
                    </TableCell>
                  </>
                );
              })}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <>
                <StyledTableRow
                  style={{ border: "0px !important" }}
                  className={row.original.level + "_" + path.split("/")[2]}
                  {...row.getRowProps()}
                >
                  {row.cells.map((cell, j) => {
                    //if the cell is diabled then we dont have to show in the UI
                    // same for isDelete, but confirm it first

                    if (mypath == "/planning/plans") {
                      if (
                        cell.row.original.isOpportunityRisk ||
                        cell.row.original.isDeleted
                      ) {
                        return null;
                      } else if (
                        cell.row.original.isEditable == true &&
                        cell.value != undefined &&
                        // || cell.value == 0
                        j != row.cells.length - 1 //adding this condition for now showing i/p box on the Total cell
                      ) {
                        return (
                          <StyledTableCell
                            className={
                              row.index == 0 ? classes.rightborder : null
                            }
                            className={row.original.level}
                            {...cell.getCellProps()}
                          >
                            {cell.render("Cell1")}
                          </StyledTableCell>
                        );
                      } else if (cell.value == ".")
                        return (
                          <StyledTableCell
                            {...cell.getCellProps()}
                          ></StyledTableCell>
                        );
                      else
                        return (
                          <StyledTableCell
                            className={
                              row.index == 0 ? classes.rightborder : null
                            }
                            {...cell.getCellProps()}
                          >
                            {cell.render("Cell")}
                          </StyledTableCell>
                        );
                    }

                    if (mypath == "/planning/opportunity_risk") {
                      // console.log(data)
                      if (
                        cell.row.original.isDeleted ||
                        cell.row.original.isUpsideDown
                      ) {
                        return null;
                      }

                      if (
                        cell.row.original.isOpportunityRisk == true ||
                        cell.row.original.isOpportunityRisk == undefined
                      ) {
                        //this undefined condition is added to render the driver level data and the version level dats
                        if (
                          cell.row.original.isEditable == true &&
                          cell.value != undefined &&
                          // || cell.value == 0
                          j != row.cells.length - 1 //adding this condition for now showing i/p box on the Total cell
                        ) {
                          return (
                            <StyledTableCell
                              className={
                                row.index == 0 ? classes.rightborder : null
                              }
                              className={row.original.level}
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell1")}
                            </StyledTableCell>
                          );
                        } else if (
                          cell.value == "."
                          // ||
                          // row.original.level=="version" //this is added to hide the version level row in the UI
                        )
                          return (
                            <StyledTableCell
                              {...cell.getCellProps()}
                            ></StyledTableCell>
                          );
                        else {
                          return (
                            <StyledTableCell
                              className="OOOOOOOOO"
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell")}
                            </StyledTableCell>
                          );
                        }
                      } else {
                        if (
                          cell.row.original.isEditable == "button" &&
                          cell.value != "."
                        ) {
                          return (
                            <StyledTableCell
                              className="KKKKKKKKKKKK"
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell")}
                            </StyledTableCell>
                          );
                        }
                      }
                    }

                    if (mypath == "/planning/upside_downside") {
                      if (
                        cell.row.original.isDeleted ||
                        cell.row.original.isOpportunityRisk
                      ) {
                        return null;
                      }

                      if (
                        cell.row.original.isUpsideDown == true ||
                        cell.row.original.isUpsideDown == undefined
                      ) {
                        //this undefined condition is added to render the driver level data and the version level dats
                        if (
                          cell.row.original.isEditable == true &&
                          cell.value != undefined &&
                          // || cell.value == 0
                          j != row.cells.length - 1 //adding this condition for now showing i/p box on the Total cell
                        ) {
                          return (
                            <StyledTableCell
                              className={
                                row.index == 0 ? classes.rightborder : null
                              }
                              className={row.original.level}
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell1")}
                            </StyledTableCell>
                          );
                        } else if (cell.value == ".")
                          return (
                            <StyledTableCell
                              {...cell.getCellProps()}
                            ></StyledTableCell>
                          );
                        else
                          return (
                            <StyledTableCell
                              className={
                                row.index == 0 ? classes.rightborder : null
                              }
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell")}
                            </StyledTableCell>
                          );
                      }
                    }
                  })}
                </StyledTableRow>
              </>
            );
          })}

          {path == "/planning/plans" ? (
            <>
              <tr className="version2">
                {someData.map((h) => {
                  if (h == "") {
                    return (
                      <td style={{ borderRight: "1px solid #A9A9A9" }}></td>
                    );
                  } else
                    return (
                      <td className={h < 100 ? "GiveRed" : "GiveGreen"}>
                        {Utils.numberWithCommas(h.toFixed(0))}
                      </td>
                    );
                })}
              </tr>
              <tr>
                {head.map((h) => {
                  if (h == "title") {
                    return <td className="DeltaRow DeltaRow1">Delta</td>;
                  } else
                    return (
                      <td className="DeltaRow1 DeltaRowCell">
                        {Utils.numberWithCommas(
                          ((sumTotal[h] - delta[h]) / 1000).toFixed(0)
                        )}
                      </td>
                    );
                })}
              </tr>
              <tr>
                {head.map((h) => {
                  if (h == "title") {
                    return <td className="DeltaRow">Sub Total</td>;
                  } else
                    return (
                      <td className="DeltaRowCell">
                        {Utils.numberWithCommas(
                          (sumTotal[h] / 1000).toFixed(0)
                        )}
                      </td>
                    );
                })}
              </tr>
            </>
          ) : null}
        </TableBody>
      </MaUTable>
    </>
  );
};

export default Table;
