import React, { useState } from "react";
import "./Chart.css";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Cell,
  ResponsiveContainer,
  Text,
  LabelList,
} from "recharts";
import { numberWithCommas } from "../../utils/utils";
import CustomTooltipWaterfall from "../../sharedComponents/CustomTooltipWaterfall";
import './waterfall.css'
 
var temp
var last_bar_value
const WaterfallChart = ({ data, aspectRatio, divideBy }) => {
  temp=divideBy
  data.map((d,i)=>{
    if(i==data.length-1)
    last_bar_value=d
  })
  return (
    <ResponsiveContainer width="99%" aspect={aspectRatio}>
      <BarChart
        data={data}
        margin={{
          top: 20,
          right: 10,
          left: -50,
          bottom: -35,
        }}
      >
        <CartesianGrid vertical={false} stroke="#efefef" />
        <XAxis
          dataKey="label"
          interval={0}
          tickLine={false}
          tick={CustomizedAxisTick}
          axisLine={false}
          height={90}
        />
        <YAxis
          type="number"
          axisLine={false}
          height={100}
          tickLine={false}
          scale={"linear"}
          tick={{ fill: "transparent", strokeWidth: 0 }}
          domain={[0, (dataMax) => dataMax * 1.5]}
        />

        {/* <Tooltip  cursor={false} /> */}
        {/* wrapperStyle={{ top: -150, left: 200 }}  */}
        {/* position={{ y: -80 }}  */}
        {/* position={{ y: -50 }} */}
        {/* wrapperStyle={{ top: -80, left: 1 }} */}
        <Bar dataKey="pv" stackId="a" barSize={25} fill="transparent" />
        <Tooltip position={{ y: -40 }} cursor={false} content={<CustomTooltip />} />

        <Bar dataKey="uv" stackId="a" fill="#82ca9d" barSize={30} >
          <LabelList
            dataKey="uv"
            content={(divideBy)=>renderCustomizedLabel(divideBy)}
            position="top"
            style={{ fontSize: "0.7em" }}
          />

          {data.map((item, index) => {
            if (item.uv <= 0) {
              return <Cell key={index} fill="#ED6318" />;
            }
            if (index === 0 || index === data.length - 1) {
              return <Cell key={index} fill="#0AA2D1" />;
            } else {
              return <Cell key={index} fill="#15CF6B" />;
            }
          })}
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  );
};


const CustomizedAxisTick = (props) => {
  const { x, y, payload } = props;

  return (
    <Text
      style={{
        fontFamily: "Lato",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "0.71em",
        lineHeight: "11px",
        color: "#999999",
        opacity: "0.5",
        wordWrap: 'break-word'
      }}
      x={x}
      y={y}
      width={1}
      textAnchor="middle"
      verticalAnchor="start"
    >
      {payload.value ? payload.value.split('/').join(" "+"/").split("/ ").join(" "+"/"): "loading.."}
    </Text>
  );
};

const renderCustomizedLabel = (props) => {
  const { x, y, width, height, value } = props;

  const radius = 10;

  return (
    <g>
      <text
        x={x + width / 2}
        y={y - radius}
        style={{
          fontFamily: "Lato",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "0.7em",
          lineHeight: "11px",
          color: "#999999",
          opacity: "0.8",
        }}
        textAnchor="middle"
        dominantBaseline="middle"
      >
        {
          temp
          ?
         numberWithCommas((Number.parseInt(value/temp)))
         :
          numberWithCommas(value)
        }
      </text>
    </g>
  );
};

const getIntroOfPage = (label) => {
  if (label === 'Page A') {
    return "Page A is about men's clothing";
  }
  if (label === 'Page B') {
    return "Page B is about women's dress";
  }
  if (label === 'Page C') {
    return "Page C is about women's bag";
  }
  if (label === 'Page D') {
    return 'Page D is about household goods';
  }
  if (label === 'Page E') {
    return 'Page E is about food';
  }
  if (label === 'Page F') {
    return 'Page F is about baby food';
  }
  return '';
};

const CustomTooltip = ({ active, payload, label }) => {
  if (active && payload && payload.length) {
    let back = `${(payload["1"]["value"])}`
    let theme=""
    if(back<=0){
      theme="mybordercolor"+"ED6318"
    }
    else{
      theme="mybordercolor"+"15CF6B"
    }
    return (
      <div className={`mytooltip ${theme}`}>
        <span style={{textAlign:'center'}}>{label}</span>
        <p className="label" style={{textAlign:'center'}}>{`${((payload[1].value/last_bar_value.uv) * 100).toFixed(0)} %`}</p>
        {/* <p className="intro">{getIntroOfPage(label)}</p>
        <p className="desc">Anything you want can be displayed here.</p> */}
      </div>
    );
  }

  return null;
};


export default WaterfallChart;
