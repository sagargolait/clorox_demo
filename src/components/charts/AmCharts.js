import React, { useEffect, useRef } from "react";

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import amchartsData from "../../data/amchartsData.json";

let randomID = Math.random().toString(36).substring(7);

const AmCharts = ({ fullscreen }) => {
  const [data, setData] = React.useState(amchartsData);
  const ref = useRef();

  console.log(data);
  let data2 = data;
  var data1;

  if (data2 !== null && data2 !== undefined && data2.length > 0) {
    data1 = JSON.parse(JSON.stringify(data));
    for (var i = 0; i < data1.length; i++) {
      data1[i].percent_change =
        i === 0 ? "-" : data1[i].percent_change.toFixed(2);
      data1[i].short_label = data1[i].label.slice(0, 3) + "...";

      //rounding data based on the display unit selected in the side filters

      data1[i].displayValue = Math.round(data1[i].values);

      data1[i].values =
        (i === 0) | (i === data1.length - 1)
          ? data1[i].values
          : data1[i].values + data1[i - 1].values;

      data1[i].open =
        (i === 0) | (i === data1.length - 1) ? 0 : data1[i - 1].values;
      // stepValue
      if (i != data1.length - 1) {
        data1[i].stepValue = data1[i].values;
      }
      // columnColor
      data1[i].columnColor =
        i === 0 || i === data1.length - 1
          ? "#0AA2D1"
          : data1[i].values < data1[i].open
          ? "#ED6318"
          : "#15CF6B";
    }
  }

  useEffect(() => {
    if (!ref.current) {
      return;
    }
    am4core.useTheme(am4themes_animated);

    //creating chart
    let chart = am4core.create(randomID, am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

    //data format based on the display unit selected in the side filters

    chart.numberFormatter.numberFormat = "#.#a";

    //add data
    chart.data = data;
    chart.cursor = new am4charts.Cursor();
    chart.cursor.behavior = "none";

    // Add category axis
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "label";
    categoryAxis.renderer.minGridDistance = 40;
    categoryAxis.renderer.labels.template.verticalCenter = "top";
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.labels.template.fill = am4core.color("grey");
    categoryAxis.tooltip.disabled = false;

    //Add label for category axis
    let label1 = categoryAxis.renderer.labels.template;
    label1.wrap = true;
    label1.maxWidth = 100;
    label1.fontSize = 10;
    label1.color = "grey";

    //Add tooltip for categoryaxis
    let axisTooltip = categoryAxis.tooltip;
    axisTooltip.background.fill = am4core.color("#ffffff");
    axisTooltip.label.fill = am4core.color("grey");
    axisTooltip.background.strokeWidth = 0;
    axisTooltip.background.cornerRadius = 3;
    axisTooltip.background.pointerLength = 0;
    axisTooltip.dy = 5;

    categoryAxis.adapter.add("getTooltipText", (text, target, key) => {
      return target._tooltipDataItem._dataContext.label;
    });

    categoryAxis.renderer.labels.template.adapter.add(
      "text",
      (text, target, key) => {
        if (target._dataItem.dataContext !== undefined) {
          return target._dataItem.dataContext.label + "\n\n\n" + "[#ffffff]hi";
        }
      }
    );

    // Add value axis
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;

    valueAxis.numberFormatter = new am4core.NumberFormatter();
    valueAxis.numberFormatter.numberFormat = "#.## a";
    valueAxis.renderer.grid.template.disabled = false;
    valueAxis.extraMax = 0.1;
    valueAxis.renderer.labels.template.disabled = true;

    //Add column series
    let columnSeries = chart.series.push(new am4charts.ColumnSeries());
    columnSeries.dataFields.categoryX = "label";
    columnSeries.dataFields.valueY = "values";
    columnSeries.dataFields.openValueY = "open";
    columnSeries.tooltipText = "[grey]{percent_change}%";
    columnSeries.tooltip.label.adapter.add("text", function (text, target) {
      if (target.dataItem && target.dataItem.categories.categoryX === "YAGO") {
        return "";
      } else {
        return text;
      }
    });
    columnSeries.fillOpacity = 0.8;
    columnSeries.sequencedInterpolation = true;
    columnSeries.interpolationDuration = 1500;

    //Tooltip for column series
    columnSeries.tooltip.getFillFromObject = false;
    columnSeries.tooltip.background.fill = am4core.color("#fff");
    columnSeries.tooltip.background.stroke = "{columnColor}";
    columnSeries.tooltip.background.strokeWidth = 1;

    let columnTemplate = columnSeries.columns.template;
    columnTemplate.strokeOpacity = 0;
    columnTemplate.propertyFields.fill = "columnColor";

    //Add label
    let label = columnTemplate.createChild(am4core.Label);
    label.text = "{displayValue}";
    label.align = "center";
    label.valign = "top";
    label.fontSize = 12;
    label.dy = -15;

    columnSeries.columns.template.events.on(
      "hit",
      function (ev) {
        window.sessionStorage.setItem(
          "driver",
          ev.target._dataItem.dataContext.label
        );

        window.sessionStorage.setItem("tab", "Driver Analytics");
      },
      this
    );

    columnSeries.columns.template.cursorOverStyle =
      am4core.MouseCursorStyle.pointer;

    //Add exporting menu
    chart.exporting.menu = new am4core.ExportMenu();
    // chart.exporting.menu.items[0].icon = arrow;
    chart.exporting.filePrefix = "Variance Decomp";

    //return chart
    return () => {
      chart.dispose();
    };
  }, [data]);

  return (
    <div
      id={fullscreen ? "fullscreen_chart" : "chart"}
      ref={ref}
      style={{ height: "100%" }}
    >
      <div
        id={randomID}
        style={{
          width: fullscreen ? "100vw" : "100%",
          position: fullscreen ? "fixed" : "static",
          top: 0,
          left: 0,
          zIndex: fullscreen ? 1110000 : 0,
          backgroundColor: "white",
          height: fullscreen ? "100vh" : "100%",
          padding: "0%",
        }}
      ></div>
    </div>
  );
};

export default AmCharts;
