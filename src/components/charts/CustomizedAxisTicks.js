import React from "react";

const CustomizedAxisTicks = (props) => {
  console.log(props);
  return (
    <div>
      <g transform={`translate(${props.x},${props.y})`}>
        <text x={-10} y={30} textAnchor="start" fill="#666">
          {props.payload.value}
        </text>
      </g>
    </div>
  );
};

export default CustomizedAxisTicks;
