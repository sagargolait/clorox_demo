import React, { useRef, useState, useEffect, useContext } from "react";
import UserFilters from "../../contexts/UserFilters";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

const ConsumptionAndShipmentChart = (props) => {
  const ref = useRef();
  const [FullScreenView, setFullScreenView] = useState(false);
  const _filters = useContext(UserFilters)["fil"];

  let data2 = props.data;

  var data;

  if (data2 !== null && data2 !== undefined && data2.length > 0) {
    data = JSON.parse(JSON.stringify(props.data));
    data.map((a, index) => {
      a.index = index;
      //columnColor
      a.columnColorShipment = a.flag === 0 ? "#B3EAEA" : "#0AA2D1";
      a.lineDashConsumption = a.flag === 0 ? "5,5" : "";
      //round data based on the display unit selected in the side filters
      if (_filters["display_unit"] === "MSC") {
        a["last_year_shipment"] = Math.round(a["last_year_shipment"] / 1000);
        a["shipment"] = Math.round(a["shipment"] / 1000);
        a["last_year_consumption"] = Math.round(
          a["last_year_consumption"] / 1000
        );
        a["consumption"] = Math.round(a["consumption"] / 1000);
      }

      a["shipment_iya"] = Math.round(a["shipment_iya"]);
      a["consumption_iya"] = Math.round(a["consumption_iya"]);
      a["last_year_consumption_color"] = "#E0E0E0";
      a["this_year_consumption_color"] = a.flag === 1 ? "#0AA2D1" : "#B3EAEA";
      a.dummy = 1;
    });
  }
  useEffect(() => {
    if (!ref.current) {
      return;
    }
    //Create chart
    let chart = am4core.create(ref.current, am4charts.XYChart);
    //Add data
    chart.data = data;
    //format data based on the display unit selected in the side filters
    if (_filters["display_unit"] === "MSC") {
      chart.numberFormatter.numberFormat = "#,###.";
    } else {
      chart.numberFormatter.numberFormat = "#.#a";
    }
    //Create category axis
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "calendar_view";
    categoryAxis.renderer.minGridDistance = 10;

    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.labels.template.fill = am4core.color("#999999");

    categoryAxis.renderer.cellStartLocation = 0.2;
    categoryAxis.renderer.cellEndLocation = 0.8;
    categoryAxis.tooltip.disabled = true;

    //Add label for category axis
    let label1 = categoryAxis.renderer.labels.template;
    label1.wrap = true;
    label1.maxWidth = 50;
    label1.fontSize = 10;
    label1.color = "grey";

    //Create value axis
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.labels.template.fill = am4core.color("#999999");
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.grid.template.disabled = false;
    valueAxis.fontSize = 10;
    valueAxis.numberFormatter = new am4core.NumberFormatter();

    //format data based on the display unit selected in the side filters
    if (_filters["display_unit"] === "MSC") {
      valueAxis.numberFormatter.numberFormat = "#,###.";
    } else {
      valueAxis.numberFormatter.numberFormat = "######.#a";
    }
    valueAxis.title.text = "[grey]Volume";

    var tooltip =
      "[#0AA2D1]Shipment This Year: {shipment}[/]\n[#64CDED]Shipment Last Year:: {last_year_shipment}[/]\n[{this_year_consumption_color}]Consumption This Year: {consumption}[/]\n[{last_year_consumption_color}]Consumption Last Year: {last_year_consumption}[/]";

    //Create line series
    var lineSeries1 = chart.series.push(new am4charts.LineSeries());
    lineSeries1.name = "Shipment Last Year";
    lineSeries1.dataFields.valueY = "last_year_shipment";
    lineSeries1.dataFields.categoryX = "calendar_view";
    lineSeries1.strokeWidth = 3;
    lineSeries1.tooltipText =
      "[bold #E0E0E0]{name}: {valueY} [bold grey] |{shipment_iya}";
    lineSeries1.fill = "#B7E1EE";
    lineSeries1.stroke = "#B7E1EE";
    lineSeries1.tooltip.pointerOrientation = "down";

    //Add Tooltip for series
    lineSeries1.tooltip.getFillFromObject = false;
    lineSeries1.tooltip.background.fill = am4core.color("#fff");
    lineSeries1.tooltip.background.stroke = "#E0E0E0";
    lineSeries1.tooltip.background.strokeWidth = 1;

    //Create line series
    var lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.name = "Shipment This Year ";
    lineSeries.dataFields.valueY = "shipment";
    lineSeries.dataFields.categoryX = "calendar_view";
    lineSeries.propertyFields.strokeDasharray = "lineDashConsumption";
    lineSeries.strokeWidth = 3;
    lineSeries.tooltipText =
      "[bold #0AA2D1]{name}: {valueY}[bold grey] | {shipment_iya}";
    lineSeries.tooltip.pointerOrientation = "down";
    lineSeries.fill = "#0AA2D1";
    lineSeries.stroke = "#0AA2D1";

    //Add Tooltip for series
    lineSeries.tooltip.getFillFromObject = false;
    lineSeries.tooltip.background.fill = am4core.color("#fff");
    lineSeries.tooltip.background.stroke = "{columnColor}";
    lineSeries.tooltip.background.strokeWidth = 1;

    //Create line series
    var lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.name = "Consumption This Year ";
    lineSeries.dataFields.valueY = "consumption";
    lineSeries.dataFields.categoryX = "calendar_view";
    lineSeries.strokeWidth = 3;
    lineSeries.tooltipText =
      "[bold #0AA2D1]{name}: {valueY}[bold grey] | {shipment_iya}";
    lineSeries.tooltip.pointerOrientation = "down";
    lineSeries.fill = "#00716E";
    lineSeries.stroke = "#00716E";

    //Add Tooltip for series
    lineSeries.tooltip.getFillFromObject = false;
    lineSeries.tooltip.background.fill = am4core.color("#fff");
    lineSeries.tooltip.background.stroke = "{columnColor}";
    lineSeries.tooltip.background.strokeWidth = 1;

    //Create line series
    var lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.name = "Consumption Last Year ";
    lineSeries.dataFields.valueY = "last_year_consumption";
    lineSeries.dataFields.categoryX = "calendar_view";
    lineSeries.strokeWidth = 3;
    lineSeries.tooltipText =
      "[bold #0AA2D1]{name}: {valueY}[bold grey] | {shipment_iya}";
    lineSeries.tooltip.pointerOrientation = "down";
    lineSeries.fill = "rgba(0, 113, 110, 0.5)";
    lineSeries.stroke = "rgba(0, 113, 110, 0.5)";

    //Add Tooltip for series
    lineSeries.tooltip.getFillFromObject = false;
    lineSeries.tooltip.background.fill = am4core.color("#fff");
    lineSeries.tooltip.background.stroke = "{columnColor}";
    lineSeries.tooltip.background.strokeWidth = 1;

    //Create value axis
    let valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis1.renderer.labels.template.fill = am4core.color("#999999");
    valueAxis1.tooltip.disabled = true;
    valueAxis1.renderer.grid.template.disabled = false;

    //Add exporting menu
    chart.exporting.menu = new am4core.ExportMenu();

    //Create column series
    let series1 = chart.series.push(new am4charts.ColumnSeries());
    series1.dataFields.valueY = "consumption";
    series1.dataFields.categoryX = "calendar_view";
    series1.name = "Shipment vs. YA Index ";
    series1.columns.template.fillOpacity = 0.8;
    series1.sequencedInterpolation = "true";
    series1.tooltip.pointerOrientation = "down";
    series1.fill = "#0AA2D1";
    series1.columns.template.tooltipText =
      "[bold #0AA2D1]{name}: {valueY}[bold grey] | {consumption_iya}";

    //Add column template
    let columnTemplate1 = series1.columns.template;
    columnTemplate1.strokeWidth = 0;
    columnTemplate1.strokeOpacity = 0;
    columnTemplate1.propertyFields.fill = "this_year_consumption_color";
    columnTemplate1.width = am4core.percent(10);

    //Add Tooltip for series
    series1.tooltip.getFillFromObject = false;
    series1.tooltip.background.fill = am4core.color("#fff");
    series1.tooltip.background.stroke = "{columnColor}";
    series1.tooltip.background.strokeWidth = 1;

    //Add exporting menu
    chart.exporting.menu = new am4core.ExportMenu();
    // chart.exporting.menu.items[0].icon = arrow;
    chart.exporting.filePrefix = "Consumption/Shipment";

    //Add Scroll bar to the chart
    chart.scrollbarX = new am4core.Scrollbar();
    chart.scrollbarX.start = 0;
    chart.scrollbarX.end = 1;
    chart.scrollbarX.parent = chart.bottomAxesContainer;

    //Add Legend to the chart
    chart.legend = new am4charts.Legend();
    chart.legend.labels.template.fill = am4core.color("#999999");
    chart.legend.position = "top";
    chart.legend.fontSize = 15;
    chart.legend.itemContainers.template.clickable = true;
    chart.legend.itemContainers.template.focusable = true;
    chart.legend.itemContainers.template.cursorOverStyle =
      am4core.MouseCursorStyle.default;

    //Return Chart
    return () => {
      chart.dispose();
    };
  }, [props.data, data]);
  //Display Message while data is still loading
  var cont;
  if (props.data === null || props.data === undefined) {
    return <h1>Loading</h1>;
  }
  //Display message when there is no data
  else if (props.data.length < 1) {
    cont = (
      <h5 style={{ textAlign: "center", padding: "10px", opacity: "0.7" }}>
        No Data
      </h5>
    );
  } else {
    cont = null;
  }

  return (
    <div style={{ position: "relative" }}>
      {cont}

      <div
        id={FullScreenView ? "fullscreen_CandSchart" : "CandSchart"}
        ref={ref}
        style={{
          width: FullScreenView ? "100vw" : "100%",
          position: FullScreenView ? "fixed" : "static",
          top: 0,
          left: 0,
          zIndex: FullScreenView ? 100 : 0,
          backgroundColor: "white",
          height: FullScreenView ? "100vh" : "300px",
          padding: "0%",
        }}
      ></div>
    </div>
  );
};

export default ConsumptionAndShipmentChart;
