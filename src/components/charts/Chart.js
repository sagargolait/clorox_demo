import React, { useState } from "react";
import "./Chart.css";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Cell,
  CartesianAxis,
} from "recharts";
// import Chartsdata from "./Chartsdata.json";
// import CustomizedAxisTicks from "./CustomizedAxisTicks";
import { numberWithCommas } from "../../utils/utils";

const Chart = () => {
  const data = [
    {
      label: "April Cycle",
      uv: 49958653,
      pv: 0,
    },
    {
      label: "Merch/Promo",
      uv: 1145082,
      pv: 49958653,
    },
    {
      label: "Base Trend",
      uv: 725837,
      pv: 51103735,
    },
    {
      label: "Spending",
      uv: 680050,
      pv: 51829572,
    },
    {
      label: "New Products",
      uv: 605424,
      pv: 52509622,
    },
    {
      label: "Assortment",
      uv: 19779,
      pv: 53115046,
    },
    {
      label: "Pricing",
      uv: 0,
      pv: 53115046,
    },
    {
      label: "Competition",
      uv: -200000,
      pv: 52915046,
    },
    {
      label: "Other",
      uv: -2166084,
      pv: 50748962,
    },
    {
      label: "FY22 LRP",
      uv: 50768301,
      pv: 0,
    },
  ];
  return (
    <div className="chart">
      <span>VARIANCE DECOMPOSITION</span>
      <BarChart
        width={1123}
        height={374}
        data={data}
        margin={{
          top: 20,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid vertical={false} />
        <XAxis
          dataKey="label"
          interval={0}
          tickLine={false}
          axisLine={false}
          // tick={<CustomizedAxisTicks />}
        />
        <YAxis
          type="number"
          axisLine={false}
          tickLine={false}
          scale={"linear"}
          tick={{ fill: "transparent", strokeWidth: 0 }}
          domain={[0, "dataMax + 20000000"]}
        />
        <Tooltip cursor={false} />
        <Bar dataKey="pv" stackId="a" barSize={40} fill="transparent" />

        <Bar
          dataKey="uv"
          stackId="a"
          fill="#82ca9d"
          // label={<CustomizedLabel />}
        >
          {data.map((item, index) => {
            if (item.uv <= 0) {
              return <Cell key={index} fill="#ED6318" />;
            }
            if (index === 0 || index === data.length - 1) {
              return <Cell key={index} fill="#0AA2D1" />;
            } else {
              return <Cell key={index} fill="#15CF6B" />;
            }
          })}
        </Bar>
      </BarChart>
    </div>
  );
};

const CustomizedLabel = (props) => {
  console.log(props);
  return (
    <text
      x={props.x}
      y={props.y}
      dy={-20}
      dx={20}
      fill={props.stroke}
      fontSize={12}
      fontFamily="Lato"
      textAnchor="middle"
    >
      {numberWithCommas(props.value)}
    </text>
  );
};

export default Chart;
