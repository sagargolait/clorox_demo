import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Grid } from '@material-ui/core';

export default function TableMessageBox({open, setOpen, setMessage, PlanToUpsideDown, msgRowID}) {

    const [msg, setMsg] = useState("")

  const handleClose = () => {
    setMessage(msg)
    setOpen(false);
    console.log(msg)
    PlanToUpsideDown(msgRowID, msg)
  };

  let onChangeHandler=(e)=>{
    setMsg(e.target.value)
  }

  return (
    <div>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">MOVE COMMENT</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <small>Please Provide Reason *</small>
          </DialogContentText>
          <TextField
            onChange={onChangeHandler}
            required
            variant="outlined"
            rows={10}
            multiline
            autoFocus
            margin="dense"
            id="name"
            label="Enter Reason"
            type="message"
            fullWidth
            style={{height:'270px',width: '550px'}}
          />
        </DialogContent>

        <Grid container justify='center'>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button variant="contained" onClick={handleClose} color="primary">
            Move Comment
          </Button>
        </DialogActions>
        </Grid>
      </Dialog>
    </div>
  );
}
