import React, { useState } from "react";
import {
  MdFileUpload,
  MdKeyboardArrowDown,
  MdKeyboardArrowUp,
} from "react-icons/md";
import { IoIosArrowUp, IoIosArrowDown } from "react-icons/io";
import Select from "react-select";
import {
  VolumeDetailsData,
  VolumebuildData,
} from "../data/VolumeDetailsTableData";
import SharedTable from "../sharedComponents/SharedTable";

export const selectStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px solid #eeeeee",
    color: state.isSelected ? "#005CB9" : "",
    fontSize: 16,
    backgroundColor: state.isSelected ? "#eee" : "",
    textAlign: "left",
    cursor: "pointer",
  }),
  container: (base) => ({
    ...base,
    width: "150px",
    height: "44px",
  }),
  control: (base) => ({
    ...base,
    width: "150px",
    height: "44px",
    fontSize: 14,
    fontFamily: "Lato",
    fontStyle: "Normal",
    fontWeight: "900",
    borderRadius: 2,
    padding: 5,
    border: "1px solid #EFEFEF",
    boxSizing: "border-box",
    boxShadow:
      -"5px -5px 10px rgba(0, 80, 115, 0.02), 5px 5px 10px rgba(0, 80, 115, 0.02)",
    borderRadius: "2px",
    width: "100%",
    textAlign: "center",
    cursor: "pointer",
  }),
  dropdownIndicator: (base) => ({
    ...base,
    color: "#005CB9",
  }),
  indicatorSeparator: (base) => ({
    ...base,
    display: "none",
  }),
  singleValue: (base) => ({
    ...base,
    color: "#005CB9",
  }),
  valueContainer: (base) => ({
    ...base,
    padding: 0,
    width: "160px",
    paddingLeft: 2,
  }),
  menu: (base) => ({
    ...base,
    width: "180px",
    "&:last-child": {
      borderBottom: "none",
    },
  }),
};

const EditableCell = ({
  value: initialValue,
  row: { index },
  column: { id },
  updateMyData, // This is a custom function that we supplied to our table instance
}) => {
  // We need to keep and update the state of the cell normally
  const [value, setValue] = React.useState(initialValue);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  // We'll only update the external data when the input is blurred
  // const onBlur = () => {
  //   updateMyData(index, id, value);
  // };

  // If the initialValue is changed external, sync it up with our state
  React.useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  return (
    <input
      style={{
        width: "100px",
        height: "29px",
        border: "1px solid #EFEFEF",
        outline: "none",
      }}
      value={value}
      type="number"
      placeholder="Enter Value"
      onChange={onChange}
      // onBlur={onBlur}
    />
  );
};

const getTotals = (data, key) => {
  let total = 0;
  data.forEach((item) => {
    total += item[key];
  });
  return total;
};

const options = [
  {
    label: "Walmart",
    value: "Walmart",
  },
];

const VolumeDetails = () => {
  const [activeVolumebyCustomers, setActiveVolumebyCustomers] =
    React.useState(true);
  const [selectedOption, setSelectedOption] = useState(null);

  const [activeVolumeBuild, setActiveVolumeBuild] = React.useState(false);
  const [data, setData] = React.useState(VolumeDetailsData);

  const renderRowSubComponent = React.useCallback(
    ({ row }) => (
      <>
        <SharedTable columns={VolumebuildColumns} data={VolumebuildData} />
      </>
    ),
    []
  );

  React.useEffect(() => {
    setData((data) => [
      ...data,
      {
        title: "Total",
        customer1: getTotals(data, "customer1"),
        customer2: getTotals(data, "customer2"),
        customer3: getTotals(data, "customer3"),
        customer4: getTotals(data, "customer4"),
      },
    ]);
  }, []);

  const VolumebuildColumns = [
    {
      id: "expander", // It needs an ID
      header: "",
      Cell: ({ row }) => (
        <span {...row.getToggleRowExpandedProps()}>
          {row.isExpanded ? (
            <>
              {`${row.original.title}`}
              <IoIosArrowUp size={"1.2rem"} />
            </>
          ) : (
            <>
              {`${row.original.title}`}
              <IoIosArrowDown size={"1.2rem"} />
            </>
          )}
        </span>
      ),
    },
    {
      header: "Total Store Count",
      accessor: "total_score_count",
      Cell: EditableCell,
    },
    {
      header: "ACV",
      id: "acv",
      Cell: EditableCell,
    },
    {
      header: "Velocity (Unit/Store/Week)",
      id: "Velocity (Unit/Store/Week",
      Cell: EditableCell,
    },
    {
      header: "Cannibalised Item",
      id: "Cannibalised Item",
      Cell: EditableCell,
    },
    {
      header: "Cannib Rate",
      id: "Cannib Rate",
      Cell: EditableCell,
    },
    {
      header: "Pipefill Cases",
      id: "Pipefill Cases",
      Cell: EditableCell,
    },
    {
      header: "Pipefill Percentage",
      id: "Pipefill Percentage",
      Cell: EditableCell,
    },
  ];

  const updateMyData = (rowIndex, columnId, value) => {
    // We also turn on the flag to not reset the page
    setData((old) =>
      old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          };
        }
        return row;
      })
    );
  };

  const handleChange = (selectedOption) => {
    setSelectedOption({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };

  const columns = React.useMemo(
    () => [
      {
        id: "no-header",
        header: "",
        accessor: "title",
      },
      {
        header: "Customer1",
        accessor: "customer1",
      },
      {
        header: "Customer2",
        accessor: "customer2",
      },
      {
        header: "Customer3",
        accessor: "customer3",
      },
      {
        header: "Customer4",
        accessor: "customer4",
      },
      {
        id: "total",
        header: "Total",
        accessor: (row) =>
          [row.customer1, row.customer2, row.customer3, row.customer4].reduce(
            (sum, current) => sum + current,
            0
          ),
      },
    ],
    []
  );

  return (
    <div className="volume-details-container">
      <div
        style={{
          display: "flex",
          flexDirection: "row-reverse",
          color: "#005CB9",
          cursor: "pointer",
        }}
      >
        Upload
        <MdFileUpload size={"1.5rem"} style={{ marginRight: "10px" }} />
      </div>
      <div className="volume-details-content">
        <span>
          Volume By Customers
          {activeVolumebyCustomers ? (
            <MdKeyboardArrowUp
              style={{ marginLeft: "20px", cursor: "pointer" }}
              size={"1.5rem"}
              onClick={() =>
                setActiveVolumebyCustomers(!activeVolumebyCustomers)
              }
            />
          ) : (
            <MdKeyboardArrowDown
              size={"1.5rem"}
              style={{ marginLeft: "20px", cursor: "pointer" }}
              onClick={() =>
                setActiveVolumebyCustomers(!activeVolumebyCustomers)
              }
            />
          )}
        </span>
        {activeVolumebyCustomers ? (
          <div>
            <SharedTable columns={columns} data={data} />
          </div>
        ) : null}
      </div>
      <div>
        <span>
          Volume Build
          {activeVolumeBuild ? (
            <MdKeyboardArrowUp
              style={{ marginLeft: "20px", cursor: "pointer" }}
              size={"1.5rem"}
              onClick={() => setActiveVolumeBuild(!activeVolumeBuild)}
            />
          ) : (
            <MdKeyboardArrowDown
              style={{ marginLeft: "20px", cursor: "pointer" }}
              size={"1.5rem"}
              onClick={() => {
                return (
                  setActiveVolumeBuild(!activeVolumeBuild),
                  setActiveVolumebyCustomers(false)
                );
              }}
            />
          )}
        </span>
        {activeVolumeBuild ? (
          <div className="volumebuild-styl-tbl-container">
            <div style={{ display: "flex", alignItems: "center" }}>
              <label style={{ marginRight: "22px" }}>Select Customer</label>
              <Select
                onChange={handleChange}
                styles={selectStyles}
                defaultValue={options[0]}
                options={options}
              />
            </div>
            <div>
              <SharedTable
                columns={VolumebuildColumns}
                data={VolumebuildData}
                renderRowSubComponent={renderRowSubComponent}
              />
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default VolumeDetails;
