import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import {
  MdTrendingUp,
  MdPictureInPictureAlt,
  MdEqualizer,
  MdWatchLater,
} from "react-icons/md";
import NdfSummary from "./planningSummary/NdfSummary";
import CaseFill from "./planningSummary/CaseFill";
import KeyMetricsOvertime from "./planningSummary/KeyMetricsOvertime";
import ShipmentConsumption from "./planningSummary/ShipmentConsumption";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3} style={{ background: "#fff" }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabpanel: {
    "& .MuiBox-root": {
      padding: "8px 24px",
    },
  },
  tabs: {
    "& .MuiTab-wrapper": {
      fontFamily: "Lato",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "14px",
      lineHeight: "17px",
      textTransform: "capitalize",
      display: "flex",
      alignItem: "center",
      justifyContent: "center",
      flexDirection: "row",
    },
    "&:last-child": {
      borderRight: "none",
    },

    "&.MuiTab-labelIcon": {
      minHeight: "30px",
    },
    "&.Mui-selected": {
      background: "rgba(0, 92, 185, 0.1);",
    },
    "&.MuiPaper-elevation4": {
      boxShadow: "none",
    },
    "&.MuiTab-textColorInherit": {
      opacity: "1",
    },
  },
}));

const StyledTabs = withStyles(() => ({
  root: {
    backgroundColor: "#FFFFFF",
    fontSize: "14px",
    minHeight: "35px",
    color: "#005CB9",
    "& .MuiTabs-flexContainer": {
      height: "20px",
    },
    "& .MuiTabs-fixed": {
      height: "35px",
    },
  },
}))(Tabs);

const PlanningSummaryTabPanel = () => {
  const classes = useStyles();

  const [value, setValue] = React.useState(0);
  const [chartSelected, setChartSelected] = React.useState(true);
  const [tableSelected, setTableSelected] = React.useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar
        position="static"
        variant="dense"
        style={{
          borderBottom: "2px solid #eee",
          boxShadow:
            "-5px -5px 10px rgba(0, 80, 115, 0.02), 5px 5px 10px rgba(0, 80, 115, 0.02);",
        }}
      >
        <StyledTabs
          value={value}
          variant={"fullWidth"}
          onChange={handleChange}
          aria-label="nav tabs example"
          TabIndicatorProps={{
            style: { border: "2px solid #005CB9" },
          }}
        >
          <LinkTab
            className={classes.tabs}
            label="NDF Update"
            icon={
              <MdTrendingUp size={"1.5rem"} style={{ marginRight: "20px" }} />
            }
            {...a11yProps(0)}
          />
          <LinkTab
            className={classes.tabs}
            label="Shipment and Consumption"
            icon={
              <MdEqualizer size={"1.5rem"} style={{ marginRight: "20px" }} />
            }
            {...a11yProps(2)}
          />
          <LinkTab
            icon={
              <MdPictureInPictureAlt
                size={"1.5rem"}
                style={{ marginRight: "20px" }}
              />
            }
            className={classes.tabs}
            label="Case Fill"
            {...a11yProps(1)}
          />

          <LinkTab
            className={classes.tabs}
            label="Key Metrics Overtime"
            icon={
              <MdWatchLater size={"1.5rem"} style={{ marginRight: "20px" }} />
            }
            {...a11yProps(3)}
          />
        </StyledTabs>
      </AppBar>
      <TabPanel value={value} className={classes.tabpanel} index={0}>
        <NdfSummary
          tableSelected={tableSelected}
          setTableSelected={setTableSelected}
          chartSelected={chartSelected}
          setChartSelected={setChartSelected}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <ShipmentConsumption
          chartSelected={chartSelected}
          setChartSelected={setChartSelected}
          tableSelected={tableSelected}
          setTableSelected={setTableSelected}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <CaseFill />
      </TabPanel>

      <TabPanel value={value} index={3}>
        <KeyMetricsOvertime />
      </TabPanel>
    </div>
  );
};

export default PlanningSummaryTabPanel;
