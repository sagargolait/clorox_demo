import React from "react";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import CssBaseline from "@material-ui/core/CssBaseline";
import InnovationTable from "./InnovationTable";
import {
  MdContentPaste,
  MdShare,
  MdFileUpload,
  MdCloudDownload,
  MdFilterList,
} from "react-icons/md";
import { IoMdSearch } from "react-icons/io";
import history from "../utils/history";
import InnovationtableData from "./InnovationtableData.json";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const BlueCheckbox = withStyles({
  root: {
    color: "#005CB9",
    "&$checked": {
      color: "#005CB9",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles((theme) => ({
  FormControlLabel: {
    "& .MuiTypography-body1": {
      fontFamily: "Lato",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "14px",
      lineHeight: "17px",
      letterSpacing: "0.06em",
      color: "#333333",
    },
  },
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    "& .MuiPaper-elevation4": {
      boxShadow: "none",
    },
  },
  tabs: {
    "& .MuiTab-wrapper": {
      fontFamily: "Lato",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "14px",
      lineHeight: "17px",
      textTransform: "capitalize",
      display: "flex",
      alignItem: "center",
      justifyContent: "center",
      flexDirection: "row",
    },
    "&:last-child": {
      borderRight: "none",
    },

    "&.MuiTab-labelIcon": {
      minHeight: "30px",
    },
    "&.Mui-selected": {
      background: "rgba(0, 92, 185, 0.1);",
    },
    "&.MuiPaper-elevation4": {
      boxShadow: "none",
    },
    "&.MuiTab-textColorInherit": {
      opacity: "1",
    },
  },
}));

const InnovationTabPanel = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  function SelectColumnFilter({ column: { filterValue, setFilter } }) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    const [state, setState] = React.useState({
      checkedA: true,
      checkedB: true,
      checkedF: true,
      checkedG: true,
    });

    const handleChange = (event) => {
      setState({ ...state, [event.target.name]: event.target.checked });
      setFilter(event.target.name);
    };
    return (
      <div style={{ position: "relative" }}>
        <MdFilterList
          style={{ marginLeft: "10px", cursor: "pointer" }}
          onClick={handleClick}
          value={filterValue}
          onChange={(e) => {
            setFilter(e.target.value || undefined);
          }}
        ></MdFilterList>
        <div>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            style={{ top: "50px" }}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            transformOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <input
              style={{
                margin: "0.5rem",
                width: "90%",
                height: "28px",
                outline: "none",
                paddingLeft: "30px",
                background: "#FFFFFF",
                border: "1px solid #EFEFEF",
                boxSizing: "border-box",
                borderRadius: "4px",
              }}
              type="text"
              placeholder="Search"
            />

            <IoMdSearch
              style={{ position: "absolute", left: "17px", top: "23px" }}
            />
            <MenuItem style={{ paddingBottom: "0px" }}>
              <FormControlLabel
                className={classes.FormControlLabel}
                control={
                  <BlueCheckbox
                    checked={state.checkedA}
                    onChange={handleChange}
                    name="checkedA"
                  />
                }
                label=" Next Generation"
              />
            </MenuItem>
            <MenuItem style={{ paddingBottom: "0px" }}>
              <FormControlLabel
                className={classes.FormControlLabel}
                control={
                  <BlueCheckbox
                    checked={state.checkedB}
                    onChange={handleChange}
                    name="checkedB"
                    color="#005CB9"
                  />
                }
                label=" Expand Base"
              />
            </MenuItem>
            <MenuItem style={{ paddingBottom: "0px", paddingTop: "0px" }}>
              <FormControlLabel
                className={classes.FormControlLabel}
                control={
                  <BlueCheckbox
                    checked={state.checkedF}
                    onChange={handleChange}
                    name="checkedF"
                    color="#005CB9"
                  />
                }
                label=" Breakthrough"
              />
            </MenuItem>
            <div className="menu-bottom-styl">
              <label>Reset</label>
              <button>Apply</button>
            </div>
          </Menu>
        </div>
      </div>
    );
  }

  const data = InnovationtableData;

  const columns = React.useMemo(() => [
    {
      Header: "Name",
      accessor: "name",
      Filter: SelectColumnFilter,
      // filter: "includes",
    },
    {
      Header: "Gear",
      accessor: "gear",
      Filter: SelectColumnFilter,
    },
    {
      Header: "Status",
      accessor: "status",
      Filter: SelectColumnFilter,
    },
    {
      Header: "Manager",
      accessor: "manager",
      Filter: SelectColumnFilter,
    },
    {
      Header: "SBU",
      accessor: "sbu",
      Filter: SelectColumnFilter,
    },
    {
      Header: "Brand",
      accessor: "brand",
      Filter: SelectColumnFilter,
    },
    {
      Header: "Volume (Stat Cases)",
      accessor: "volume",
      Filter: SelectColumnFilter,
    },
    {
      Header: "SOS",
      accessor: "sos",
      Filter: SelectColumnFilter,
    },
    {
      id: "Upload",
      Header: "",
      Cell: ({ row }) => (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            cursor: "pointer",
          }}
        >
          <MdContentPaste
            color="#005CB9"
            size="1.5rem"
            onClick={() => history.push(`/innovation/allprojects/${row.id}`)}
          />
          <MdShare color="#005CB9" size="1.5rem" />
          <MdCloudDownload color="#005CB9" size="1.5rem" />
          <MdFileUpload color="#005CB9" size="1.5rem" />
        </div>
      ),
    },
  ]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="innovation-projects-screen">
      <CssBaseline />
      <InnovationTable columns={columns} data={data} />
    </div>
  );
};

export default InnovationTabPanel;
