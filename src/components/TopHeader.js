import React from "react";
import TopDropdown from "../components/TopDropdown";
import { BsArrowLeft } from "react-icons/bs";
import { IoIosArrowForward } from "react-icons/io";
import { Link } from "react-router-dom";
import Filter from "./Filter/Filter";
import history from "../utils/history";
import { capitalize } from "../utils/utils";

const TopHeader = ({ pageName }) => {
  return (
    <div className="top-header">
      <div style={{ display: "flex", alignItems: "center" }}>
        <Filter />
        <div style={{ marginLeft: "20px" }}>
          <label>
            <Link style={{ cursor: "pointer" }} className="link-styl">
              {pageName.split("/")[1].toUpperCase()}
            </Link>
          </label>
          <IoIosArrowForward style={{ cursor: "pointer", margin: "0 10px" }} />
          <label>{capitalize((pageName.split("/")[2])).replace("_"," ")}</label>
        </div>
      </div>
    </div>
  );
};

export default TopHeader;
