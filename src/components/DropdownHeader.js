import React, { useEffect, useState, useContext } from "react";
import TopDropdown from "./TopDropdown";
import { MdSave } from "react-icons/md";
import { IoCloudDoneSharp } from "react-icons/io5";
import HeaderTag from "./HeaderTag";
import axios from "axios";
import TableData from "../contexts/TableData";
import _ from "lodash";
import { URL, PORT } from "../Config";

const DropdownHeader = ({
  pageName,
  subPage,
  selectedPrior,
  selectedViewBy,
  selectedCurrentNDF,
  setSelectedPrior,
  setSelectedViewBy,
  setSelectedCurrentNDF,
  setSelectedDrillDown,
  selectedDrillDown,
}) => {
  const { setData } = useContext(TableData);

  const [selectedBrands, setSelectedBrands] = useState(null);
  const [dropdownData, setDropDownData] = useState(null);
  let selectedNodes = sessionStorage.getItem("IbpStore_allFiltersSelected");

  useEffect(() => {
    //call trend table api here for changing the table data in accordance to dropdown selected
    if (
      selectedViewBy !== null ||
      selectedCurrentNDF !== null ||
      selectedPrior !== null
    ) {
      axios
        .post(`${URL}/plan/trend_table`, selectedNodes)

        .then((resp) => {
          // console.log(resp);
          setData(resp.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }

    if (selectedViewBy === "Brands" || selectedViewBy === "MFF") {
      // axios
      //   .get("topdropdown api with drilldown data")
      //   .then((resp) => console.log(resp))
      //   .catch((err) => console.log(err));
    }
  }, [selectedNodes]);

  React.useEffect(() => {
    const values = JSON.parse(
      window.sessionStorage.getItem("IbpStore_allFiltersSelected")
    );

    console.log("inside use");

    if (
      !sessionStorage.getItem("dropdownCheck") ||
      sessionStorage.getItem("dropdownCheck") === null
    ) {
      console.log("is here", sessionStorage.getItem("dropdownCheck"));
      const body = {
        ...values,
        level: viewBy[0].label,
        // currentndf: compareWith[0].label,
        // base: base[2].label,
      };
      window.sessionStorage.setItem(
        "IbpStore_allFiltersSelected",
        JSON.stringify(body)
      );
    }

    // axios
    //   .get(`${URL}/plan/topfilter`)
    //   .then((resp) => {
    // setDropDownData(resp)
    //     console.log(resp);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  }, []);

  const dropdownOptions = [
    { "View By": ["Drivers", "Brands", "MFF"] },
    {
      Base: [
        "FY21 March NDF",
        "FY21 April NDF",
        "FY21 May/June NDF",
        "FY22 BUDGET #2",
      ],
    },
    {
      "Compare With": [
        "FY21 March NDF",
        "FY21 April NDF",
        "FY21 May/June NDF",
        "FY22 BUDGET #2",
      ],
    },
    { DrillDown: ["MFF", "Sub MFF"] },
    { Brands: ["Brands", "Purpose"] },
  ];

  const viewBy = dropdownOptions[0]["View By"]
    .filter((item) => item != "Brands")
    .map((item) => {
      return {
        value: item,
        label: item,
      };
    });

  const base = dropdownOptions[1]["Base"]
    .filter((item) => item !== JSON.parse(selectedNodes).currentndf)
    .map((item) => {
      return {
        value: item,
        label: item,
      };
    });

  const compareWith = dropdownOptions[2]["Compare With"]
    .filter((item) => item !== JSON.parse(selectedNodes).base)
    .map((item) => {
      return {
        value: item,
        label: item,
      };
    });

  const Brands = dropdownOptions[4]["Brands"].map((item) => {
    return {
      value: item,
      label: item,
    };
  });

  const DrillDown = dropdownOptions[3]["DrillDown"].map((item) => {
    return {
      value: item,
      label: item,
    };
  });

  const handleChangeViewBy = (selectedOption) => {
    setSelectedViewBy(selectedOption.label);
    const sessionStorageValues = JSON.parse(
      window.sessionStorage.getItem("IbpStore_allFiltersSelected")
    );

    setSelectedDrillDown(selectedOption.label);

    sessionStorage.setItem("dropdownCheck", true);
    const body = {
      ...sessionStorageValues,
      level: selectedOption.label,
      drilldown:
        selectedOption.label === "MFF"
          ? "MFF"
          : selectedOption.label === "Brands"
          ? "Brands"
          : "",
    };
    window.sessionStorage.setItem(
      "IbpStore_allFiltersSelected",
      JSON.stringify(body)
    );
  };

  const handleChangeCurrentNdf = (selectedOption) => {
    setSelectedCurrentNDF(selectedOption.label);
    const sessionStorageValues = JSON.parse(
      window.sessionStorage.getItem("IbpStore_allFiltersSelected")
    );
    sessionStorage.setItem("dropdownCheck", true);

    const body = { ...sessionStorageValues, currentndf: selectedOption.label };
    window.sessionStorage.setItem(
      "IbpStore_allFiltersSelected",
      JSON.stringify(body)
    );
  };

  const handleChangeBase = (selectedOption) => {
    console.log("called");
    setSelectedPrior(selectedOption.label);
    const sessionStorageValues = JSON.parse(
      window.sessionStorage.getItem("IbpStore_allFiltersSelected")
    );
    sessionStorage.setItem("dropdownCheck", true);

    const body = { ...sessionStorageValues, base: selectedOption.label };
    window.sessionStorage.setItem(
      "IbpStore_allFiltersSelected",
      JSON.stringify(body)
    );
  };

  const handleChangeDrillDown = (selectedOption) => {
    setSelectedDrillDown(selectedOption.label);
    const sessionStorageValues = JSON.parse(
      window.sessionStorage.getItem("IbpStore_allFiltersSelected")
    );
    const body = { ...sessionStorageValues, drilldown: selectedOption.label };
    window.sessionStorage.setItem(
      "IbpStore_allFiltersSelected",
      JSON.stringify(body)
    );
  };

  // const handleChangeBrands = (selectedOption) => {
  //   setSelectedPrior(selectedOption.label);
  //   const sessionStorageValues = JSON.parse(
  //     window.sessionStorage.getItem("IbpStore_allFiltersSelected")
  //   );
  //   const body = { ...sessionStorageValues, base: selectedOption.label };
  //   window.sessionStorage.setItem(
  //     "IbpStore_allFiltersSelected",
  //     JSON.stringify(body)
  //   );
  // };

  return (
    <div className="dropdown-header-container">
      <HeaderTag />
      {pageName.split("/")[2] === "plans" ? (
        <TopDropdown
          viewBy={viewBy}
          base={base}
          compareWith={compareWith}
          handleChangeBase={handleChangeBase}
          handleChangeCurrentNdf={handleChangeCurrentNdf}
          handleChangeViewBy={handleChangeViewBy}
          handleChangeDrillDown={handleChangeDrillDown}
          selectedViewBy={selectedViewBy}
          selectedCurrentNDF={selectedCurrentNDF}
          selectedPrior={selectedPrior}
          selectedDrillDown={selectedDrillDown}
          Brands={Brands}
          DrillDown={DrillDown}
        />
      ) : pageName.split("/")[2] === "summary" ? (
        <TopDropdown
          base={base}
          compareWith={compareWith}
          handleChangeBase={handleChangeBase}
          handleChangeCurrentNdf={handleChangeCurrentNdf}
          handleChangeViewBy={handleChangeViewBy}
          selectedViewBy={selectedViewBy}
          selectedCurrentNDF={selectedCurrentNDF}
          selectedPrior={selectedPrior}
          currentndf="Current NDF"
          prior="Prior"
        />
      ) : pageName.split("/")[2] === "opportunity_risk" ? (
        <TopDropdown
          viewBy={viewBy}
          base={base}
          compareWith={compareWith}
          handleChangeBase={handleChangeBase}
          handleChangeCurrentNdf={handleChangeCurrentNdf}
          handleChangeViewBy={handleChangeViewBy}
          handleChangeDrillDown={handleChangeDrillDown}
          selectedViewBy={selectedViewBy}
          selectedCurrentNDF={selectedCurrentNDF}
          selectedPrior={selectedPrior}
          selectedDrillDown={selectedDrillDown}
          viewby={"View By"}
          currentndf={"Current NDF"}
          Brands={Brands}
          DrillDown={DrillDown}
        />
      ) : pageName.split("/")[2] === "upside_down" ? (
        <TopDropdown
          viewBy={viewBy}
          base={base}
          compareWith={compareWith}
          handleChangeDrillDown={handleChangeDrillDown}
          handleChangeBase={handleChangeBase}
          handleChangeCurrentNdf={handleChangeCurrentNdf}
          handleChangeViewBy={handleChangeViewBy}
          selectedViewBy={selectedViewBy}
          selectedCurrentNDF={selectedCurrentNDF}
          selectedPrior={selectedPrior}
          selectedDrillDown={selectedDrillDown}
          viewby={"View By"}
          currentndf={"Current NDF"}
          Brands={Brands}
          DrillDown={DrillDown}
        />
      ) : subPage ? (
        <div className="save-btn-styl">
          <button>
            <MdSave style={{ marginRight: "10px" }} size={"1.1rem"} />
            Save
          </button>
          <button>
            <IoCloudDoneSharp style={{ marginRight: "10px" }} size={"1.2rem"} />
            Publish
          </button>
        </div>
      ) : null}
    </div>
  );
};

export default DropdownHeader;
