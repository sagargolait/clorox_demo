import React from "react";

const ProjectDetails = ({ projectDetailsData }) => {
  return (
    <>
      <span id="head">Reco Data</span>

      <div className="product-details-styl">
        {Object.keys(projectDetailsData).map(function (keyName, keyIndex) {
          return (
            <div className="details-styl" key={keyName}>
              <p>{keyName}</p>
              <h6>{projectDetailsData[keyName]}</h6>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default ProjectDetails;
