import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import companyLogo from "../assets/logos/companyLogo.png";
import Hublogo from "../assets/logos/Hub-logo.png";
import "./NavigationBar.css";
import Avatar from "@material-ui/core/Avatar";
import { IoMdNotificationsOutline } from "react-icons/io";
import { useHistory } from "react-router";

const NavigationBar = () => {
  const history = useHistory();
  return (
    <>
      <Navbar className="navbar-styl" bg="#005CB9" expand="lg">
        <Navbar.Brand href="/">
          <img src={companyLogo} style={{ height: "29px" }} alt="companyLogo" />
        </Navbar.Brand>
        <img src={Hublogo} alt="hubIcon" style={{ marginRight: "10px" }} />
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavDropdown title="PLANNING" id="basic-nav-dropdown">
              <NavDropdown.Item
                className="dropdown-items-styl"
                onClick={() => history.push("/planning/summary")}
              >
                Summary
              </NavDropdown.Item>
              <NavDropdown.Item
                className="dropdown-items-styl"
                onClick={() => history.push("/planning/plans")}
              >
                Plans
              </NavDropdown.Item>
              <NavDropdown.Item
                className="dropdown-items-styl"
                onClick={() => history.push("/planning/opportunity_risk")}
              >
                Opportunity and Risk
              </NavDropdown.Item>
              <NavDropdown.Item
                className="dropdown-items-styl"
                onClick={() => history.push("/planning/upside_downside")}
              >
                Upside Downside
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="INNOVATION" id="basic-nav-dropdown">
              <NavDropdown.Item
                className="dropdown-items-styl"
                href="#action/3.2"
              >
                ScoreCard
              </NavDropdown.Item>
              <NavDropdown.Item
                className="dropdown-items-styl"
                onClick={() => history.push("/innovation/allprojects")}
              >
                All Projects
              </NavDropdown.Item>
              <NavDropdown.Item
                className="dropdown-items-styl"
                href="#action/3.2"
              >
                In Market Tracking
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
        <IoMdNotificationsOutline
          size={"1.5rem"}
          style={{ marginRight: "10px" }}
        />
        <Avatar
          style={{
            backgroundColor: "#fff",
            color: "#005CB9",
            width: "30px",
            height: "30px",
            fontStyle: "normal",
            fontFamily: "Lato",
            fontWeight: "600",
            fontSize: "12px",
            lineHeight: "14px",
          }}
        >
          SA
        </Avatar>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginLeft: "10px",
          }}
        ></div>
      </Navbar>
    </>
  );
};

export default NavigationBar;
