import React from "react";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { projectDetailsData } from "../data/ProductdetailsData";
import ProjectDetails from "./ProjectDetails";
import Productdetails from "./Productdetails";
import CustomerDetails from "./CustomerDetails";
import VolumeDetails from "./VolumeDetails";
import { VolumeDetailsData } from "../data/VolumeDetailsTableData";
import {
  MdAssignment,
  MdShoppingCart,
  MdPeople,
  MdInsertChart,
  MdMonetizationOn,
} from "react-icons/md";
import FinanceDetails from "../InnovationDetails/FinanceDetails";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabs: {
    "& .MuiTab-wrapper": {
      fontFamily: "Lato",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "14px",
      lineHeight: "17px",
      textTransform: "capitalize",
      display: "flex",
      alignItem: "center",
      justifyContent: "center",
      flexDirection: "row",
    },
    "&:last-child": {
      borderRight: "none",
    },

    "&.MuiTab-labelIcon": {
      minHeight: "30px",
    },
    "&.Mui-selected": {
      background: "rgba(0, 92, 185, 0.1);",
    },
    "&.MuiPaper-elevation4": {
      boxShadow: "none",
    },
    "&.MuiTab-textColorInherit": {
      opacity: "1",
    },
  },
}));

const StyledTabs = withStyles(() => ({
  root: {
    backgroundColor: "#FFFFFF",
    fontSize: "14px",
    minHeight: "35px",
    color: "#005CB9",
    "& .MuiTabs-flexContainer": {
      height: "20px",
    },
    "& .MuiTabs-fixed": {
      height: "35px",
    },
  },
}))(Tabs);

const Details = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ borderBottom: "1px solid #eee" }}>
        <StyledTabs
          variant="fullWidth"
          value={value}
          onChange={handleChange}
          aria-label="nav tabs example"
          TabIndicatorProps={{ style: { border: "2px solid #005CB9" } }}
        >
          <LinkTab
            className={classes.tabs}
            label="Project Details"
            icon={
              <MdAssignment size={"1.5rem"} style={{ marginRight: "5px" }} />
            }
            {...a11yProps(0)}
          />
          <LinkTab
            className={classes.tabs}
            label="Product Details"
            icon={
              <MdShoppingCart size={"1.5rem"} style={{ marginRight: "5px" }} />
            }
            href="/trash"
            {...a11yProps(1)}
          />
          <LinkTab
            className={classes.tabs}
            label="Customer Details"
            icon={<MdPeople size={"1.5rem"} style={{ marginRight: "5px" }} />}
            href="/spam"
            {...a11yProps(2)}
          />
          <LinkTab
            className={classes.tabs}
            label="Volume Details"
            icon={
              <MdInsertChart size={"1.5rem"} style={{ marginRight: "5px" }} />
            }
            href="/spam"
            {...a11yProps(3)}
          />
          <LinkTab
            className={classes.tabs}
            label="Finance Details"
            href="/spam"
            icon={
              <MdMonetizationOn
                size={"1.5rem"}
                style={{ marginRight: "5px" }}
              />
            }
            {...a11yProps(4)}
            style={{ borderRight: "1px solid #eee" }}
          />
        </StyledTabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ProjectDetails projectDetailsData={projectDetailsData} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Productdetails />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <CustomerDetails />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <VolumeDetails />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <FinanceDetails />
      </TabPanel>
    </div>
  );
};

export default Details;
