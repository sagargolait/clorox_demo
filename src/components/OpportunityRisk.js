import React, { useState } from "react";
import { useTable, useExpanded } from "react-table";
import customData from "./monthly_data.json";
import {
  IoIosArrowUp,
  IoIosArrowDown,
  RiArrowUpDownFill,
} from "react-icons/io";
import {
  MdDelete,
  MdRemoveCircleOutline,
  MdSwapVert,
  MdReplyAll,
  MdSave,
} from "react-icons/md";
import { IoCloudDoneSharp } from "react-icons/io5";
import { BsFillReplyAllFill } from "react-icons/bs";
import { GiSightDisabled } from "react-icons/gi";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import "./Table.css";
import TopHeader from "../components/TopHeader";

var head;
const useStyles = makeStyles({
  row: {
    borderRadius: "2px 2px 0px 0px",
    "&:first-child": {},
  },

  rightborder: {
    "&:first-child": {
      borderRight: "1px solid #DDDDDD",
    },
  },
});

const StyledTableCell = withStyles((theme) => ({
  root: {
    borderBottom: "0px !important",
    "&:first-child": {
      borderRight: "1px solid #DDDDDD",
      border: "0",
      width:'25%'
    },
  },
  body: {
    fontSize: 14,
    fontFamily: "Lato",
    fontWeight: "700",
    lineHeight: "14px",
    color: "#000000",
    "&:first-child": {
      fontSize: "14px",
      fontFamily: "Lato",
      fontWeight: "700",
      lineHeight: "14px",
      color: "#666666",
    },
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {},
}))(TableRow);

const EditableCell = ({
  value: initialValue,
  row,
  column: { id },
  updateMyData,
  ...props
}) => {
  // We need to keep and update the state of the cell normally
  const [value, setValue] = React.useState(initialValue);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  // We'll only update the external data when the input is blurred
  const onBlur = () => {
    updateMyData(row.original.id, id, value);
  };

  let getID = (id) => {};
  return (
    <>
      <input
        type="text"
        className="editableTableInput1"
        value={value}
        style={{
          width: "40px",
          height: "37px",
          border: "1px solid #EFEFEF",
          boxSizing: "border-box",
          outline: 0,
          fontFamily: "Lato",
          fontWeight: "700",
          fontSize: "14px",
          color: "#000000",
        }}
        onChange={onChange}
        onBlur={onBlur}
        disabled={row.original.isDisabled}
        onClick={(id) => {
          getID(id);
        }}
      />
    </>
  );
};

const defaultColumn = {
  Cell1: EditableCell,
};

function Table() {
  const classes = useStyles();

  const [data, setData] = useState(customData);

  let testing_head = Object.keys(data[0]);
  testing_head = testing_head.filter((x) => {
    return (
      x != "id" &&
      x != "level" &&
      x != "isEditable" &&
      x != "isSaved" &&
      x != "isPublished" &&
      x != "subRows" &&
      x != "title" &&
      x != "Total"
    );
  });

  testing_head.unshift("title");
  testing_head[testing_head.length] = "Total";
  head = testing_head;

  let columns = React.useMemo(
    () =>
      head.map((x, i) => {
        if (x == "title") {
          return {
            //   // Build our expander column
            id: "expander", // Make sure it has an ID
            Cell: ({ row }) =>
              row.canExpand ? (
                <span
                  title={""}
                  {...row.getToggleRowExpandedProps({
                    style: {
                      paddingLeft: `${row.depth}rem`,
                      fontFamily: "Lato",
                      fontWeight: "700",
                      fontStyle: "SemiBold",
                      fontSize: "14px",
                      lineHeight: "17px",
                      color: "#666666",
                    },
                  })}
                >
                  {row.isExpanded ? (
                    <>
                      <IoIosArrowUp />
                      {`${row.original.title}`}
                    </>
                  ) : (
                    <>
                      <IoIosArrowDown /> {`${row.original.title}`}
                      {row.original.level != "version" ? (
                        <Tooltip
                          disableFocusListener
                          disableTouchListener
                          placement="right-start"
                          title={`${row.subRows.length - 1} comments`}
                        >
                          <span title={""} className="commentCount">
                            ℹ️
                          </span>
                        </Tooltip>
                      ) : null}
                    </>
                  )}
                </span>
              ) : //added this later when the 2nd row was like not expandable
              row.id != 1 ? (
                <>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    {row.original.isEditable === "button" ? (
                      <span
                        title={""}
                        id={row.original.id}
                        style={{
                          cursor: "pointer",
                          fontFamily: "Lato",
                          fontStyle: "semiBold",
                          fontWeight: "700",
                          fontSize: "14px",
                          lineHeight: "17px",
                          color: "#005CB9",
                          fontStyle: "italic",
                        }}
                        onClick={(e) => addComment(e, row)}
                      >
                        Add New Comment
                      </span>
                    ) : (
                      <>
                        {" "}
                        <Tooltip
                          title={`${row.original.title}`}
                          placement="top-end"
                        >
                          <input
                            value={row.original.title}
                            type="text"
                            placeholder={`${row.original.type} `}
                            style={{
                              height: "37px",
                              boxSizing: "border-box",
                              marginRight: "10px",
                              marginLeft:"16px",
                              padding: "0.8em",
                              fontWeight:'700',
                              fontSize:'14px',
                              color:'#666666'
                            }}
                            className="testingInput"
                            disabled={row.original.isDisabled}
                            onChange={(e) => onChangeHandler(e, row)}
                            onBlur={(e) => {
                              onBlurHandler(e, row);
                            }}
                          />
                        </Tooltip>
                        <span title={"Delete"} style={{ cursor: "pointer" }}>
                          <MdDelete
                            onClick={(e) => deleteRow(row.original)}
                            style={{ marginRight: "5px" }}
                            size={"1.4rem"}
                            color={"#005CB9"}
                          />
                        </span>{" "}
                        <span title={"Disable"}>
                          <BsFillReplyAllFill
                            style={{ marginLeft: "7px" }}
                            onClick={() => disableRow(row.original)}
                            size={"1.4rem"}
                            color={"#005CB9"}
                          />
                        </span>{" "}
                        {/* <span
                        title={"Disable"}
                          style={{ cursor: "pointer" }}
                        >
                          <MdSwapVert
                            style={{ marginLeft: "7px" }}
                            onClick={() => disableRow(row.original)}
                            size={"1.4rem"}
                            color={"#005CB9"}
                          />
                        </span>{" "} */}
                      </>
                    )}
                  </div>
                </>
              ) : (
                <span title={""}>{row.original.title}</span>
              ),
          };
        } else {
          return {
            Header: x.replace("_", " "),
            accessor: x,
            Cell: ({ value }) => Number(value).toFixed(0),
          };
        }
      }),
    []
  );

  const disableRow = (rowId) => {
    const newData = [...data];

    newData.forEach(function iter(a) {
      if (rowId.id == a.id) {
        a.isDisabled = !a.isDisabled;
        a["isEdited"] = true;
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    let isDisabledCount = 0;
    let parentID = rowId.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");
    newData.forEach(function additionToParentRow(x) {
      if (x.id == parentID) {
        head.map((v) => {
          if (v != "title") {
            x[v] = 0;
          }
        });

        x.subRows.map((cellsData, index) => {
          if (index != x.subRows.length - 1) {
            if (!cellsData.isDisabled && !cellsData.isDeleted) {
              head.map((h) => {
                if (h != "title") {
                  x[h] = Number(x[h]) + Number(cellsData[h]);
                }
              });
            } else {
              isDisabledCount++;
            }
          }

          //making q1, q2, q3 as 0 if everything is deleted or disabled
          if (x.subRows.length - 1 == isDisabledCount) {
            head.map((h) => {
              if (h != "title") {
                x[h] = Number(0);
              }
            });
          }
        });
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
    });

    setData(newData);
  };

  const deleteRow = (rowId) => {
    const newData = [...data];

    newData.forEach(function iter(a) {
      if (rowId.id == a.id) {
        a.isDeleted = !a.isDeleted;
        a["isEdited"] = true;
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    let isDeletedCount = 0;
    let parentID = rowId.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    newData.forEach(function additionToParentRow(x) {
      if (x.id == parentID) {
        head.map((v) => {
          if (v != "title") {
            x[v] = 0; //initializing each parent cell to 0
          }
        });

        x.subRows.map((cellsData, index) => {
          //in cellData all the comments are coming
          //in x i hava parent data and in cellData we have subRows of it i.e x
          if (index != x.subRows.length - 1) {
            //not iterating for the last one as its a button (in the comment part only)

            if (!cellsData.isDeleted && !cellsData.isDisabled) {
              head.map((h) => {
                if (h != "title") {
                  x[h] = Number(x[h]) + Number(cellsData[h]);
                }
              });
            } else {
              isDeletedCount++;
            }
          }

          //making q1, q2, q3 as 0 if everything is deleted or disabled
          if (x.subRows.length - 1 == isDeletedCount) {
            head.map((h) => {
              if (h != "title") {
                x[h] = Number(0);
              }
            });
          }
        });
      }
      Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
    });

    setData(newData);
  };

  const updateMyData = (rowId, columnId, value) => {
    const newData = [...data];
    let reqired_head = [];
    reqired_head = head.filter((h) => {
      return h != "title" && h != "Total";
    });

    newData.forEach(function iter(a) {
      if (rowId == a.id) {
        if (value !== "") {
          a[columnId] = value;
          a["isEdited"] = true;
        }
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    //putting this condition of title as we dont want the title to get added according to comments
    if (columnId != "title") {
      let parentID = rowId.split("_");
      parentID = parentID.slice(0, parentID.length - 1);
      parentID = parentID.join("_");

      newData.forEach(function additionToParentRow(x) {
        if (x.id == parentID) {
          x[columnId] = 0;

          x.subRows.map((subdata, index) => {
            if (index !== x.subRows.length - 1) {
              if (!subdata.isDeleted && !subdata.isDisabled)
                x[columnId] = Number(x[columnId]) + Number(subdata[columnId]);

              subdata["Total"] = reqired_head.reduce((a, h) => {
                return a + Number(subdata[h]);
              }, 0);

              // subdata["Total"] =
              //   Number(subdata["Q1"]) +
              //   Number(subdata["Q2"]) +
              //   Number(subdata["Q3"]) +
              //   Number(subdata["Q4"]);
            }

            //this is for parent Total
            x["Total"] = reqired_head.reduce((a, h) => {
              return a + Number(x[h]);
            }, 0);

            // x["Total"] =
            //   Number(x["Q1"]) +
            //   Number(x["Q2"]) +
            //   Number(x["Q3"]) +
            //   Number(x["Q4"]);
          });
        }
        Array.isArray(x.subRows) && x.subRows.forEach(additionToParentRow);
      });
    }

    setData(newData);
  };

  const addComment = (x, y) => {
    //if x.tagrget.id is "1_1_1_45"
    //then dd="1_1_1_46"
    let dd = x.target.id.split("_");
    dd[dd.length - 1] = String(Number(dd[dd.length - 1]) + 1);
    dd = dd.join("_");

    //if x.tagrget.id is "1_1_1_45"
    //then parentID = 1_1_1
    let parentID = x.target.id.split("_");
    parentID = parentID.slice(0, parentID.length - 1);
    parentID = parentID.join("_");

    let preserve_button = y.original;

    const newData = [...data];
    newData.forEach(function iter(a) {
      if (a.id == parentID) {
        a.subRows.map((d) => {
          if (d.id == x.target.id) {
            // d.Q1 = "";
            // d.Q2 = "";
            // d.Q3 = "";
            // d.Q4 = "";
            // d.Total = "";

            head.map((h) => {
              d[h] = "";
              d[h] = "";
              d[h] = "";
              d[h] = "";
              d[h] = "";
              d[h] = "";
            });

            d.id = x.target.id;
            d.isDeleted = false;
            d.isDisabled = false;
            d.isEditable = true;
            d.isEdited = true;
            d.isPublished = false;
            d.isSaved = false;
            d.level = y.original.level;
            d.parentTitle = y.original.parentTitle;
            d.type = "Comments";
          }
        });

        let dummy_obj = {
          id: dd,
          isDeleted: false,
          isDisabled: false,
          isEditable: "button",
          isEdited: false,
          isPublished: "",
          isSaved: "",
          level: y.original.level,
          parentTitle: y.original.parentTitle,
          type: "Comments",
        };

        head.map((h) => {
          if (h == "title") dummy_obj[h] = y.original.title;
          else {
            dummy_obj[h] = ".";
          }
        });

        a.subRows = [
          ...a.subRows,
          dummy_obj,
          //just commented these to make it dynamic
          // {
          //   "title": y.original.title,
          //   "Q1": ".",
          //   "Q2": ".",
          //   "Q3": ".",
          //   "Q4": ".",
          //   "Total": ".",

          //   "id": dd,
          //   "isDeleted": false,
          //   "isDisabled": false,
          //   "isEditable": "button",
          //   "isEdited": false,
          //   "isPublished": "",
          //   "isSaved": "",
          //   "level": y.original.level,
          //   "parentTitle": y.original.parentTitle,
          //   "type": "Comments",
          // }
        ];
      }
      setData([...data]);
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    setData([...data]);
  };

  let onChangeHandler = (m, n) => {
    updateMyData(n.original.id, "title", m.target.value);
  };

  let onBlurHandler = (m, n) => {
    updateMyData(n.original.id, "title", m.target.value);
    //here we are sending "title" as a parameter because we want to update the value of title parameter in the main data
  };

  let editedData = [];
  //this function is getting called when the SAVE button is clicked
  const onSave = () => {
    const newData = [...data];

    newData.forEach(function iter(a) {
      if (a.isEdited) {
        editedData.push(a);
      } else {
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });
  };

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable(
      {
        columns,
        data,
        defaultColumn,
        updateMyData,
        addComment,
        autoResetExpanded: false,
        initialState: {
          expanded: { 0: true },
        },
      },
      useExpanded
    );

  let running_difference_temp = data[1];
  let r = {};
  let d = {};
  head.map((h) => {
    if (h != "title") r[h] = -running_difference_temp[h];
  });

  head.map((h) => {
    if (h != "title") d[h] = running_difference_temp[h];
  });

  let running_difference = r;
  let delta_running_difference = d;

  let claculateRunningDifference = () => {
    head.map((head_data) => {
      if (head_data != "title") {
        data.forEach(function iter(a) {
          if (a.level == "version" || a.level == "driver") {
            running_difference[head_data] =
              running_difference[head_data] + a[head_data];
            delta_running_difference[head_data] =
              running_difference[head_data] +
              delta_running_difference[head_data];
          }
          Array.isArray(a.subRows) && a.subRows.forEach(iter);
        });
      }
    });
  };
  return (
    <>
      {claculateRunningDifference()}

      <MaUTable
        style={{
          overflowX: "auto",
          margin: "1rem",
          border: "1px solid",
          width: "97%",
          border: "0 !important",
        }}
        {...getTableProps()}
      >
        <TableHead>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => {
                return (
                  <>
                    <TableCell {...column.getHeaderProps()}>
                      {column.render("Header")}
                    </TableCell>
                  </>
                );
              })}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <>
                <StyledTableRow
                  style={{ border: "0px !important" }}
                  className={row.original.level}
                  {...row.getRowProps()}
                >
                  {row.cells.map((cell, j) => {
                    //if the cell is diabled then we dont have to show in the UI
                    // same for isDelete, but confirm it first
                    if (
                      cell.row.original.isDisabled ||
                      cell.row.original.isDeleted
                    ) {
                      return null;
                    } else if (
                      cell.row.original.isEditable == true &&
                      cell.value != undefined &&
                      // || cell.value == 0
                      j != row.cells.length - 1 //adding this condition for now showing i/p box on the Total cell
                    ) {
                      return (
                        <StyledTableCell
                          className={
                            row.index == 0 ? classes.rightborder : null
                          }
                          className={row.original.level}
                          {...cell.getCellProps()}
                        >
                          {cell.render("Cell1")}
                        </StyledTableCell>
                      );
                    } else if (cell.value == ".")
                      return (
                        <StyledTableCell
                          {...cell.getCellProps()}
                        ></StyledTableCell>
                      );
                    else
                      return (
                        <StyledTableCell
                          className={
                            row.index == 0 ? classes.rightborder : null
                          }
                          {...cell.getCellProps()}
                        >
                          {cell.render("Cell")}
                        </StyledTableCell>
                      );
                  })}
                </StyledTableRow>
              </>
            );
          })}

          {
            <>
              <tr>
                {head.map((h) => {
                  if (h == "title") {
                    return <td className="DeltaRow DeltaRow1">Delta</td>;
                  } else
                    return (
                      <td className="DeltaRow1 DeltaRowCell">
                        {delta_running_difference[h].toFixed(0)}
                      </td>
                    );
                })}
              </tr>
              <tr>
                {head.map((h) => {
                  if (h == "title") {
                    return <td className="DeltaRow">Sub Total</td>;
                  } else
                    return (
                      <td className="DeltaRowCell">
                        {running_difference[h].toFixed(0)}
                      </td>
                    );
                })}
              </tr>
            </>
          }
        </TableBody>
      </MaUTable>
    </>
  );
}

let OpportunityRisk = (props) => {
  return (
    <div className="planning-container">
      <TopHeader pageName={props.location.pathname} />
      <div className="table-content">
        <div className="content-header">
          <div className="title-styl">
            <label>
              Volume Decompositon - <b>Drivers</b>
            </label>
          </div>
          <div className="planning-header-btn-styl">
            <button style={{ marginRight: "15px" }}>
              <MdSave style={{ marginRight: "10px" }} size={"1.1rem"} />
              Save
            </button>
            <button disabled>
              <IoCloudDoneSharp
                style={{ marginRight: "10px" }}
                size={"1.2rem"}
              />
              Publish
            </button>
          </div>
        </div>
        <Table />
      </div>
    </div>
  );
};

export default OpportunityRisk;
