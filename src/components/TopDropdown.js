import React, { useState } from "react";
import Select, { components } from "react-select";
import "./TopDropdown.css";
import axios from "axios";
import Tooltip from "../utils/Tooltip";

export const selectStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px solid #eeeeee",
    color: state.isSelected ? "#005CB9" : "",
    fontSize: 16,
    backgroundColor: state.isSelected ? "#eee" : "",
    textAlign: "left",
    cursor: "pointer",
  }),
  container: (base) => ({
    ...base,
    width: "120px",
    height: "24px",
    margin: "0px 6px",
  }),
  control: (base) => ({
    ...base,
    width: "120px",
    height: "24px",
    minHeight: "24px",
    fontSize: 14,
    fontFamily: "Lato",
    fontStyle: "Normal",
    fontWeight: "900",
    borderRadius: 2,
    borderColor: "none",
    border: "none",
    boxShadow:
      "-5px -5px 10px rgba(0, 80, 115, 0.02), 5px 5px 10px rgba(0, 80, 115, 0.02)",
    width: "100%",
    textAlign: "center",
    cursor: "pointer",
  }),
  dropdownIndicator: (base) => ({
    ...base,
    color: "#005CB9",
    padding: 0,
    paddingRight: "5px",
  }),
  indicatorSeparator: (base) => ({
    ...base,
    display: "none",
    padding: "0px",
  }),
  singleValue: (base) => ({
    ...base,
    color: "#005CB9",
    maxWidth: "calc(100% - 30px)",
  }),
  valueContainer: (base) => ({
    ...base,
    padding: 0,
    width: "160px",
    position: "none",
    paddingLeft: 2,
  }),
  menu: (base) => ({
    ...base,
    width: "180px",
    "&:last-child": {
      borderBottom: "none",
    },
  }),
};

const customSingleValue = (props) => {
  console.log(props);
  const { data, getStyles, isDisabled } = props;
  return (
    <Tooltip content={data.label} isDisabled={isDisabled}>
      <div
        // ref={selectRef}
        style={getStyles("singleValue", props)}
        className="pq-select__single-value"
      >
        {data.icon && (
          <span
            className={`pq-select__single-value-icon pq-icon -${data.icon}`}
          />
        )}
        <span>{data.label}</span>
      </div>
    </Tooltip>
  );
};

const TopDropdown = ({
  currentndf,
  prior,
  viewby,
  viewBy,
  base,
  compareWith,
  handleChangeViewBy,
  handleChangeCurrentNdf,
  handleChangeBase,
  handleChangeDrillDown,
  selectedPrior,
  selectedViewBy,
  DrillDown,
  Brands,
  selectedCurrentNDF,
  selectedDrillDown,
}) => {
  let selectedNodes = sessionStorage.getItem("IbpStore_allFiltersSelected");

  return (
    <div className="dropdown-container">
      {currentndf && prior ? (
        <>
          <div>
            <label>{prior}</label>

            <Select
              styles={selectStyles}
              onChange={handleChangeBase}
              value={
                selectedPrior
                  ? { label: selectedPrior }
                  : { label: JSON.parse(selectedNodes).base }
              }
              options={base}
              isDisabled={false}
              components={{
                SingleValue: (props) => {
                  return customSingleValue(props);
                },
              }}
            />
          </div>
          <div>
            <label>{currentndf}</label>
            <Select
              styles={selectStyles}
              onChange={handleChangeCurrentNdf}
              value={
                selectedCurrentNDF
                  ? { label: selectedCurrentNDF }
                  : { label: JSON.parse(selectedNodes).currentndf }
              }
              options={compareWith}
            />
          </div>
        </>
      ) : viewby && currentndf ? (
        <>
          {selectedViewBy === "MFF" ||
          JSON.parse(selectedNodes).level === "MFF" ||
          selectedViewBy === "Brands" ||
          JSON.parse(selectedNodes).level === "Brands" ? (
            <>
              <div>
                <label>Drill Down By</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedDrillDown
                      ? { label: selectedDrillDown }
                      : { label: JSON.parse(selectedNodes).level }
                  }
                  onChange={handleChangeDrillDown}
                  options={
                    JSON.parse(selectedNodes).level == "Brands"
                      ? Brands
                      : DrillDown
                  }
                />
              </div>
              <div>
                <label>View By</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedViewBy
                      ? { label: selectedViewBy }
                      : { label: JSON.parse(selectedNodes).level }
                  }
                  onChange={handleChangeViewBy}
                  options={
                    JSON.parse(selectedNodes)["view_by"] === "MFF"
                      ? viewBy
                      : [
                          { label: "Drivers", value: "Drivers" },
                          { label: "Brands", value: "Brands" },
                        ]
                  }
                />
              </div>
              <div>
                <label>{currentndf}</label>
                <Select
                  styles={selectStyles}
                  onChange={handleChangeCurrentNdf}
                  value={
                    selectedCurrentNDF
                      ? { label: selectedCurrentNDF }
                      : { label: JSON.parse(selectedNodes).currentndf }
                  }
                  options={compareWith}
                />
              </div>
            </>
          ) : (
            <>
              <div>
                <label>View By</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedViewBy
                      ? { label: selectedViewBy }
                      : { label: JSON.parse(selectedNodes).level }
                  }
                  onChange={handleChangeViewBy}
                  options={
                    JSON.parse(selectedNodes)["view_by"] === "MFF"
                      ? viewBy
                      : [
                          { label: "Drivers", value: "Drivers" },
                          { label: "Brands", value: "Brands" },
                        ]
                  }
                />
              </div>
              <div>
                <label>{currentndf}</label>
                <Select
                  styles={selectStyles}
                  onChange={handleChangeCurrentNdf}
                  defaultValue={compareWith[0]}
                  options={compareWith}
                />
              </div>
            </>
          )}
        </>
      ) : (
        <>
          {selectedViewBy === "MFF" ||
          JSON.parse(selectedNodes).level === "MFF" ||
          selectedViewBy === "Brands" ||
          JSON.parse(selectedNodes).level === "Brands" ? (
            <>
              <div>
                <label>Drill Down By</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedDrillDown
                      ? { label: selectedDrillDown }
                      : { label: JSON.parse(selectedNodes).level }
                  }
                  onChange={handleChangeDrillDown}
                  options={
                    JSON.parse(selectedNodes).level === "Brands"
                      ? Brands
                      : DrillDown
                  }
                />
              </div>
              <div>
                <label>View By</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedViewBy
                      ? { label: selectedViewBy }
                      : { label: JSON.parse(selectedNodes).level }
                  }
                  defaultInputValue={selectedViewBy}
                  onChange={handleChangeViewBy}
                  options={
                    JSON.parse(selectedNodes)["view_by"] === "MFF"
                      ? viewBy
                      : [
                          { label: "Drivers", value: "Drivers" },
                          { label: "Brands", value: "Brands" },
                        ]
                  }
                />
              </div>
              <div>
                <label>Prior</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedPrior
                      ? { label: selectedPrior }
                      : { label: JSON.parse(selectedNodes).base }
                  }
                  onChange={handleChangeBase}
                  options={base}
                />
              </div>
              <div>
                <label>Current NDF</label>
                <Select
                  styles={selectStyles}
                  onChange={handleChangeCurrentNdf}
                  value={
                    selectedCurrentNDF
                      ? { label: selectedCurrentNDF }
                      : { label: JSON.parse(selectedNodes).currentndf }
                  }
                  options={compareWith}
                />
              </div>
            </>
          ) : viewby ? (
            <div>
              <label>View By</label>

              <Select
                styles={selectStyles}
                value={
                  selectedViewBy
                    ? { label: selectedViewBy }
                    : { label: JSON.parse(selectedNodes).level }
                }
                onChange={handleChangeViewBy}
                options={
                  JSON.parse(selectedNodes)["view_by"] === "MFF"
                    ? viewBy
                    : [
                        { label: "Drivers", value: "Drivers" },
                        { label: "Brands", value: "Brands" },
                      ]
                }
              />
            </div>
          ) : (
            <>
              <div>
                <label>View By</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedViewBy
                      ? { label: selectedViewBy }
                      : { label: JSON.parse(selectedNodes).level }
                  }
                  onChange={handleChangeViewBy}
                  options={
                    JSON.parse(selectedNodes)["view_by"] === "MFF"
                      ? viewBy
                      : [
                          { label: "Drivers", value: "Drivers" },
                          { label: "Brands", value: "Brands" },
                        ]
                  }
                />
              </div>
              <div>
                <label>Prior</label>
                <Select
                  styles={selectStyles}
                  value={
                    selectedPrior
                      ? { label: selectedPrior }
                      : { label: JSON.parse(selectedNodes).base }
                  }
                  onChange={handleChangeBase}
                  options={base}
                />
              </div>
              <div>
                <label>Current NDF</label>
                <Select
                  styles={selectStyles}
                  onChange={handleChangeCurrentNdf}
                  value={
                    selectedCurrentNDF
                      ? { label: selectedCurrentNDF }
                      : { label: JSON.parse(selectedNodes).currentndf }
                  }
                  options={compareWith}
                />
              </div>
            </>
          )}
        </>
      )}
    </div>
  );
};

export default TopDropdown;
