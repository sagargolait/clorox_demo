import React, { PureComponent } from "react";
import {
  ComposedChart,
  Line,
  Area,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  LineChart,
} from "recharts";

const data = [
  {
    name: "Jan FY20",
    uv: 590,
    pv: 800,
    amt: 1400,
    type: "bar",
  },
  {
    name: "Feb FY20",
    uv: 868,
    pv: 967,
    amt: 1506,
    type: "bar",
  },
  {
    name: "Mar FY20",
    uv: 1397,
    pv: 1098,
    amt: 989,
    type: "bar",
  },
  {
    name: "Apr FY20",
    uv: 1480,
    pv: 1200,
    amt: 1228,
    type: "bar",
  },
  {
    name: "May FY20",
    uv: 1520,
    pv: 1108,
    amt: 1100,
    type: "bar",
  },
  {
    name: "Jun FY20",
    uv: 1400,
    pv: 680,
    amt: 1700,
    type: "bar",
  },
  {
    name: "Jul FY20",
    uv: 1700,
    pv: 680,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Aug FY20",
    uv: 1700,
    pv: 680,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Sep FY20",
    uv: 500,
    pv: 980,
    amt: 1900,
    type: "bar",
  },
  {
    name: "Oct FY20",
    uv: 700,
    pv: 1180,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Nov FY20",
    uv: 900,
    pv: 680,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Dec FY20",
    uv: 700,
    pv: 680,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Jan FY21",
    uv: 1200,
    pv: 1180,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Feb FY21",
    uv: 1000,
    pv: 1090,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Mar FY21",
    uv: 1700,
    pv: 999,
    amt: 1200,
    type: "bar",
  },
  {
    name: "Apr FY21",
    uv: 1700,
    pv: 680,
    amt: 1200,
    type: "bar",
  },
  {
    name: "May FY21",
    uv: 1700,
    pv: 1800,
    amt: 1900,
    type: "bar",
  },
  {
    name: "Jun FY21",
    uv: 1500,
    pv: 1500,
    amt: 1200,
    type: "bar",
  }
];

function ShipmentConsumption() {
  return (
    <div style={{ height: "50vh" }}>
      <ResponsiveContainer width="100%" height="100%">
        <ComposedChart
          width={900}
          height={400}
          data={data}
          margin={{
            top: 20,
            right: 10,
            bottom: 20,
            left: 10,
          }}
        >
          <CartesianGrid vertical={false} stroke="#f5f5f5" />
          <XAxis
            tickMargin={15}
            dataKey="name"
            scale="band"
            axisLine={false}
            angle={-30}
            width={12}
          />
          <YAxis 
            axisLine={false} 
        />
          {/* <Tooltip /> */}
          {/* <Legend /> */}
          <Bar dataKey="pv" barSize={20} fill="#0AA2D1" />
          <Line type="linear" dataKey="uv" stroke="#ff7300" />
          <Line type="linear" dataKey="pv" stroke="#00716E" />
          <Line type="linear" dataKey="amt" stroke="#00716E" />

        </ComposedChart>
      </ResponsiveContainer>
    </div>
  );
}

export default ShipmentConsumption;
