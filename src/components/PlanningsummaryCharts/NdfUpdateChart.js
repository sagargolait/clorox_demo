import React from "react";
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  LabelList,
  Text,
} from "recharts";
import { numberWithCommas } from "../../utils/utils";

const NdfUpdateChart = ({ndf_update_data, setNdfUpdateData, headerValue, setHeaderValue, setViewType}) => {

  let MonthlyView =(month_data, month_name)=>{
    setNdfUpdateData(month_data)
    setHeaderValue(`🠔 ${month_name}`)
    setViewType("monthly")
  }

  return (
    <>
      <ResponsiveContainer width="100%" height='auto'  aspect={5}>
        <BarChart
          data={ndf_update_data}
          margin={{
            top: 20,
            right: 30,
            left: -50,
            bottom: -15,
          }}
        >
          <CartesianGrid vertical={false} stroke="#efefef" />

          <XAxis
            dataKey="name"
            interval={0}
            tickLine={false}
            axisLine={false}
            tick={CustomizedAxisTick}
          />  
          <YAxis
            type="number"
            axisLine={false}
            tickLine={false}
            scale={"linear"}
            tick={{ fill: "transparent", strokeWidth: 0 }}
            domain={[0, (dataMax) => dataMax * 1.5]}
          />
        {/* <Tooltip position={{ y: -80 }} cursor={false} content={<CustomTooltip />} /> */}
          <Bar dataKey="uv" fill="#82ca9d" barSize={20}
          onClick={(data)=>{return (Array.isArray(data.month) ? MonthlyView(data.month, data.name): null)}}>
            {ndf_update_data.map((item, index) => {
              if (index === 0) {
                return <Cell key={index} fill="#0AA2D1" />;
              } else {
                return <Cell key={index} fill="#0AC7C2" />;
              }
            })}
            <LabelList
              dataKey="uv"
              content={renderCustomizedLabel}
              position="top"
              style={{ fontSize: "0.7em" }}
            />
          </Bar>
        </BarChart>
      </ResponsiveContainer>
    </>
  );
};



const CustomizedAxisTick = (props) => {
  const { x, y, payload } = props;

  return (
    <Text
      style={{
        fontFamily: "Lato",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "0.7em",
        lineHeight: "11px",
        color: "#999999",
        opacity: "0.5",
      }}
      x={x}
      y={y}
      width={300}
      textAnchor="middle"
      verticalAnchor="start"
    >
      {payload.value ? payload.value: "loading.."}
    </Text>
  );
};

const renderCustomizedLabel = (props) => {
  const { x, y, width, height, value } = props;

  const radius = 10;

  return (
    <g>
      <text
        x={x + width / 2}
        y={y - radius}
        style={{
          fontFamily: "Lato",
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "0.7em",
          lineHeight: "11px",
          color: "#999999",
          opacity: "0.8",
        }}
        textAnchor="middle"
        dominantBaseline="middle"
      >
        {numberWithCommas((Number.parseInt(value/1000)))}
      </text>
    </g>
  );
};

const CustomizedLabel = (props) => {
  return (
    <text
      x={props.x}
      y={props.y}
      dy={-20}
      dx={20}
      fill={props.stroke}
      fontSize={12}
      fontFamily="Lato"
      textAnchor="middle"
      opacity={0.5}
    >
      {numberWithCommas(props.value)}
    </text>
  );
};


const getIntroOfPage = (label) => {
  if (label === 'Page A') {
    return "Page A is about men's clothing";
  }
  if (label === 'Page B') {
    return "Page B is about women's dress";
  }
  if (label === 'Page C') {
    return "Page C is about women's bag";
  }
  if (label === 'Page D') {
    return 'Page D is about household goods';
  }
  if (label === 'Page E') {
    return 'Page E is about food';
  }
  if (label === 'Page F') {
    return 'Page F is about baby food';
  }
  return '';
};

const CustomTooltip = ({ active, payload, label }) => {
  if (active && payload && payload.length) {
    return (
      <div className="mytooltip">
        <p className="label">{`${label} : ${payload[0].value}`}</p>
        <p className="intro">{getIntroOfPage(label)}</p>
        <p className="desc">Anything you want can be displayed here.</p>
      </div>
    );
  }

  return null;
};


export default NdfUpdateChart;
