import React, { PureComponent } from 'react';
import {
  ComposedChart,
  Line,
  Area,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  LineChart,
} from 'recharts';

const data = [
  {
    name: 'FY21 Jul',
    uv: 590,
    pv: 800,
    amt: 1400,
    type:'bar'
  },
  {
    name: 'FY21 Aug',
    uv: 868,
    pv: 967,
    amt: 1506,
    type:'bar'
  },
  {
    name: 'FY21 Sep',
    uv: 1397,
    pv: 1098,
    amt: 989,
    type:'bar'
  },
  {
    name: 'FY21 Oct',
    uv: 1480,
    pv: 1200,
    amt: 1228,
    type:'bar'
  },
  {
    name: 'FY21 Nov',
    uv: 1520,
    pv: 1108,
    amt: 1100,
    type:'bar'
  },
  {
    name: 'FY21 Dec',
    uv: 1400,
    pv: 680,
    amt: 1700,
    type:'bar'
  },
  {
    name: 'FY21 Jan',
    uv: 1700,
    pv: 680,
    amt: 1200,
    type:'bar'
  }
  //,
  // {
  //   name: 'Series 1',
  //   data: [
  //     { category: 'Page A', value: 10 },
  //     { category: 'Page B', value: 20 },
  //     { category: 'Page C', value: 10},
  //     { category: 'Page D', value: 100},
  //   ],
  //   stroke: "#0AA2D1",
  //   type:'line'
  // },
  // {
  //   name: 'Series 2',
  //   data: [
  //     { category: 'Page A', value: 50 },
  //     { category: 'Page B', value: 50 },
  //     { category: 'Page C', value: 50},
  //     { category: 'Page D', value: 100},
  //   ],
  //   stroke: "#0AA2D1",
  //   type:'line'
  // }
];

function KeyMetricsOvertimeChart(){
    return (
      <div style={{height:'50vh'}}>
      <ResponsiveContainer width="100%" height="100%">
        <ComposedChart
          width={500}
          height={400}
          data={data}
          margin={{
            top: 20,
            right: 80,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid vertical={false} stroke="#f5f5f5" />
          <XAxis angle={-30} tickMargin={15} dataKey="name" scale="band" axisLine={false} />
          <YAxis axisLine={false}/>
          {/* <Tooltip /> */}
          {/* <Legend /> */}
          <Bar dataKey="pv" barSize={20} fill="#0AC7C2" />
          <Bar dataKey="uv" barSize={20} fill="#B3EAEA" />
          <Line type="linear" dataKey="uv" stroke="#ff7300" />
        </ComposedChart>
      </ResponsiveContainer>
      </div>
    );
}

export default KeyMetricsOvertimeChart
