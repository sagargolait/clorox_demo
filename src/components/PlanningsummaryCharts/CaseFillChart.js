import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, CartesianAxis, ReferenceLine } from 'recharts';



function CaseFillChart({data}){
    return (
      <div style={{height:'50vh'}}>
      <ResponsiveContainer width="99%" height="100%">
        <LineChart width={500} height={300}>
          <CartesianGrid vertical={false} stroke="#EFEFEF"/>
          <ReferenceLine y={96} stroke="#BDBDBD" strokeWidth={0.5}/>
          <ReferenceLine y={90} stroke="#BDBDBD"strokeWidth={0.5} />
          <XAxis dataKey="category" type="category" tickMargin={15} style={{fontSize:'12px', lineBreak:'auto'}} tickSize={0} allowDuplicatedCategory={false} allowDataOverflow={false} axisLine={false}/>
          <YAxis type="number" domain={[50, 100]} tickMargin={30} style={{fontSize:'12px'}}  tickSize={0} unit="%" axisLine={false}/>
          {/* <Tooltip /> */}
          <Legend verticalAlign="top" height={36} iconSize={15} iconType="rect"/>
          {
            data.map((s,i) => {
                return <Line type="linear" strokeWidth={2} legendType="rect" dot={false} dataKey="value" data={s.data} name={s.name} key={s.name} stroke={s.stroke}/>
              }
          )}
        </LineChart>
      </ResponsiveContainer>
      </div>
    )
}

export default CaseFillChart