import React from "react";
import { useTable } from "react-table";
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { ShipmentConsumptionTableData } from "../../data/ShipmentConsumptionTableData";

const ShipmentConsumptionTable = ({ data }) => {
  const StyledTableRow = withStyles((theme) => ({}))(TableRow);

  const StyledTableCellBody1 = withStyles((theme) => ({
    root: {
      borderBottom: "1px solid #eee",
    },
    body: {
      fontSize: 14,
      fontFamily: "Lato",
      fontWeight: "700",
      lineHeight: "14px",
      color: "#000000",
      borderBottom: "none",
      "&:first-child": {
        fontSize: "14px",
        fontFamily: "Lato",
        fontWeight: "normal",
        borderRight: "1px solid #DDDDDD",
        lineHeight: "14px",
        textAlign: "bottom",
        color: "#666666",
      },
      "&:nth-child(2)": {
        fontSize: "14px",
        fontFamily: "Lato",
        borderRight: "1px solid #DDDDDD",
        fontWeight: "normal",
        lineHeight: "14px",
        color: "#666666",
      },
      "&:nth-child(3)": {
        fontSize: "14px",
        fontFamily: "Lato",
        borderRight: "1px solid #DDDDDD",
        fontWeight: "normal",
        lineHeight: "14px",
        color: "#666666",
      },
    },
  }))(TableCell);

  // const data1 = ShipmentConsumptionTableData;

  // let head1 = Object.keys(data1[0][0]);

  // console.log(head1);

  // head1 = head1.filter((x) => {
  //   return (
  //     x !== "account" &&
  //     x !== "current_ndf" &&
  //     x !== "category" &&
  //     x !== "viewType"
  //   );
  // });

  // console.log(head1);

  let testing_head = Object.keys(data[0]);
  testing_head = testing_head.filter((x) => {
    return (
      x != "account" &&
      x !== "current_ndf" &&
      x !== "category" &&
      x !== "viewType"
    );
  });

  testing_head.unshift("account", "category", "current_ndf");
  let head = testing_head;

  let columns = React.useMemo(
    () =>
      head.map((x, i) => {
        if (x == "account") {
          return {
            id: "null",
            Header: "",
            width: 95,
            style: { "white-space": "unset" },
            fixed: "left",
            accessor: "account",
            Cell: (row) => {
              return (
                <div
                  style={{
                    textAlign: "left",
                    fontWeight: "bold",
                    paddingTop: "126%",
                    color: "#666666",
                  }}
                >
                  {row.row.id % 2 !== 0 ? " " : row.value}
                </div>
              );
            },
          };
        } else if (x == "category") {
          return {
            id: "null1",
            Header: "",
            accessor: "category",
          };
        } else if (x == "current_ndf") {
          return {
            Header: "Current_NDF",
            accessor: "current_ndf",
            Cell: ({ value }) => {
              return <div>{Math.trunc(parseInt(value))}</div>;
            },
          };
        } else {
          return {
            Header: x,
            accessor: x,
            Cell: ({ value }) => (
              <div>
                {Array.isArray(value) ? Math.trunc(value[0]) : value}
                <br />
                <div
                  className={value[1] >= 100 ? "doGreen" : "doRed"}
                  style={{
                    textAlign: "right",
                    paddingTop: "10px",
                    paddingRight: "13px",
                  }}
                >
                  <span>{Math.trunc(value[1])}</span>
                </div>
              </div>
            ),
          };
        }
      }),

    []
  );

  const cl1 = React.useMemo(
    () => [
      {
        id: "account",
        Header: "",
        accessor: "account",
        Cell: (row) => {
          return (
            <div
              style={{
                textAlign: "left",
                fontWeight: "bold",
                color: "#666666",
              }}
            >
              {row.id % 2 === 0 ? " " : row.value}
            </div>
          );
        },
      },
      {
        id: "category",
        Header: "",
        accessor: "category",
      },
      {
        Header: "Current_NDF",
        accessor: "current_ndf",
      },
      {
        Header: "Quarters",
        accessor: (data) => data.quarter.map((i) => {}),
      },
    ],

    []
  );

  // let cl2 = [];

  // console.log(data1);

  // data1.map((item) => {
  //   item.forEach((header) => {
  //     head1.map((i, index) => {
  //       console.log(header[i]);
  //       cl2.push({
  //         Header: i,
  //         columns: Object.keys(header[i]).map((j) => {
  //           return [
  //             {
  //               Header: j,
  //               accessor: j,
  //               Cell: (row) => {
  //                 console.log(row);
  //               },
  //             },
  //           ];
  //         }),
  //       });
  //     });
  //   });
  // });

  // const mainCl = React.useMemo(() => cl1.concat(cl2), []);
  // console.log(mainCl);

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data,
    });

  // Render the UI for your table
  return (
    <div style={{ overflowX: "auto" }}>
      <MaUTable
        {...getTableProps()}
        style={{
          width: "100%",
          border: "0 !important",
        }}
      >
        <TableHead style={{ background: "#F1F1F1" }}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <TableCell
                  {...column.getHeaderProps()}
                  style={{ padding: "12px" }}
                >
                  {column.render("Header")}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <StyledTableRow {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <StyledTableCellBody1 {...cell.getCellProps()}>
                      {cell.render("Cell")}
                    </StyledTableCellBody1>
                  );
                })}
              </StyledTableRow>
            );
          })}
        </TableBody>
      </MaUTable>
    </div>
  );
};

export default ShipmentConsumptionTable;
