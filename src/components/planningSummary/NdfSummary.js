import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import { numberWithCommas } from "../../utils/utils";
import Table from "../../assets/logos/table.png";
import Chart from "../../assets/logos/chart.png";
import outlineChart from "../../assets/logos/outlineChart.png";
import outlineTable from "../../assets/logos/outlineTable.png";
import { MdFileDownload, MdCropFree } from "react-icons/md";
import NdfUpdateChart from "../PlanningsummaryCharts/NdfUpdateChart";
import WaterfallChart from "../charts/WaterfallChart";
// import { chartData_ByDrivers } from "./data/ndf_by_driver";
// import { chartData_ByCustomer } from "./data/ndf_by_customer";
// import { chartData_ByBrand } from "./data/ndf_by_brand";
import NdfSummaryTable from "./NdfSummaryTable";
// import { ndf_update_data_dummy } from "./data/ndf_update.js";
import UserFilters from "../../contexts/UserFilters";
import TimeFilter from "../../contexts/TimeFilter";
import FullScreenModal from "../../sharedComponents/FullScreenModal";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import "./NdfSummary.css";
import {URL, PORT} from '../../Config'

const NdfSummary = ({
  tableSelected,
  setTableSelected,
  chartSelected,
  setChartSelected,
}) => {
  let filterData = sessionStorage.getItem("IbpStore_allFiltersSelected");
  const { timeFilter, setTimeFilter } = React.useContext(TimeFilter);
  const { fil, setFil } = React.useContext(UserFilters);
  const [allFilters, setAllFilters] = React.useState({ ...timeFilter, ...fil });

  const [open, setOpen] = React.useState(false);
  const [comp_for_modal, setCompForModal] = React.useState("");
  const [data_for_modal, setDataForModal] = React.useState([]);

  const [headerValue, setHeaderValue] = React.useState("NDF UPDATE");
  let [original_ndfupdate_data, setOriginal_NdfUpdateData] = React.useState([]);
  const [ndf_update_data, setNdfUpdateData] = React.useState([]);
  const [viewType, setViewType] = React.useState("quater");

  const [chartData_ByDrivers, setChartData_ByDrivers] = React.useState([]);
  const [chartData_ByCustomer, setChartData_ByCustomer] = React.useState([]);
  const [chartData_ByBrand, setChartData_ByBrand] = React.useState([]);

  const [kpi_data, setKpiData] = React.useState("");

  let calculated_table_delta = [];
  let calculated_table_index = [];

  let allfiltersSelected = JSON.parse(
    window.sessionStorage.getItem("IbpStore_allFiltersSelected")
  );

  const handle_ndf_update = useFullScreenHandle();
  const handle_bycustomer = useFullScreenHandle();
  const handle_bydriver = useFullScreenHandle();
  const handle_bymff = useFullScreenHandle();

  const [isFullScreen, setIsFullScreen] = React.useState(false);

  // useEffect(()=>{
  //   Promise.all([
  //     axios({
  //         method: "post",
  //         url: "`${URL}/summary/ndf_update",
  //         data: sessionStorage["IbpStore_allFiltersSelected"],
  //         headers: {
  //           "Access-Control-Allow-Origin": "*",
  //           // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  //         },
  //       }),

  //     axios({
  //         method: "post",
  //         url: `${URL}/summary/bydriver/waterfall_charts",
  //         data: sessionStorage["IbpStore_allFiltersSelected"],
  //         headers: {
  //           "Access-Control-Allow-Origin": "*",
  //           // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  //         },
  //       }),

  //     axios({
  //         method: "post",
  //         url: `${URL}/summary/bycustomer/waterfall_charts",
  //         data: sessionStorage["IbpStore_allFiltersSelected"],
  //         headers: {
  //           "Access-Control-Allow-Origin": "*",
  //           // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  //         },
  //       }),

  //     axios({
  //         method: "post",
  //         url: `${URL}/summary/bybrand/waterfall_charts",
  //         data: JSON.stringify({
  //           ...allFilters,
  //           level: "MFF",
  //           drilldown: "MFF",
  //         }),
  //         headers: {
  //           "Access-Control-Allow-Origin": "*",
  //           // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  //         },
  //       }),

  //     axios({
  //         method: "post",
  //         url: `${URL}/summary/kpi",
  //         data: sessionStorage["IbpStore_allFiltersSelected"],
  //         headers: {
  //           "Access-Control-Allow-Origin": "*",
  //           // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  //         },
  //       })

  //   ]). then(([data1, data2, data3, data4, data5])=>{
  //           setNdfUpdateData(data1.data);
  //           setOriginal_NdfUpdateData(data1.data);

  //           setChartData_ByDrivers(data2.data);

  //           setChartData_ByCustomer(data3.data);

  //           setChartData_ByBrand(data4.data);

  //           setKpiData(data5.data);
  //   })

  // return(()=>{
  //   setNdfUpdateData([]);
  //     setOriginal_NdfUpdateData([]);
  //     setChartData_ByDrivers([]);
  //     setChartData_ByCustomer([]);
  //     setChartData_ByBrand([]);
  //     setKpiData([]);
  // })
  // },[filterData])

  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/ndf_update`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        setNdfUpdateData(response.data);
        setOriginal_NdfUpdateData(response.data);
        // tableCalculation(response.data)
        console.log("............");
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setNdfUpdateData([]);
      setOriginal_NdfUpdateData([]);
    };
  }, [filterData]);

  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/bydriver/waterfall_charts`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        setChartData_ByDrivers(response.data);
        console.log("............");
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setChartData_ByDrivers([]);
    };
  }, [filterData]);

  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/bycustomer/waterfall_charts`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        setChartData_ByCustomer(response.data);
        console.log("............");
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setChartData_ByCustomer([]);
    };
  }, [filterData]);

  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/bybrand/waterfall_charts`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        setChartData_ByBrand(response.data);
        console.log("............");
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setChartData_ByBrand([]);
    };
  }, [filterData]);

  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/kpi`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        setKpiData(response.data);
        console.log("............");
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setKpiData([]);
    };
  }, [filterData]);

  let handleHeaderValue = () => {
    setNdfUpdateData(original_ndfupdate_data);
    setHeaderValue("NDF UPDATE");
    setViewType("quater");
  };

  let tableCalculation = () => {
    calculated_table_delta = [];
    calculated_table_delta = [];
    let delta;
    let index_val;

    original_ndfupdate_data.map((data, index) => {
      if (index == 0) {
        calculated_table_delta.push("Delta vs YA");
        calculated_table_index.push("Index vs YA");
        delta = data.uv;
      } else {
        calculated_table_delta.push(((data.uv - delta) / 1000).toFixed(0));
        calculated_table_index.push(((data.uv / delta) * 100).toFixed(0));
      }
    });
    // console.log(calculated_table_delta)
    // console.log(calculated_table_index)
  };

  const FullScreenForNDF = () => {
    !isFullScreen ? handle_ndf_update.enter() : handle_ndf_update.exit();
    console.log(isFullScreen);
    setIsFullScreen(!isFullScreen);
  };
  const FullScreenForByDriver = () => {
    !isFullScreen ? handle_bydriver.enter() : handle_bydriver.exit();
    console.log(isFullScreen);
    setIsFullScreen(!isFullScreen);
  };

  const FullScreenForByCustomer = () => {
    !isFullScreen ? handle_bycustomer.enter() : handle_bycustomer.exit();
    console.log(isFullScreen);
    setIsFullScreen(!isFullScreen);
  };
  const FullScreenForByMFF = () => {
    !isFullScreen ? handle_bymff.enter() : handle_bymff.exit();
    console.log(isFullScreen);
    setIsFullScreen(!isFullScreen);
  };

  return (
    <div>
      <div className="ndf-header" style={{ marginTop: "0px" }}>
        <div style={{ marginRight: "30%" }}>
          {kpi_data != "" ? (
            <div className="ndf-header-content">
              <label>
                Current NDF{" "}
                <span>
                  {numberWithCommas((kpi_data.Current_NDF / 1000).toFixed(0))}
                </span>
              </label>
              <label>
                Index <span style={{ color: "#15CF6B" }}>{kpi_data.Index}</span>
              </label>
              <label>
                Variance vs Prior{" "}
                <span>
                  {numberWithCommas(
                    (kpi_data.Variance_vs_LRP / 1000).toFixed(0)
                  )}
                </span>
              </label>
            </div>
          ) : (
            <h4>Loading...</h4>
          )}
        </div>
        <div style={{ display: "flex" }}>
          <div>
            {tableSelected ? (
              <MdFileDownload
                size={"1.1rem"}
                style={{ color: "#005CB9", marginRight: "10px" }}
              />
            ) : null}
          </div>
          <div
            onClick={() => {
              return setChartSelected(!chartSelected), setTableSelected(false);
            }}
            className={`${chartSelected ? "icon-styl-selected" : "icon-styl"}`}
          >
            {chartSelected ? (
              <img src={outlineChart} alt="chart" />
            ) : (
              <img src={Chart} alt="chart" />
            )}
          </div>
        </div>
        <div
          onClick={() => {
            return setTableSelected(!tableSelected), setChartSelected(false);
          }}
          className={`${tableSelected ? "icon-styl-selected" : "icon-styl"}`}
        >
          {tableSelected ? (
            <img src={outlineTable} alt="table" />
          ) : (
            <img src={Table} alt="table" />
          )}
        </div>
      </div>
      {chartSelected ? (
        <div style={{ height: "110%" }}>
          {tableCalculation()}
          <div className="summary-content">
            <div className="summary-box">
              <FullScreen handle={handle_ndf_update}>
                <div className="summary-box-header">
                  <button
                    style={{ background: "transparent", border: "0" }}
                    onClick={handleHeaderValue}
                  >
                    <span>{headerValue}</span>
                  </button>
                  <div style={{ cursor: "pointer" }}>
                    <MdFileDownload
                      style={{ marginRight: "10px" }}
                      color={"#005CB9"}
                    />
                    <MdCropFree color={"#005CB9"} onClick={FullScreenForNDF} />
                  </div>
                </div>

                <NdfUpdateChart
                  ndf_update_data={ndf_update_data}
                  setNdfUpdateData={setNdfUpdateData}
                  headerValue={headerValue}
                  setHeaderValue={setHeaderValue}
                  setViewType={setViewType}
                />
                {
                  <>
                    <div
                      style={{ marginLeft: "30px", width: "91%" }}
                      className={viewType == "quater" ? "display" : "nodisplay"}
                    >
                      <table
                        style={{
                          marginBottom: "5px",
                          width: "100%",
                          // border: "1px solid",
                        }}
                      >
                        <tbody className="mini-table-body">
                          <tr className="tr1-styl">
                            {calculated_table_delta != [] ? (
                              calculated_table_delta.map((d) => {
                                return <td>{d}</td>;
                              })
                            ) : (
                              <h5>Loading...</h5>
                            )}
                          </tr>
                          <tr>
                            {calculated_table_delta != [] ? (
                              calculated_table_index.map((d) => {
                                return (
                                  <td className={d < 100 ? "doRed" : "doGreen"}>
                                    {d}
                                  </td>
                                );
                              })
                            ) : (
                              <h5>Loading...</h5>
                            )}
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </>
                }
              </FullScreen>
            </div>
            <div className="summary-box">
              <FullScreen handle={handle_bydriver}>
                <div className="summary-box-header">
                  <button style={{ background: "transparent", border: "0" }}>
                    <span>By Driver</span>
                  </button>
                  <div style={{ cursor: "pointer" }}>
                    <MdFileDownload
                      style={{ marginRight: "10px" }}
                      color={"#005CB9"}
                    />
                    <MdCropFree
                      color={"#005CB9"}
                      onClick={FullScreenForByDriver}
                    />
                  </div>
                </div>

                <WaterfallChart
                  data={chartData_ByDrivers}
                  aspectRatio={3.7}
                  divideBy={1000}
                />
              </FullScreen>
            </div>
            {/* </div> */}

            {/* <div className="summary-content"> */}
            <div className="summary-box">
              <FullScreen handle={handle_bycustomer}>
                <div className="summary-box-header">
                  <button style={{ background: "transparent", border: "0" }}>
                    <span>By Customer</span>
                  </button>
                  <div style={{ cursor: "pointer" }}>
                    <MdFileDownload
                      style={{ marginRight: "10px" }}
                      color={"#005CB9"}
                    />
                    <MdCropFree
                      color={"#005CB9"}
                      onClick={FullScreenForByCustomer}
                    />
                  </div>
                </div>
                <WaterfallChart
                  data={chartData_ByCustomer}
                  aspectRatio={3.8}
                  divideBy={1000}
                />
              </FullScreen>
            </div>
            <div className="summary-box">
              <FullScreen handle={handle_bymff}>
                <div className="summary-box-header">
                  <button style={{ background: "transparent", border: "0" }}>
                    <span>{"By" + " " + allfiltersSelected["view_by"]}</span>
                  </button>
                  <div style={{ cursor: "pointer" }}>
                    <MdFileDownload
                      style={{ marginRight: "10px" }}
                      color={"#005CB9"}
                    />
                    <MdCropFree
                      color={"#005CB9"}
                      onClick={FullScreenForByMFF}
                    />
                  </div>
                </div>

                <WaterfallChart
                  data={chartData_ByBrand}
                  aspectRatio={3.8}
                  divideBy={1000}
                />
              </FullScreen>
            </div>
          </div>
        </div>
      ) : (
        <NdfSummaryTable />
      )}
    </div>
  );
};

export default NdfSummary;
