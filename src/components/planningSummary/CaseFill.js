import React from "react";
import { numberWithCommas } from "../../utils/utils";
import Table from "../../assets/logos/table.png";
import Chart from "../../assets/logos/chart.png";
import outlineChart from "../../assets/logos/outlineChart.png";
import outlineTable from "../../assets/logos/outlineTable.png";
import { MdFileDownload, MdCropFree } from "react-icons/md";
import CaseFillChart from "../PlanningsummaryCharts/CaseFillChart";
import data from './data/monthly_fill.json'
import data_mff from './data/monthly_fill_mff.json'

const CaseFill = ({
  tableSelected,
  setTableSelected,
  chartSelected,
  setChartSelected,
}) => {
  return (
    <div>
      <div className="ndf-header">
      </div>
      <div className="summary-content">
        <div className="summary-box">
          <div className="summary-box-header">
            <span>MONTHLY CASE FILL</span>
            <div style={{ cursor: "pointer" }}>
              <MdFileDownload
                style={{ marginRight: "10px" }}
                color={"#005CB9"}
              />
              <MdCropFree color={"#005CB9"} />
            </div>
          </div>
          <CaseFillChart data={data} />
        </div>
        <div className="summary-box">
          <div className="summary-box-header">
            <span>MONTHLY CASE FILL BY MFF</span>
            <div style={{ cursor: "pointer" }}>
              <MdFileDownload
                style={{ marginRight: "10px" }}
                color={"#005CB9"}
              />
              <MdCropFree color={"#005CB9"} />
            </div>
          </div>
          <CaseFillChart data={data_mff}/>
        </div>
      </div> 
    </div>
  );
};

export default CaseFill;
