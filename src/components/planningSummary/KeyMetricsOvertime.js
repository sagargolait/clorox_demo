import React from "react";
import { numberWithCommas } from "../../utils/utils";
import Table from "../../assets/logos/table.png";
import Chart from "../../assets/logos/chart.png";
import outlineChart from "../../assets/logos/outlineChart.png";
import outlineTable from "../../assets/logos/outlineTable.png";
import { MdFileDownload, MdCropFree } from "react-icons/md";
import KeyMetricsOvertimeChart from "../PlanningsummaryCharts/KeyMetricsOvertimeChart";

const KeyMetricsOvertime = ({
  tableSelected,
  setTableSelected,
  chartSelected,
  setChartSelected,
}) => {
  return (
    <div>
      <div>
        <div className="summary-box" style={{ width: "100%" }}>
          <div className="summary-box-header">
            <span>KEY METRICS OVERTIME</span>
            <div style={{ cursor: "pointer" }}>
              <MdFileDownload
                style={{ marginRight: "10px" }}
                color={"#005CB9"}
              />
              <MdCropFree color={"#005CB9"} />
            </div>
          </div>
          <KeyMetricsOvertimeChart />
        </div>

        <div className="summary-box" style={{ width: "100%" }}>
          <div className="summary-box-header">
            <span>KEY METRICS OVERTIME</span>
          </div>
          {/* <KeyMetricsOvertimeChart /> */}
        </div>
      </div>
    </div>
  );
};

export default KeyMetricsOvertime;
