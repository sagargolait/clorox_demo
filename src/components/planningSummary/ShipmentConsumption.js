import React, { useEffect } from "react";
import { numberWithCommas } from "../../utils/utils";
import Table from "../../assets/logos/table.png";
import Chart from "../../assets/logos/chart.png";
import outlineChart from "../../assets/logos/outlineChart.png";
import outlineTable from "../../assets/logos/outlineTable.png";
import ConsumptionAndShipmentChart from "../charts/ConsumptionAndShipmentChart";
import TopDropdown from "../TopDropdown";
import SharedTable from "../../sharedComponents/SharedTable";
import Spinner from "../../utils/Spinner";
import {
  ShipmentConsumptionTableData,
  newShipmentdata,
} from "../../data/ShipmentConsumptionTableData";
import ConsumptionData from "../charts/ConsumptionChartData.json";
import axios from "axios";
import { URL, PORT } from "../../Config";
import ShipmentConsumptionTable from "./ShipmentConsumptionTable";

const ShipmentConsumption = ({
  tableSelected,
  setTableSelected,
  chartSelected,
  setChartSelected,
}) => {
  const filters = JSON.parse(
    window.sessionStorage.getItem("IbpStore_allFiltersSelected")
  );

  const [data, setData] = React.useState(null);
  const [consumptionChartData, setConsumptionChartData] = React.useState(null);

  useEffect(() => {
    axios
      .post(
        `${URL}/summary/shipment_consumption_chart`,
        sessionStorage["IbpStore_allFiltersSelected"]
      )
      .then((resp) => setConsumptionChartData(resp.data))
      .catch((err) => console.log(err));
  }, [sessionStorage["IbpStore_allFiltersSelected"]]);

  useEffect(() => {
    axios
      .post(
        `${URL}/summary/shipment_consumption_table`,
        sessionStorage["IbpStore_allFiltersSelected"]
      )
      .then((resp) => setData(resp.data))
      .catch((err) => console.log(err));
  }, [sessionStorage["IbpStore_allFiltersSelected"]]);

  let calculated_table_delta = [];
  let calculated_table_index = [];

  const tableCalculation = () => {
    let delta1;
    let delta2;
    consumptionChartData.map((item, index) => {
      if (index == 0) {
        delta1 = item.consumption;
        delta2 = item.shipment;
        calculated_table_delta.push("Shipment vs. YA");
        calculated_table_index.push("Consumption vs. YA");
        calculated_table_delta.push(
          ((item.last_year_shipment / delta1) * 100).toFixed(0)
        );
        calculated_table_index.push(
          ((item.last_year_consumption / delta2) * 100).toFixed(0)
        );
      } else {
        calculated_table_delta.push(
          ((item.last_year_shipment / delta1) * 100).toFixed(0)
        );
        calculated_table_index.push(
          ((item.last_year_consumption / delta2) * 100).toFixed(0)
        );
      }
    });
  };

  return (
    <div>
      <div>
        <div className="summary-box" style={{ width: "100%" }}>
          <div className="summary-box-header">
            <span>VOLUME DECOMPOSITION - {filters.level}</span>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              padding: "0.5rem",
              alignItem: "center",
            }}
          >
            <div style={{ display: "flex" }}>
              <div>
                {tableSelected ? <TopDropdown viewby={"viewBy"} /> : null}
              </div>
              {chartSelected ? (
                <div style={{ marginRight: "10px" }}>
                  <button
                    style={{
                      background: "#FFFFFF",
                      outline: "none",
                      border: " none",
                      boxShadow:
                        "0px 0px 10px rgba(0, 80, 115, 0.05), 0px 5px 10px rgba(0, 80, 115, 0.05)",
                      borderRight: "1px solid #eee",
                    }}
                  >
                    Weekly
                  </button>
                  <button
                    style={{
                      background: "#FFFFFF",
                      outline: "none",
                      border: " none",
                      boxShadow:
                        "0px 0px 10px rgba(0, 80, 115, 0.05), 0px 5px 10px rgba(0, 80, 115, 0.05)",
                    }}
                  >
                    Monthly
                  </button>
                </div>
              ) : null}
              <div
                onClick={() => {
                  return (
                    setChartSelected(!chartSelected), setTableSelected(false)
                  );
                }}
                className={`${
                  chartSelected ? "icon-styl-selected" : "icon-styl"
                }`}
              >
                {chartSelected ? (
                  <img src={outlineChart} alt="chart" />
                ) : (
                  <img src={Chart} alt="chart" />
                )}
              </div>
            </div>
            <div
              onClick={() => {
                return (
                  setTableSelected(!tableSelected), setChartSelected(false)
                );
              }}
              className={`${
                tableSelected ? "icon-styl-selected" : "icon-styl"
              }`}
            >
              {tableSelected ? (
                <img src={outlineTable} alt="table" />
              ) : (
                <img src={Table} alt="table" />
              )}
            </div>
          </div>
          {chartSelected ? (
            <>
              {consumptionChartData !== null ? tableCalculation() : null}
              {consumptionChartData !== null ? (
                <ConsumptionAndShipmentChart data={consumptionChartData} />
              ) : (
                <>
                  <Spinner msg="Loading Chart" />
                </>
              )}
              <div>
                <table
                  style={{
                    marginBottom: "5px",
                    width: "100%",
                    // border: "1px solid",
                  }}
                >
                  <tbody className="consumption-mini-table-body">
                    <tr className="consumption-tr1-styl">
                      {calculated_table_delta != [] ? (
                        calculated_table_delta.map((d) => {
                          return (
                            <td className={d < 100 ? "doRed" : "doGreen"}>
                              {d}
                            </td>
                          );
                        })
                      ) : (
                        <h5>Loading...</h5>
                      )}
                    </tr>
                    <tr>
                      {calculated_table_delta != [] ? (
                        calculated_table_index.map((d) => {
                          return (
                            <td className={d < 100 ? "doRed" : "doGreen"}>
                              {d}
                            </td>
                          );
                        })
                      ) : (
                        <h5>Loading...</h5>
                      )}
                    </tr>
                  </tbody>
                </table>
              </div>
            </>
          ) : (
            <>
              {data !== null ? (
                <ShipmentConsumptionTable data={data[0]} />
              ) : (
                <>
                  <Spinner msg="Loading Table" />
                </>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default ShipmentConsumption;
