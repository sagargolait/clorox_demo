import React from 'react'
import { useTable } from 'react-table'
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import * as Utils from '../../utils/utils.js'
// import tableview_data from './data/tableview'


export default function TableView({data}) {
  const StyledTableRow = withStyles((theme) => ({
    root: {
      
      '&:nth-of-type(odd)': {
        backgroundColor: "#f7f7f7e3",
      },
    },
  }))(TableRow);

  const StyledTableCell = withStyles((theme) => ({
    root: {
      padding:'7px !important'
    },
    body: {
      color:'#828282',
      '&:first-child': {
        // background: 'black',
        borderRight:'0.6px solid #DDDDDD'
    },
   
  }
  }))(TableCell);


    const columns = React.useMemo(
        () => Object.keys(data[0]).map(k=>{
          if(k=="title"){
            return{
              Header:"",
              accessor:k
            }
          }
          else{
            return{
              Header:k,
              accessor:k,
              Cell: ({ value }) =>
              Utils.numberWithCommas((Number(value) / 1000).toFixed(0)),
            }
          }
          
        }),
        []
      )
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      rows,
      prepareRow,
    } = useTable({
      columns,
      data,
    })
  
    // Render the UI for your table
    return (
  
      <MaUTable {...getTableProps()} style={{width:'100%'}}>
        <TableHead style={{background:'#F1F1F1',}}>
          {headerGroups.map(headerGroup => (
            <TableRow {...headerGroup.getHeaderGroupProps()} >
              {headerGroup.headers.map(column => (
                <TableCell {...column.getHeaderProps()} style={{padding:'12px'}}>{column.render('Header')}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row)
            return (
              <StyledTableRow  {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <StyledTableCell {...cell.getCellProps()}>{cell.render('Cell')}</StyledTableCell>
                })}
              </StyledTableRow >
            )
          })}
        </TableBody>
      </MaUTable>

    )
  }