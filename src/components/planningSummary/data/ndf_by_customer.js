export const chartData_ByCustomer = [
  {
    label: "FY21 Mar Fcst",
    uv: "51853",
    pv: "0",
  },
  {
    label: "Others",
    uv: "1796",
    pv: "51853",
  },
  {
    label: "Dollar General",
    uv: "1000",
    pv: "52458",
  },
  {
    label: "Kroger",
    uv: "-800",
    pv: "52949",
  },
  {
    label: "Wallmart",
    uv: "-230",
    pv: "52253",
  },
  {
    label: "FY21 May Fcst",
    uv: "53639",
    pv: "0",
  }
];
