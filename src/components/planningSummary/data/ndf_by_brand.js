export const chartData_ByBrand = [
    {
      label: "FY21 Mar Fcst",
      uv: "51853",
      pv: "0",
    },
    {
      label: "Fresh Step",
      uv: "1993",
      pv: "51853",
    },
    {
      label: "Ever Clear",
      uv: "300",
      pv: "52458",
    },
    {
      label: "Scoop Away",
      uv: "-207",
      pv: "52949",
    },
    {
      label: "FY21 May Fcst",
      uv: "53639",
      pv: "0",
    }
  ];
  