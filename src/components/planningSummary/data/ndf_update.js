const ndf_update_data_dummy = [
    {
       "name":"FY20 Actuals",
       "uv":51853,
       "month":[
          {
             "name":"Jul",
             "uv":12000
          },
          {
             "name":"Aug",
             "uv":15000
          },
          {
             "name":"Sept",
             "uv":11000
          },
          {
             "name":"Oct",
             "uv":16000
          },
          {
             "name":"Nov",
             "uv":10000
          },
          {
             "name":"Dec",
             "uv":16000
          },
          
          {
             "name":"Jan",
             "uv":16000
          },
          
          {
             "name":"Feb",
             "uv":16000
          },
          
          {
             "name":"Mar",
             "uv":16000
          },
          
          {
             "name":"Apr",
             "uv":16000
          },
          
          {
             "name":"May",
             "uv":16000
          },
          
          {
             "name":"Jun",
             "uv":16000
          }
       ]
    },
    {
       "name":"FY21 Budget",
       "uv":56022,
       "month":[
         {
            "name":"Jul",
            "uv":12000
         },
         {
            "name":"Aug",
            "uv":15000
         },
         {
            "name":"Sept",
            "uv":11000
         },
         {
            "name":"Oct",
            "uv":16000
         },
         {
            "name":"Nov",
            "uv":10000
         },
         {
            "name":"Dec",
            "uv":16000
         },
         
         {
            "name":"Jan",
            "uv":16000
         },
         
         {
            "name":"Feb",
            "uv":16000
         },
         
         {
            "name":"Mar",
            "uv":16000
         },
         
         {
            "name":"Apr",
            "uv":16000
         },
         
         {
            "name":"May",
            "uv":16000
         },
         
         {
            "name":"Jun",
            "uv":16000
         }
      ]
    },
    {
       "name":"FY21 Nov Fcst",
       "uv":53836,
       "month":[
         {
            "name":"Jul",
            "uv":12000
         },
         {
            "name":"Aug",
            "uv":15000
         },
         {
            "name":"Sept",
            "uv":11000
         },
         {
            "name":"Oct",
            "uv":16000
         },
         {
            "name":"Nov",
            "uv":10000
         },
         {
            "name":"Dec",
            "uv":16000
         },
         
         {
            "name":"Jan",
            "uv":16000
         },
         
         {
            "name":"Feb",
            "uv":16000
         },
         
         {
            "name":"Mar",
            "uv":16000
         },
         
         {
            "name":"Apr",
            "uv":16000
         },
         
         {
            "name":"May",
            "uv":16000
         },
         
         {
            "name":"Jun",
            "uv":16000
         }
      ]
    },
    {
       "name":"FY21 Mar Fcst",
       "uv":53639,
       "month":[
         {
            "name":"Jul",
            "uv":12000
         },
         {
            "name":"Aug",
            "uv":15000
         },
         {
            "name":"Sept",
            "uv":11000
         },
         {
            "name":"Oct",
            "uv":16000
         },
         {
            "name":"Nov",
            "uv":10000
         },
         {
            "name":"Dec",
            "uv":16000
         },
         
         {
            "name":"Jan",
            "uv":16000
         },
         
         {
            "name":"Feb",
            "uv":16000
         },
         
         {
            "name":"Mar",
            "uv":16000
         },
         
         {
            "name":"Apr",
            "uv":16000
         },
         
         {
            "name":"May",
            "uv":16000
         },
         
         {
            "name":"Jun",
            "uv":16000
         }
      ]
    },
    {
       "name":"FY21 May Fcst",
       "uv":53639,
       "month":[
         {
            "name":"Jul",
            "uv":12000
         },
         {
            "name":"Aug",
            "uv":15000
         },
         {
            "name":"Sept",
            "uv":11000
         },
         {
            "name":"Oct",
            "uv":16000
         },
         {
            "name":"Nov",
            "uv":10000
         },
         {
            "name":"Dec",
            "uv":16000
         },
         
         {
            "name":"Jan",
            "uv":16000
         },
         
         {
            "name":"Feb",
            "uv":16000
         },
         
         {
            "name":"Mar",
            "uv":16000
         },
         
         {
            "name":"Apr",
            "uv":16000
         },
         
         {
            "name":"May",
            "uv":16000
         },
         
         {
            "name":"Jun",
            "uv":16000
         }
      ]
    }
 ]

 export default  ndf_update_data_dummy