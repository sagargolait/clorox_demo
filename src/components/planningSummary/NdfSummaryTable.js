import React, {useEffect} from "react";
import axios from 'axios'
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import { MdArrowDropDown } from "react-icons/md";
import TableView from './TableView'
import {URL, PORT} from '../../Config'



const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    
    padding: "1rem 0rem",
    "& .MuiAccordionSummary-root": {
      background: "#E1EAF2",
      borderRadius: "2px",
      height: "15px",
      minHeight:'35px',

    },
    "& .MuiAccordionDetails-root":{
      padding: '0px',
    },
    "& .MuiAccordionSummary-root.Mui-expanded":{
      minHeight:'35px',
      height:'15px'
    }
  },
  heading: {
    background: "#E1EAF2",
    borderRadius: "2px",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "16px",
    lineHeight: "19px",
    textTransform: "uppercase",
    color: "#828282",
  },
}));

const NdfSummaryTable = () => {
  
  const [table_data_ndf_update, setTable_data_ndf_update]=React.useState(" ")
  const [table_data_ByDrivers,set_Table_data_ByDrivers]=React.useState(" ")
  const [table_data_ByCustomer,set_Table_data_ByCustomer]=React.useState(" ")
  const [table_data_ByBrand,set_Table_data_ByBrand]=React.useState(" ")

  let filterData = sessionStorage.getItem("IbpStore_allFiltersSelected");

  
  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/table/ndf_update`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        setTable_data_ndf_update(response.data);
      })
      .catch((error) => {
        console.log(error);
      });

      return () => {                                                               // second, we return an anonymous clean up function
        setTable_data_ndf_update([])
      }
  }, [filterData]);


  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/table/bydriver`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        set_Table_data_ByDrivers(response.data);
      })
      .catch((error) => {
        console.log(error);
      });

      return () => {                                                               // second, we return an anonymous clean up function
        set_Table_data_ByDrivers([])
      }
  }, [filterData]);


  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/table/bycustomer`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        set_Table_data_ByCustomer(response.data);
      })
      .catch((error) => {
        console.log(error);
      });

      return () => {                                                               // second, we return an anonymous clean up function
        set_Table_data_ByCustomer([])
      }
  }, [filterData]);


  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/summary/table/bybrand`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        set_Table_data_ByBrand(response.data);
      })
      .catch((error) => {
        console.log(error);
      });

      return () => {                                                               // second, we return an anonymous clean up function
        set_Table_data_ByBrand([])
      }
  }, [filterData]);




  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Accordion defaultExpanded="true" >
        <AccordionSummary
          expandIcon={<MdArrowDropDown color={"#005CB9"} />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>FY21 Current NDF</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Array.isArray(table_data_ndf_update) ? <TableView data={table_data_ndf_update}/> : <h3>Loading...</h3>}
          
        </AccordionDetails>
      </Accordion>



      <Accordion>
        <AccordionSummary
          expandIcon={<MdArrowDropDown color={"#005CB9"} />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>BY DRIVER</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Array.isArray(table_data_ByDrivers)? <TableView data={table_data_ByDrivers}/> :<h3>Loading...</h3>}
        </AccordionDetails>
      </Accordion>


      <Accordion>
        <AccordionSummary
          expandIcon={<MdArrowDropDown color={"#005CB9"} />}
          aria-controls="panel3a-content"
          id="panel3a-header"
        >
          <Typography className={classes.heading}>BY CUSTOMER</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Array.isArray(table_data_ByCustomer) ? <TableView data={table_data_ByCustomer}/>: <h3>Loading...</h3>}
        </AccordionDetails>
      </Accordion>


      <Accordion>
        <AccordionSummary
          expandIcon={<MdArrowDropDown color={"#005CB9"} />}
          aria-controls="panel4a-content"
          id="panel4a-header"
        >
          <Typography className={classes.heading}>BY BRAND</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Array.isArray(table_data_ByBrand) ? <TableView data={table_data_ByBrand}/> : <h3>Loading..</h3>}
        </AccordionDetails>
      </Accordion>
    </div>
  );
};


export default NdfSummaryTable;
