import React, { useContext } from "react";
import { capitalize } from "../utils/utils";
import userFilters from "../contexts/UserFilters";

const HeaderTag = ({ array }) => {
  const filters = useContext(userFilters)["fil"];
  let selectedNodes = sessionStorage.getItem("IbpStore_allFiltersSelected");

  return (
    <div className="summary-header">
      <span>
        <strong>{JSON.parse(selectedNodes)["currentndf"]}</strong>
        &nbsp; vs&nbsp;
        <strong>{JSON.parse(selectedNodes)["base"]}</strong>
        &nbsp;for&nbsp;
        <strong>
          {filters.GBC_NAME == undefined
            ? "All Category"
            : filters.GBC_NAME.join(", ")}
        </strong>
        &nbsp;-&nbsp;
        <strong>
          {JSON.parse(sessionStorage.getItem("IbpStore_nodesSelected"))[
            "product"
          ].length === 0
            ? "All Brand"
            : JSON.parse(sessionStorage.getItem("IbpStore_nodesSelected"))
                ["product"].map((a) => a.label)
                .join(", ")}
        </strong>
        &nbsp; (in&nbsp;
        <strong>
          {filters.unit_of_measure}
          {filters.display_unit === "MSC" ? "(in '000s)" : null}
        </strong>
        &nbsp; ) &nbsp;for&nbsp;
        <strong>
          {filters.customerType === "national"
            ? "National"
            : filters.Account == undefined
            ? "All Customers"
            : "All Customers"}
        </strong>
        {/* &nbsp; - &nbsp; */}
        <strong>
          {/* {JSON.parse(sessionStorage.getItem("IbpStore_nodesSelected"))["time"]
            .length === 0
            ? " "
            : JSON.parse(sessionStorage.getItem("IbpStore_nodesSelected"))
                ["time"].map((a) => a.level_id.replace("_", " "))
                .join(", ")} */}
        </strong>
      </span>
    </div>
  );
};

export default HeaderTag;
