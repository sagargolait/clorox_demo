import React from "react";
import { IoCloudDoneSharp } from "react-icons/io5";
import { MdSave } from "react-icons/md";

const InnovationTopHeader = () => {
  return (
    <div className="innvotaion-top-header-container">
      <div className="innovation-sub-header-btn-styl">
        <button style={{ marginRight: "20px" }}>
          <MdSave style={{ marginRight: "10px" }} size={"1.1rem"} />
          Save
        </button>
        <button>
          <IoCloudDoneSharp style={{ marginRight: "10px" }} size={"1.2rem"} />
          Publish
        </button>
      </div>
    </div>
  );
};

export default InnovationTopHeader;
