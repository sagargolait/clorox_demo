import React from "react";

import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { IoMdArrowDropdown, IoMdArrowDropup } from "react-icons/io";

import { useTable, useSortBy, useFilters } from "react-table";

const useStyles = makeStyles(() => ({
  rowStyle: {
    background: "#F2F2F2",
    boxShadow: "0px 1px 0px #EFEFEF",
    "&:first-child": {
      background: "#FCFDFF",
      boxShadow: "0px 1px 0px #EFEFEF",
    },
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#F9F9F9",
    color: "#666666",
  },
  body: {
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "14px",
    lineHeight: "17px",
    textAlign: "center",
  },
  "& .MuiTableCell-root": {
    display: "flex",
  },
}))(TableCell);

const StyledTableCellBody = withStyles((theme) => ({
  body: {
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "14px",
    lineHeight: "17px",
    textAlign: "center",
    color: "#333333",
    padding: "5px",
  },
}))(TableCell);

const InnovationTable = ({ columns, data }) => {
  const classes = useStyles();
  const { getTableProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data,
    },
    useFilters,
    useSortBy
  );

  return (
    <MaUTable
      style={{ margin: "1rem", border: "1px solid", width: "97%" }}
      {...getTableProps()}
    >
      <TableHead>
        {headerGroups.map((headerGroup) => (
          <TableRow {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <StyledTableCell {...column.getHeaderProps()}>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  {column.render("Header")}
                  <div>{column.canFilter ? column.render("Filter") : null}</div>
                  <span
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                  >
                    {column.isSorted ? (
                      column.isSortedDesc ? (
                        <IoMdArrowDropdown
                          style={{ paddingLeft: "10px" }}
                          size={"1.5rem"}
                        />
                      ) : (
                        <IoMdArrowDropup
                          style={{ paddingLeft: "10px" }}
                          size={"1.5rem"}
                        />
                      )
                    ) : (
                      <IoMdArrowDropdown
                        style={{ paddingLeft: "10px" }}
                        size={"1.5rem"}
                      />
                    )}
                  </span>
                </div>
              </StyledTableCell>
            ))}
          </TableRow>
        ))}
      </TableHead>
      <TableBody>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <TableRow className={classes.rowStyle} {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return (
                  <StyledTableCellBody {...cell.getCellProps()}>
                    {cell.render("Cell")}
                  </StyledTableCellBody>
                );
              })}
            </TableRow>
          );
        })}
      </TableBody>
    </MaUTable>
  );
};

export default InnovationTable;
