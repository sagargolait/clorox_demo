import React from "react";
import "./Homepage.css";
import { Link } from "react-router-dom";
import companyLogo from "../assets/logos/companyLogo.png";
import Vector from "../assets/logos/Vector.png";

function Homepage() {
  return (
    <div className="background-styl">
      <img src={Vector} alt="vector" />
      <img id="logo-styl" src={companyLogo} alt="company logo" />
      <h1>Welcome to Planning Hub</h1>
      <hr
        style={{
          width: "70%",
          position: "absolute",
          top: "38%",
          left: "15%",
          backgroundColor: "#fff",
        }}
      />
      <div className="btn-sty">
        <Link to={"/planning/summary"}>
          <button>PLANNING</button>
        </Link>
        <Link to={"/innovation/allprojects"}>
          <button>INNOVATION</button>
        </Link>
      </div>
    </div>
  );
}

export default Homepage;
