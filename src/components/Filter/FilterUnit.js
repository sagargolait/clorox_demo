//Import required modules
import React from "react";

import AccordionDetails from "@material-ui/core/AccordionDetails";
import Spinner from "../../utils/Spinner";
import Button from "@material-ui/core/Button";

//Import Custom Components
import DropdownTreeSelect from "react-dropdown-tree-select";
import FilterRadio from "./filterTypes/FilterRadio";
import "./filterTypes/tree.css";
import _ from "lodash";

//Import utils
import { getAllChildren } from "../../utils/utils";

//Navbar Component
const FilterUnit = (props) => {
  let {
    name,
    data,
    type,
    label,
    loading,
    display,
    testdata,
    filterChange,
    setFilterChange,
    nodesSelected,
    setNodesSelected,
    allFilters,
    setAllFilters,
    nodesToggled,
    setNodesToggled,
  } = props;

  console.log("props in filterunit", props);
  let currNodesSelected = nodesSelected[name] || [];

  let spinnerHolderStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  };

  let buttonDivStyle = {
    margin: "0px",
    textAlign: "center",
    width: "100%",
  };

  let buttonStyle = {
    backgroundColor: "#005CB9",
    color: "white",
    marginRight: "10px",
    marginBottom: "10px",
  };

  const onChange = (currentNode, selectedNodes) => {
    //getting selected data in the json format required to send for updating customer/product
    console.log("currentNode", currentNode);
    let selectedData = {};
    if (name === "customer") {
      var obj = currentNode;
      let idArr = obj["_id"].split("-").slice(1); //first element isn't the ID
      let currData;
      let idArrLength = idArr.length;

      console.log("currData", currData);

      idArr.map((id, index) => {
        currData = index ? currData["children"][id] : data[id];
        if (index === idArrLength - 1) {
          if (index === idArrLength - 1) {
            if (currentNode["checked"] === true) {
              selectedData = currData.children
                ? getAllChildren(
                    allFilters,
                    currData.children,
                    selectedData,
                    "customer",
                    true
                  )
                : selectedData;
              selectedData[currData["labelCat"]] = allFilters[
                currData["labelCat"]
              ]
                ? allFilters[currData["labelCat"]].concat(currData["label"])
                : [currData["label"]];
            } else {
              selectedData = currData.children
                ? getAllChildren(
                    allFilters,
                    currData.children,
                    selectedData,
                    "customer",
                    false
                  )
                : selectedData;
              selectedData[currData["labelCat"]] =
                allFilters[currData["labelCat"]] !== undefined
                  ? allFilters[currData["labelCat"]].filter(
                      (item) => item !== currData["label"]
                    )
                  : undefined;
              selectedData[currData["labelCat"]] = selectedData[
                currData["labelCat"]
              ]
                ? selectedData[currData["labelCat"]].length === 0
                  ? undefined
                  : selectedData[currData["labelCat"]]
                : undefined;
            }
          } else {
            if (currentNode["checked"] === true) {
              selectedData[currData["labelCat"]] = allFilters[
                currData["labelCat"]
              ]
                ? allFilters[currData["labelCat"]].concat(currData["label"])
                : [currData["label"]];
            } else {
              selectedData[currData["labelCat"]] =
                allFilters[currData["labelCat"]] !== undefined
                  ? allFilters[currData["labelCat"]].filter(
                      (item) => item !== currData["label"]
                    )
                  : undefined;
              selectedData[currData["labelCat"]] = selectedData[
                currData["labelCat"]
              ]
                ? selectedData[currData["labelCat"]].length === 0
                  ? undefined
                  : selectedData[currData["labelCat"]]
                : undefined;
            }
          }
          // selectedData = currData.children ? getAllChildren(currData.children, selectedData) : selectedData
        }
        // selectedData[currData['labelCat']] = selectedData[currData['labelCat']] ? selectedData[currData['labelCat']].concat(currData["label"]) : [currData["label"]]
      });
      if (selectedData["Account"] !== undefined) {
        selectedData["Account"] = selectedData["Account"].filter(
          (acc) => acc !== "All"
        );
      }
    } else if (name === "time") {
      var obj = currentNode;
      let idArr = obj["_id"].split("-").slice(1); //first element isn't the ID
      console.log("idArr", idArr);
      let currData;
      let idArrLength = idArr.length;

      idArr.map((id, index) => {
        currData = index ? currData["children"][id] : data[id];
        if (index === idArrLength - 1) {
          // selectedData = currData.children ? getAllChildren(currData.children, selectedData,"time") : selectedData
          if (index === idArrLength - 1) {
            if (currentNode["checked"] === true) {
              selectedData = currData.children
                ? getAllChildren(
                    allFilters,
                    currData.children,
                    selectedData,
                    "time",
                    true
                  )
                : selectedData;
              selectedData[currData["labelCat"]] = allFilters[
                currData["labelCat"]
              ]
                ? allFilters[currData["labelCat"]].concat(currData["label"])
                : [currData["label"]];
            } else {
              selectedData = currData.children
                ? getAllChildren(
                    allFilters,
                    currData.children,
                    selectedData,
                    "time",
                    false
                  )
                : {};
              selectedData[currData["labelCat"]] =
                allFilters[currData["labelCat"]] !== undefined
                  ? allFilters[currData["labelCat"]].filter(
                      (item) => item !== currData["label"]
                    )
                  : undefined;
              selectedData[currData["labelCat"]] = selectedData[
                currData["labelCat"]
              ]
                ? selectedData[currData["labelCat"]].length === 0
                  ? undefined
                  : selectedData[currData["labelCat"]]
                : undefined;
            }
          } else {
            if (currentNode["checked"] === true) {
              selectedData[currData["labelCat"]] = allFilters[
                currData["labelCat"]
              ]
                ? allFilters[currData["labelCat"]].concat(currData["label"])
                : [currData["label"]];
            } else {
              selectedData[currData["labelCat"]] =
                allFilters[currData["labelCat"]] !== undefined
                  ? allFilters[currData["labelCat"]].filter(
                      (item) => item !== currData["label"]
                    )
                  : undefined;
              selectedData[currData["labelCat"]] = selectedData[
                currData["labelCat"]
              ]
                ? selectedData[currData["labelCat"]].length === 0
                  ? undefined
                  : selectedData[currData["labelCat"]]
                : undefined;
            }
          }
        }
        // selectedData[currData['labelCat']] = selectedData[currData['labelCat']] ? selectedData[currData['labelCat']].concat(currData["level_id"]) : [currData["level_id"]]
      });

      if (
        !selectedData["selected_list"] ||
        (allFilters["viewType"] === "Monthly" &&
          selectedData["selected_list"].length <= 12) ||
        (allFilters["viewType"] === "Quarterly" &&
          selectedData["selected_list"].length <= 4)
      ) {
        selectedData = {
          ...selectedData,
        };
      }
    } else {
      var obj = currentNode;
      let idArr = obj["_id"].split("-").slice(1); //first element isn't the ID
      let currData;
      let idArrLength = idArr.length;
      idArr.map((id, index) => {
        currData = index ? currData["children"][id] : data[id];

        if (index === idArrLength - 1) {
          if (currentNode["checked"] === true) {
            selectedData = currData.children
              ? getAllChildren(
                  allFilters,
                  currData.children,
                  selectedData,
                  "product",
                  true
                )
              : selectedData;
            selectedData[currData["labelCat"]] = allFilters[
              currData["labelCat"]
            ]
              ? allFilters[currData["labelCat"]].concat(currData["label"])
              : [currData["label"]];
          } else {
            selectedData = currData.children
              ? getAllChildren(
                  allFilters,
                  currData.children,
                  selectedData,
                  "product",
                  false
                )
              : {};
            selectedData[currData["labelCat"]] =
              allFilters[currData["labelCat"]] !== undefined
                ? allFilters[currData["labelCat"]].filter(
                    (item) => item !== currData["label"]
                  )
                : undefined;
            selectedData[currData["labelCat"]] = selectedData[
              currData["labelCat"]
            ]
              ? selectedData[currData["labelCat"]].length === 0
                ? undefined
                : selectedData[currData["labelCat"]]
              : undefined;
          }
        } else {
          if (currentNode["checked"] === true) {
            selectedData[currData["labelCat"]] = allFilters[
              currData["labelCat"]
            ]
              ? allFilters[currData["labelCat"]].concat(currData["label"])
              : [currData["label"]];
          } else {
            selectedData[currData["labelCat"]] =
              allFilters[currData["labelCat"]] !== undefined
                ? allFilters[currData["labelCat"]].filter(
                    (item) => item !== currData["label"]
                  )
                : undefined;
            selectedData[currData["labelCat"]] = selectedData[
              currData["labelCat"]
            ]
              ? selectedData[currData["labelCat"]].length === 0
                ? undefined
                : selectedData[currData["labelCat"]]
              : undefined;
          }
        }
      });
    }

    //Remove duplicate values & key customer/product
    let uniqueSelectedData = {};

    uniqueSelectedData[currentNode["labelCat"]] =
      selectedData[currentNode["labelCat"]] === undefined
        ? undefined
        : selectedData[currentNode["labelCat"]];
    Object.keys(selectedData).map((key) => {
      if (key !== name && selectedData[key] !== undefined) {
        uniqueSelectedData[key] = selectedData[key].filter(
          (item, index, inputArray) => index === inputArray.indexOf(item)
        );
      }
    });

    currNodesSelected = selectedNodes;
    //if freeze needs to be removed
    console.log(currNodesSelected);
    var temp = {};
    currNodesSelected.map((t) => {
      if (temp[t.labelCat] !== undefined) {
        temp[t.labelCat].push(t.label);
      } else {
        temp[t.labelCat] = [t.label];
      }
    });

    console.log(temp);

    setNodesSelected({
      ...nodesSelected,
      [name]: currNodesSelected,
    });

    setAllFilters({
      ...allFilters,
      ...uniqueSelectedData,
      ...temp,
    });

    setFilterChange({
      ...filterChange,
      [name]: filterChange[name] + 1,
    });
  };

  const onNodeToggle = (currentNode) => {
    //remove the current node from selected list
    let currNodesToggled = nodesToggled[name].filter(
      (obj) => obj.level_id !== currentNode.level_id
    );

    //add the current node with the latest info
    currNodesToggled = [...currNodesToggled, ...[currentNode]];

    //remove those nodes which are not expanded
    currNodesToggled = currNodesToggled.filter((obj) => obj.expanded);

    setNodesToggled({
      ...nodesToggled,
      [name]: currNodesToggled,
    });
    setFilterChange({
      ...filterChange,
      [name]: filterChange[name] + 1,
    });
  };

  //Condition to restrict max 12 months selection
  if (data !== null) {
    if (name === "time") {
      if (allFilters["viewType"] === "Monthly") {
        var count = 0;
        console.log(data);
        data.map((a) => {
          a["children"] &&
            a["children"].map((c) => {
              c["children"] &&
                c["children"].map((d) => {
                  if (d.checked) {
                    count++;
                  }
                });
            });
        });

        if (count === 12) {
          data.map((a) => {
            a["disabled"] = !a["checked"] ? true : false;

            a["children"] &&
              a["children"].map((c) => {
                c["disabled"] = !c["checked"] ? true : false;
                c["children"] &&
                  c["children"].map((d) => {
                    d["disabled"] = !d["checked"] ? true : false;
                  });
              });
          });
        } else {
          data.map((a) => {
            a["disabled"] = false;

            a["children"] &&
              a["children"].map((c) => {
                c["disabled"] = false;
                c["children"] &&
                  c["children"].map((d) => {
                    d["disabled"] = false;
                  });
              });
          });
        }
      } else {
        var count = 0;

        data.map((a) => {
          a["children"] &&
            a["children"].map((c) => {
              if (c.checked) {
                count++;
              }
            });
        });

        if (count === 4) {
          data.map((a) => {
            a["disabled"] = !a["checked"] ? true : false;

            a["children"] &&
              a["children"].map((c) => {
                c["disabled"] = !c["checked"] ? true : false;
              });
          });
        } else {
          data.map((a) => {
            a["disabled"] = false;

            a["children"] &&
              a["children"].map((c) => {
                c["disabled"] = false;
              });
          });
        }
      }
    }
  }

  return (
    <>
      <AccordionDetails>
        {loading ? (
          <div style={spinnerHolderStyle}>
            <Spinner />
            Loading..
          </div>
        ) : (
          <>
            {display ? (
              type === "radio" ? (
                <FilterRadio
                  data={data}
                  name={name}
                  setNodesSelected={setNodesSelected}
                  filterChange={filterChange}
                  setFilterChange={setFilterChange}
                  allFilters={allFilters}
                  setAllFilters={setAllFilters}
                />
              ) : (
                <DropdownTreeSelect
                  data={data}
                  texts={{ placeholder: "Type/Select..." }}
                  showDropdown={"always"}
                  onChange={(e, d) => {
                    onChange(e, d);
                  }}
                  onNodeToggle={(e) => onNodeToggle(e)}
                  className="mdl-demo-custom"
                  // className="bootstrap-demo"
                />
              )
            ) : null}
          </>
        )}
      </AccordionDetails>
    </>
  );
};

export default React.memo(FilterUnit, (prevProps, nextProps) => {
  function filterObject(obj, key) {
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;
      if (i == key) {
        delete obj[key];
      } else if (typeof obj[i] == "object") {
        filterObject(obj[i], key);
      }
    }
    return obj;
  }
  if (prevProps.name === "product") {
    return _.isEqual(
      filterObject(filterObject(prevProps.testdata, "checked"), "expanded"),
      filterObject(filterObject(nextProps.testdata, "checked"), "expanded")
    );
  } else {
    return false;
  }
});
