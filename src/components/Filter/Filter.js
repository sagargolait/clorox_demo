import React, { useState, useContext } from "react";
import { FiFilter } from "react-icons/fi";
import FilterLogo from "../../assets/logos/filterLogo.png";
import "./Filter.css";
// import axios from 'axios'

import FilterPane from "./FilterPane";
import { applyFilters, updateLocalStorage } from "../../utils/utils";

const Filter = () => {
  const [filterState, setFilterState] = useState(false);

  return (
    <div>
      <FilterPane filterState={filterState} setFilterState={setFilterState} />
      <button className="filter-btn-styl" onClick={() => setFilterState(true)}>
        <img src={FilterLogo} alt="FilterLogo}" />
      </button>
    </div>
  );
};

export default Filter;
