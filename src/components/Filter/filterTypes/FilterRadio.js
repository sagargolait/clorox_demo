//Import required modules
import React, { useState } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { withStyles, makeStyles } from "@material-ui/core/styles";

const BlueRadio = withStyles({
  root: {
    color: "#005CB9",
    "&$checked": {
      color: "#005CB9",
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

const useStyles = makeStyles(() => ({
  root: {
    "& .MuiFormControlLabel-root": {
      paddingRight: "10px",
    },
    "& .MuiFormLabel-root": {
      padding: "10px 0px",
      paddingLeft: "5px",
      fontFamily: "Lato",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "12px",
      lineHeight: "14px",
      letterSpacing: "0.03em",
    },
    "&  .MuiTypography-root": {
      paddingLeft: "5px",
      fontFamily: "Lato",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "12px",
      lineHeight: "14px",
      letterSpacing: "0.03em",

      color: "#666666",
    },
    "& .MuiIconButton-root": {
      padding: 0,
    },
    "& .MuiSvgIcon-root": {
      width: "20px",
    },
  },
}));

const FilterRadio = ({
  name,
  setNodesSelected,
  data,
  filterChange,
  setFilterChange,
  allFilters,
  setAllFilters,
}) => {
  const classes = useStyles();
  const [value, setValue] = useState(allFilters[name]);
  var prod_hier = {
    GBC_NAME: undefined,
    DIVISION_NAME: undefined,
    BRAND_NAME: undefined,
    MFF_NAME: undefined,
    EAN_UPC: undefined,
    SUB_MFF_NAME: undefined,
    PURPOSE_NAME: undefined,
    TYPE_NAME: undefined,
    PACK_SIZE_NAME: undefined,
    EAN_UPC_Index: undefined,
    EAN_DESC: undefined,
  };

  const handleChange = (event) => {
    setValue(event.target.value);
    setNodesSelected({
      customer: [],
      product: [],
    });
    if (event.target.value === "national") {
      setAllFilters({
        ...allFilters,
        ...prod_hier,
        Account:
          event.target.value === "national"
            ? window.sessionStorage.getItem("defaultAccounts").split(",")
            : undefined,
        [name]: event.target.value,
      });
    } else {
      setAllFilters({
        ...allFilters,
        Account:
          event.target.value === "national"
            ? window.sessionStorage.getItem("defaultAccounts").split(",")
            : undefined,

        [name]: event.target.value,
      });
    }
    setFilterChange({
      ...filterChange,
      [name]: filterChange[name] + 1,
    });
  };

  let radioGroupStyle = {
    flexDirection: "row",
  };
  return (
    <div>
      <FormControl className={classes.root} component="fieldset">
        {data[0]["value"] === "MSC" ? (
          <FormLabel style={{ paddingLeft: "20px", color: "black" }}>
            Display Units
          </FormLabel>
        ) : null}
        <RadioGroup
          aria-label={name}
          // className={classes.root}
          name={name}
          value={value}
          onChange={handleChange}
          style={
            data[0]["value"] === "MSC"
              ? { ...radioGroupStyle, paddingLeft: "20px" }
              : radioGroupStyle
          }
        >
          {data.map((obj, i) => {
            return (
              <FormControlLabel
                key={i}
                value={obj.value}
                disabled={
                  obj.value === "Actual Case" || obj.value === "Units"
                    ? true
                    : false
                }
                //disabled={obj.disabled}
                control={<BlueRadio />}
                label={obj.label}
              />
            );
          })}
        </RadioGroup>
      </FormControl>
    </div>
  );
};
export default FilterRadio;
