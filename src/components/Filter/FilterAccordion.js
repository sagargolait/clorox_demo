import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FilterUnit from "./FilterUnit";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  accordion: {
    border: "1px solid #eeeeee",
    borderRadius: "1px",
    padding: "2px",

    "& .MuiAccordionDetails-root": {
      padding: "0px 16px",
    },
    "& .MuiAccordionSummary-root.Mui-expanded": {
      minHeight: "54px",
    },
  },
  heading: {
    fontStyle: "normal",
    fontWeight: "300",
    fontSize: "14px",
    lineHeight: "17px",
    letterSpacing: "0.03em",
    textTransform: "uppercase",
    color: "#666666",
  },
  radio: {
    flexDirection: "row",
  },
}));

const FilterAccordion = (props) => {
  const classes = useStyles();

  let {
    label,
    filters,
    filterChange,
    setFilterChange,
    nodesSelected,
    setNodesSelected,
    allFilters,
    setAllFilters,
    nodesToggled,
    setNodesToggled,
  } = props;

  const [value, setValue] = React.useState("brand");

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <div className={classes.root}>
      <Accordion className={classes.accordion}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>{label}</Typography>
        </AccordionSummary>
        {Object.keys(filters).map((key, index) => {
          return (
            <FilterUnit
              key={index}
              {...filters[key]}
              testdata={JSON.parse(JSON.stringify(filters[key]["data"]))}
              filterChange={filterChange}
              setFilterChange={setFilterChange}
              nodesSelected={nodesSelected}
              setNodesSelected={setNodesSelected}
              allFilters={allFilters}
              setAllFilters={setAllFilters}
              nodesToggled={nodesToggled}
              setNodesToggled={setNodesToggled}
            />
          );
        })}
      </Accordion>
    </div>
  );
};

export default FilterAccordion;
