import React, { useEffect, useState, useContext } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
// import axios from "axios";
import { IoMdClose } from "react-icons/io";
import { MdRefresh } from "react-icons/md";

import FilterAccordion from "./FilterAccordion";

import initialCustomerFilter from "./data/initialCustomerFilter.json";
import initialProductFilter from "./data/initialProductFilter.json";
import initialTimeFilter from "./data/initialTimeFilter.json";
import initialMeasureFilter from "./data/initialMeasureFilter.json";
import initialFilterChange from "./data/initialFilterChange.json";
import initialNodesSelected from "./data/initialNodesSelected.json";
import initialNodesToggled from "./data/initialNodesToggled.json";
import resetFilters from "./data/resetFilters.json";

import UserFilters from "../../contexts/UserFilters";
import TimeFilter from "../../contexts/TimeFilter";

import {
  updateLocalStorage,
  applyFilters,
  updateFilterData,
} from "../../utils/utils";

import { URL, PORT } from "../../Config";

const useStyles = makeStyles({
  list: {
    width: 400,
  },
  fullList: {
    width: "auto",
  },
});

const FilterPane = ({ filterState, setFilterState }) => {
  const classes = useStyles();

  const [defaultvalue, setDefaultValue] = useState([
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
  ]);

  // useEffect(() => {
  //   axios(`${URL} + "/default_year_filters", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({
  //       year: "2021",
  //       viewType: "Monthly",
  //     }),
  //   })
  //     .then((res) => res.json())
  //     .then((res) => {
  //       // console.log(res)
  //       setDefaultValue(["FY21_Jul", "FY21_Aug", "FY21_Sep"]);
  //     })
  //     .catch((err) => console.log("Cannot load filter data: " + err));
  // }, []);

  let nodesSelectedFromStorage = JSON.parse(
    window.sessionStorage.getItem("IbpStore_nodesSelected")
  );
  let nodesToggledFromStorage = JSON.parse(
    window.sessionStorage.getItem("IbpStore_nodesToggled")
  );

  const { timeFilter, setTimeFilter } = useContext(TimeFilter);
  const { fil, setFil } = useContext(UserFilters);

  const [prodFilterData, setProdFilterData] = useState(initialProductFilter);
  const [custFilterData, setCustFilterData] = useState(initialCustomerFilter);
  const [timeFilterData, setTimeFilterData] = useState(initialTimeFilter);
  const [allFilters, setAllFilters] = useState({ ...timeFilter, ...fil });
  const [measFilterData, setMeasFilterData] = useState(initialMeasureFilter);
  const [filterChange, setFilterChange] = useState(initialFilterChange);
  const [defaultAccount, setdefaultaccounts] = useState();
  const [nodesSelected, setNodesSelected] = useState(nodesSelectedFromStorage);
  const [nodesToggled, setNodesToggled] = useState(nodesToggledFromStorage);

  const toggleDrawer = (open) => (event) => {
    setFilterState(open);
  };
  // // Get Filter Data for the first time
  useEffect(() => {
    let updcustFilterData;
    let updprodFilterData;
    let updtimeFilterData;
    let paramObj;

    // Updating customer hierarchy
    fetch(`${URL}/nested_filters_customers`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(allFilters),
    })
      .then((res) => res.json())
      .then((res) => {
        var acc = res["customer"][0]["children"].map((a) => a.label);
        paramObj = {
          filterName: "customer",
          filterData: custFilterData,
          key: "customer",
          data: res["customer"],
          loading: false,
          nodesSelected: nodesSelected,
          display: allFilters.customerType === "account",
          defaultCheck: true,
          nodesToggled: nodesToggled,
        };
        updcustFilterData = updateFilterData(paramObj);
        setCustFilterData(updcustFilterData);

        console.log(res); //
        console.log(acc);
        console.log(paramObj);
        console.log(updcustFilterData);
        // setdefaultaccounts(acc)
        // window.sessionStorage.setItem("defaultAccounts",acc)
        // console.log(defaultAccount)
      })
      .catch((err) => console.log("Cannot load filter data: " + err));

    fetch(`${URL}/nested_filters_customers`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    })
      .then((res) => res.json())
      .then((res) => {
        var acc = res["customer"][0]["children"].map((a) => a.label);
        paramObj = {
          filterName: "customer",
          filterData: custFilterData,
          key: "customer",
          data: res["customer"],
          loading: false,
          nodesSelected: nodesSelected,
          display: allFilters.customerType === "account",
          defaultCheck: true,
          nodesToggled: nodesToggled,
        };
        updcustFilterData = updateFilterData(paramObj);
        // setCustFilterData(updcustFilterData)

        setdefaultaccounts(acc);
        window.sessionStorage.setItem("defaultAccounts", acc);
        console.log(defaultAccount);

        console.log(res);
        console.log(acc);
        console.log(paramObj);
        console.log(updcustFilterData);
      })
      .catch((err) => console.log("Cannot load filter data: " + err));
  }, [filterChange.filterPaneClosed, timeFilter]);

  useEffect(() => {
    let updcustFilterData;
    let updprodFilterData;
    let updtimeFilterData;
    let paramObj;

    fetch(`${URL}/nested_filters_product`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ...allFilters,
        Account:
          allFilters["Account"] !== undefined
            ? allFilters["Account"]
            : allFilters["customerType"] === "national"
            ? undefined
            : defaultAccount,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        // console.log(res)
        paramObj = {
          filterName: "product",
          filterData: prodFilterData,
          key: "product",
          data: res["product"],
          loading: false,
          nodesSelected: nodesSelected,
          display: true,
          defaultCheck: true,
          nodesToggled: nodesToggled,
        };
        updprodFilterData = updateFilterData(paramObj);
        setProdFilterData(updprodFilterData);
      })
      .catch((err) => console.log("Cannot load filter data: " + err));
  }, [
    filterChange.filterPaneClosed,
    defaultAccount,
    filterChange.customerType,
    timeFilter,
  ]);

  //Updating product hierarchy
  useEffect(() => {
    let updcustFilterData;
    let updprodFilterData;
    let updtimeFilterData;
    let paramObj;

    fetch(`${URL}/nested_filters_time`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(allFilters),
    })
      .then((res) => res.json())
      .then((res) => {
        // console.log(res)
        let timeData = res["time"];
        paramObj = {
          filterName: "time",
          filterData: timeFilterData,
          key: "time",
          data: timeData,
          loading: false,
          nodesSelected: nodesSelected,
          display: true,
          defaultCheck: true,
          nodesToggled: nodesToggled,
        };
        updtimeFilterData = updateFilterData(paramObj);
        setTimeFilterData(updtimeFilterData);
      })
      .catch((err) => console.log("Cannot load filter data: " + err));
  }, [filterChange.filterPaneClosed]);

  // Get Customer data if user FREEZES Prod data
  useEffect(() => {
    let updprodFilterData;
    let updcustFilterData;
    let paramObj;
    if (filterChange.product !== 0) {
      //console.log('Re-rendered')
      paramObj = {
        filterName: "customer",
        filterData: custFilterData,
        key: "customer",
        data: null,
        loading: true,
        nodesSelected: nodesSelected,
        display: allFilters.customerType === "account",
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updcustFilterData = updateFilterData(paramObj);
      setCustFilterData(updcustFilterData);

      // change filter to loading
      fetch(`${URL}/nested_filters_customers`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(allFilters),
      })
        .then((res) => res.json())
        .then((res) => {
          // console.log(res)
          paramObj = {
            filterName: "customer",
            filterData: custFilterData,
            key: "customer",
            data: res["customer"],
            loading: false,
            nodesSelected: nodesSelected,
            display: allFilters.customerType === "account",
            defaultCheck: true,
            nodesToggled: nodesToggled,
          };
          updcustFilterData = updateFilterData(paramObj);
          setCustFilterData(updcustFilterData);
        })
        .catch((err) => console.log("Cannot load filter data: " + err));
    }

    if (filterChange.product !== 0) {
      //console.log('Re-rendered')
      paramObj = {
        filterName: "product",
        filterData: prodFilterData,
        key: "product",
        data: null,
        loading: true,
        nodesSelected: nodesSelected,
        display: true,
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updprodFilterData = updateFilterData(paramObj);
      setProdFilterData(updprodFilterData);

      console.log("productfilter", updprodFilterData);

      paramObj = {
        filterName: "product",
        filterData: prodFilterData,
        key: "product",
        data: prodFilterData["product"].filters["product"]["data"],
        loading: false,
        nodesSelected: nodesSelected,
        display: true,
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updprodFilterData = updateFilterData(paramObj);
      setProdFilterData(updprodFilterData);
    }

    if (filterChange.customerType !== 0) {
      paramObj = {
        filterName: "customer",
        filterData: custFilterData,
        key: "customer",
        data: custFilterData["customer"].filters["customer"]["data"],
        loading: false,
        nodesSelected: nodesSelected,
        display: allFilters.customerType === "account",
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updcustFilterData = updateFilterData(paramObj);
      setCustFilterData(updcustFilterData);
    }
  }, [filterChange.product, filterChange.customerType]);

  // Get Prod data if user FREEZES Cust data or changes Prod View By
  useEffect(() => {
    let updprodFilterData;
    let updcustFilterData;
    let paramObj;
    if (filterChange.customer !== 0 || filterChange.view_by !== 0) {
      //console.log('Re-rendered')
      paramObj = {
        filterName: "product",
        filterData: prodFilterData,
        key: "product",
        data: null,
        loading: true,
        nodesSelected: nodesSelected,
        display: true,
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updprodFilterData = updateFilterData(paramObj);
      setProdFilterData(updprodFilterData);

      // change filter to loading
      fetch(`${URL}/nested_filters_product`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          ...allFilters,
          Account:
            allFilters["customerType"] === "national"
              ? undefined
              : allFilters["Account"],
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          // console.log(res)
          paramObj = {
            filterName: "product",
            filterData: prodFilterData,
            key: "product",
            data: res["product"],
            loading: false,
            nodesSelected: nodesSelected,
            display: true,
            defaultCheck: true,
            nodesToggled: nodesToggled,
          };
          updprodFilterData = updateFilterData(paramObj);
          setProdFilterData(updprodFilterData);
        })
        .catch((err) => console.log("Cannot load filter data: " + err));
    }
    if (filterChange.customer !== 0) {
      //console.log('Re-rendered')
      console.log(custFilterData);
      paramObj = {
        filterName: "customer",
        filterData: custFilterData,
        key: "customer",
        data: null,
        loading: true,
        nodesSelected: nodesSelected,
        display: allFilters.customerType === "account",
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updcustFilterData = updateFilterData(paramObj);
      setCustFilterData(updcustFilterData);
      paramObj = {
        filterName: "customer",
        filterData: custFilterData,
        key: "customer",
        data: custFilterData["customer"].filters["customer"]["data"],
        loading: false,
        nodesSelected: nodesSelected,
        display: allFilters.customerType === "account",
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updcustFilterData = updateFilterData(paramObj);
      setCustFilterData(updcustFilterData);
    }
  }, [filterChange.customer, filterChange.view_by, filterChange.customerType]);

  // Get Time data if user changes calendarType or view_by
  useEffect(() => {
    let paramObj;

    if (filterChange.calendarType !== 0 || filterChange.viewType !== 0) {
      let updtimeFilterData;
      // console.log('Time Re-rendered')
      paramObj = {
        filterName: "time",
        filterData: timeFilterData,
        key: "time",
        data: null,
        loading: true,
        nodesSelected: nodesSelected,
        display: true,
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updtimeFilterData = updateFilterData(paramObj);
      setTimeFilterData(updtimeFilterData);

      // change filter to loading
      fetch(`${URL}/nested_filters_time`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(allFilters),
      })
        .then((res) => res.json())
        .then((res) => {
          // console.log(res)
          paramObj = {
            filterName: "time",
            filterData: timeFilterData,
            key: "time",
            data: res["time"],
            loading: false,
            nodesSelected: nodesSelected,
            display: true,
            defaultCheck: true,
            nodesToggled: nodesToggled,
          };
          updtimeFilterData = updateFilterData(paramObj);
          console.log(updtimeFilterData);
          setTimeFilterData(updtimeFilterData);
        })
        .catch((err) => console.log("Cannot load filter data: " + err));
    }
  }, [filterChange.calendarType, filterChange.viewType]);

  // Update Time data after freeze
  useEffect(() => {
    if (filterChange.time !== 0) {
      let updtimeFilterData;
      let paramObj;

      paramObj = {
        filterName: "time",
        filterData: timeFilterData,
        key: "time",
        data: null,
        loading: true,
        nodesSelected: nodesSelected,
        display: true,
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updtimeFilterData = updateFilterData(paramObj);
      setTimeFilterData(updtimeFilterData);

      paramObj = {
        filterName: "time",
        filterData: timeFilterData,
        key: "time",
        data: timeFilterData["time"].filters["time"]["data"],
        loading: false,
        nodesSelected: nodesSelected,
        display: true,
        defaultCheck: true,
        nodesToggled: nodesToggled,
      };
      updtimeFilterData = updateFilterData(paramObj);
      setTimeFilterData(updtimeFilterData);
    }
  }, [filterChange.time]);

  const handleApply = () => {
    updateLocalStorage(null, allFilters, "IbpStore_allFiltersSelected");
    updateLocalStorage(null, nodesSelected, "IbpStore_nodesSelected");
    updateLocalStorage(null, nodesToggled, "IbpStore_nodesToggled");
    applyFilters(allFilters, setFil, setTimeFilter);
    setFilterState(false);
  };

  const handleReset = () => {
    // let tab = window.sessionStorage.getItem("tab");
    window.sessionStorage.clear();
    // window.sessionStorage.setItem("tab", tab);
    setAllFilters(resetFilters);
    setProdFilterData(initialProductFilter);
    setCustFilterData(initialCustomerFilter);
    setTimeFilterData(initialTimeFilter);
    setMeasFilterData(initialMeasureFilter);
    setNodesSelected(initialNodesSelected);
    setNodesToggled(initialNodesToggled);
    updateLocalStorage(
      null,
      {
        ...initialNodesSelected,
        ...{
          time: [
            {
              checked: true,
              disabled: false,
              expanded: false,
              label: "Q1",
              labelCat: "selected_quarter",
              level_id: "Q1",
            },
            {
              checked: true,
              disabled: false,
              expanded: false,
              label: "Q2",
              labelCat: "selected_quarter",
              level_id: "Q2",
            },
            {
              checked: true,
              disabled: false,
              expanded: false,
              label: "Q3",
              labelCat: "selected_quarter",
              level_id: "Q3",
            },
            {
              checked: true,
              disabled: false,
              expanded: false,
              label: "Q4",
              labelCat: "selected_quarter",
              level_id: "Q4",
            },
          ],
        },
      },
      "IbpStore_nodesSelected"
    );
    updateLocalStorage(null, initialNodesToggled, "IbpStore_nodesToggled");
    applyFilters(
      { ...resetFilters, ...{ selected_list: defaultvalue } },
      setFil,
      setTimeFilter
    );
    setFilterChange(false);
  };

  const list = (anchor) => (
    <div className="filter-container">
      <div className="filter-header">
        <input placeholder="Select" />
        <MdRefresh
          id="reset-icon"
          color={"#005CB9"}
          size={"1.1rem"}
          onClick={() => handleReset()}
        />
        <IoMdClose
          className="close-icon-styl"
          color={"#DEDEDE"}
          size={"1.5rem"}
          onClick={() => setFilterState(false)}
        />
      </div>
      {Object.keys(custFilterData).map((key, i) => {
        //delete custFilterData[key]["filters"]["customerType"]
        console.log(custFilterData);
        return (
          <FilterAccordion
            key={i}
            {...custFilterData[key]}
            filterChange={filterChange}
            setFilterChange={setFilterChange}
            nodesSelected={nodesSelected}
            setNodesSelected={setNodesSelected}
            allFilters={allFilters}
            setAllFilters={setAllFilters}
            nodesToggled={nodesToggled}
            setNodesToggled={setNodesToggled}
          />
        );
      })}
      {Object.keys(prodFilterData).map((key, i) => {
        console.log(prodFilterData);
        return (
          <FilterAccordion
            key={i}
            {...prodFilterData[key]}
            filterChange={filterChange}
            setFilterChange={setFilterChange}
            nodesSelected={nodesSelected}
            setNodesSelected={setNodesSelected}
            allFilters={allFilters}
            setAllFilters={setAllFilters}
            nodesToggled={nodesToggled}
            setNodesToggled={setNodesToggled}
          />
        );
      })}
      {Object.keys(timeFilterData).map((key, i) => {
        console.log(timeFilterData);
        delete timeFilterData[key]["filters"]["calendarType"];
        return (
          <FilterAccordion
            key={i}
            {...timeFilterData[key]}
            filterChange={filterChange}
            setFilterChange={setFilterChange}
            nodesSelected={nodesSelected}
            setNodesSelected={setNodesSelected}
            allFilters={allFilters}
            setAllFilters={setAllFilters}
            nodesToggled={nodesToggled}
            setNodesToggled={setNodesToggled}
          />
        );
      })}
      {Object.keys(measFilterData).map((key, i) => {
        if (allFilters["unit_of_measure"] !== "Stat Case") {
          measFilterData[key]["filters"]["display_unit"]["display"] = false;
        } else {
          measFilterData[key]["filters"]["display_unit"]["display"] = true;
        }
        return (
          <FilterAccordion
            key={i}
            {...measFilterData[key]}
            filterChange={filterChange}
            setFilterChange={setFilterChange}
            nodesSelected={nodesSelected}
            setNodesSelected={setNodesSelected}
            allFilters={allFilters}
            setAllFilters={setAllFilters}
          />
        );
      })}
      <div
        style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}
      >
        <button onClick={() => handleApply()}>Apply</button>
      </div>
    </div>
  );

  const handleClose = () => {
    setFilterChange({
      ...filterChange,
      filterPaneClosed: filterChange.filterPaneClosed + 1,
    });
    setAllFilters(
      JSON.parse(window.sessionStorage.getItem("IbpStore_allFiltersSelected"))
    );
    setNodesSelected(
      JSON.parse(window.sessionStorage.getItem("IbpStore_nodesSelected"))
    );
    setNodesToggled(
      JSON.parse(window.sessionStorage.getItem("IbpStore_nodesToggled"))
    );
    setFilterState(false);
  };

  return (
    <div>
      <Drawer anchor={"left"} open={filterState} onClose={toggleDrawer(false)}>
        {list("left")}
      </Drawer>
    </div>
  );
};

export default FilterPane;
