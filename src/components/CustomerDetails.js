import React from "react";
import { MdFileUpload } from "react-icons/md";
import SharedTable from "../sharedComponents/SharedTable";
import Select from "react-select";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

export const selectStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px solid #eeeeee",
    color: state.isSelected ? "#005CB9" : "",
    fontSize: 16,
    backgroundColor: state.isSelected ? "#eee" : "",
    textAlign: "left",
    cursor: "pointer",
  }),
  container: (base) => ({
    ...base,
    width: "125px",
  }),
  control: (base) => ({
    ...base,
    width: "125px",
    height: "26px",
    minHeight: "26px",
    fontSize: "14px",
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: "normal",
    background: "#FFFFFF",
    borderRadius: 2,
    border: "1px solid #999",
    boxShadow:
      "-5px -5px 10px rgba(0, 80, 115, 0.02), 5px 5px 10px rgba(0, 80, 115, 0.02)",
    width: "100%",
    textAlign: "center",
    cursor: "pointer",
  }),
  dropdownIndicator: (base) => ({
    ...base,
    color: "rgba(0, 0, 0, 0.6)",
    position: "none",
    padding: 0,
    paddingRight: "5px",
  }),

  indicatorSeparator: (base) => ({
    ...base,
    display: "none",
  }),
  singleValue: (base) => ({
    ...base,
    color: "#005CB9",
  }),
  valueContainer: (base) => ({
    ...base,
    width: "130px",
    position: "none",
    padding: "0 10px",
  }),
  menu: (base) => ({
    ...base,
    width: "125px",
    "&:last-child": {
      borderBottom: "none",
    },
  }),
};

const CustomerDetails = () => {
  const [selectedOption, setSelectedOption] = React.useState(null);

  const handleChange = (selectedOption) => {
    setSelectedOption({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };
  const columns = [
    {
      id: "Customer Name",
      header: "Customer Name",
      Cell: () => {
        return (
          <div>
            <Select
              styles={selectStyles}
              value={selectedOption}
              onChange={handleChange}
              placeholder="Enter Value"
              options={options}
            />
          </div>
        );
      },
    },
    {
      id: "Channel",
      header: "Channel",
      Cell: () => {
        return (
          <div>
            <Select
              styles={selectStyles}
              value={selectedOption}
              placeholder="Enter Value"
              onChange={handleChange}
              options={options}
            />
          </div>
        );
      },
    },
    {
      id: "Decision Date",

      header: "Decision Date",
      Cell: () => {
        return (
          <div>
            <Select
              styles={selectStyles}
              value={selectedOption}
              onChange={handleChange}
              placeholder="Enter Value"
              options={options}
            />
          </div>
        );
      },
    },
    {
      id: "SKU Name",
      header: "SKU Name",
      Cell: () => {
        return (
          <div>
            <Select
              styles={selectStyles}
              value={selectedOption}
              placeholder="Enter Value"
              onChange={handleChange}
              options={options}
            />
          </div>
        );
      },
    },
  ];
  return (
    <div className="customer-details-container">
      <div
        style={{
          display: "flex",
          flexDirection: "row-reverse",
          color: "#005CB9",
        }}
      >
        Upload
        <MdFileUpload size={"1.5rem"} style={{ marginRight: "10px" }} />
      </div>
      <div style={{ border: "1px solid #efefef", marginTop: "10px" }}>
        <SharedTable columns={columns} data={[{}, {}, {}, {}]} />
        <span
          style={{
            cursor: "pointer",
            marginLeft: "1rem",
          }}
        >
          + Add New
        </span>
      </div>
    </div>
  );
};

export default CustomerDetails;
