export const PlmMappingData = [
  {
    material_name: "HV ScrtSce 6/12fo",
    material_number: "#Value",
    stat_factor: "#Value",
    case_pack: "#Value",
  },
  {
    material_name: "BHV Scrt Sce SpcyRnch 6/12fo",
    material_number: "#Value",
    stat_factor: "#Value",
    case_pack: "#Value",
  },
  {
    material_name: "HV ScrtSce Smkhs 6/12fo",
    material_number: "#Value",
    stat_factor: "#Value",
    case_pack: "#Value",
  },
  {
    material_name: "HV ScrtSce 6/12fo HNG1",
    material_number: "#Value",
    stat_factor: "#Value",
    case_pack: "#Value",
  },
  {
    material_name: "HV ScrtSce Spcy 6/12fo",
    material_number: "#Value",
    stat_factor: "#Value",
    case_pack: "#Value",
  },
  {
    material_name: "HV ScrtSce Smkhs 6/12fo",
    material_number: "#Value",
    stat_factor: "#Value",
    case_pack: "#Value",
  },
];
