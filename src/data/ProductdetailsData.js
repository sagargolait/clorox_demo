export const projectDetailsData = {
  "Project Name": "Project 1",
  "Project Number": 231176,
  "Project Gear": "Next Generation",
  "Project Status": "In-Development Flight",
  Brand: "SBU 1",
  "LRP Traget SOS": "04/23/2021",
  "Planned SOS": "4/23/2021",
  "Actual SOS": "4/23/2021",
  Volume: "#Value",
  NCS: "#Value",
  "Company Region": "USA",
  "Division/Functionality": "Speciality",
  "Business Unit": "Foods",
  "Project Class": "Multi-type Innovation Growth",
  "Project Manager": "Carrier Clement",
  "Executive Overview":
    "Hidden Valley Ranch 4 skus New line of 3 Ailios and 1 new Secret Sauce flavor",
};
