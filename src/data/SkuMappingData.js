export const SkuMappingData = [
  {
    sku_name: "Sku 1 (Sceret Sauce) - UPC ***",
    stat_factor: "#Value",
    case_pack: "#Value",
    sku_number: "#Value",
  },
  {
    sku_name: "Sku 2 (Spicy Sceret Sauce) - UPC ***",
    stat_factor: "#Value",
    case_pack: "#Value",
    sku_number: "#Value",
  },
  {
    sku_name: "Sku 3 (Smoky Sceret Sauce) - UPC ***",
    stat_factor: "#Value",
    case_pack: "#Value",
    sku_number: "#Value",
  },
];
