export const chartData_ByDrivers = [
  {
    label: "FY21 Mar Fcst",
    uv: "51853",
    pv: "0",
  },
  {
    label: "Category Trends",
    uv: "605",
    pv: "51853",
  },
  {
    label: "New Products",
    uv: "491",
    pv: "52458",
  },
  {
    label: "Competition",
    uv: "472",
    pv: "52949",
  },
  {
    label: "Advertising",
    uv: "304",
    pv: "52253",
  },
  {
    label: "Other Adjustments",
    uv: "219",
    pv: "53472",
  },
  {
    label: "Distribution",
    uv: "194",
    pv: "53666",
  },
  {
    label: "Merchandising",
    uv: "-200",
    pv: "53466",
  },
  {
    label: "Pricing",
    uv: "-299",
    pv: "53167",
  },
  {
    label: "FY21 May Fcst",
    uv: "53639",
    pv: "0",
  },
];
