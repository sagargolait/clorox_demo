import React from "react";
import TopHeader from "../components/TopHeader";
import "./Innovation.css";
import InnovationTopHeader from "../components/InnovationTopHeader";
import InnovationTabPanel from "../components/InnovationTabPanel";

const Innovation = (props) => {
  return (
    <div className="innovation-container">
      <TopHeader pageName={props.location.pathname} />
      <InnovationTopHeader />
      <div className="innvoation-tab-panel-styl">
        <InnovationTabPanel />
      </div>
    </div>
  );
};

export default Innovation;
