import React from "react";
import Details from "../components/Details";
import InnovationTopHeader from "../components/InnovationTopHeader";
import TopHeader from "../components/TopHeader";

const InnovationDetails = (props) => {
  return (
    <div className="innovation-container">
      <TopHeader pageName={props.location.pathname} subPage={"details"} />
      <InnovationTopHeader />
      <div style={{ margin: " 0 20px" }}>
        <Details />
      </div>
    </div>
  );
};

export default InnovationDetails;
