import React, { useState } from "react";
import "./PlanningSummary.css";
import TopHeader from "../components/TopHeader";
import DropdownHeader from "../components/DropdownHeader";
import PlanningSummaryTabPanel from "../components/PlanningSummaryTabPanel";

const PlanningSummary = (props) => {
  const [selectedViewBy, setSelectedViewBy] = useState(null);
  const [selectedCurrentNDF, setSelectedCurrentNDF] = useState(null);
  const [selectedPrior, setSelectedPrior] = useState(null);
  return (
    <div className="planning-summary-container">
      <TopHeader pageName={props.location.pathname} />
      <DropdownHeader
        pageName={props.location.pathname}
        selectedViewBy={selectedViewBy}
        selectedCurrentNDF={selectedCurrentNDF}
        selectedPrior={selectedPrior}
        setSelectedViewBy={setSelectedViewBy}
        setSelectedCurrentNDF={setSelectedCurrentNDF}
        setSelectedPrior={setSelectedPrior}
      />
      <div className="planning-summary-content">
        <PlanningSummaryTabPanel />
      </div>
    </div>
  );
};

export default PlanningSummary;
