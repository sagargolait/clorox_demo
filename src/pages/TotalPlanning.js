import React, { useEffect, useState } from "react";
import "./TotalPlanning.css";
import { MdSave } from "react-icons/md";
import { IoCloudDoneSharp } from "react-icons/io5";
import Table from "../components/Table";
import TopHeader from "../components/TopHeader";
import WaterfallChart from "../components/charts/WaterfallChart";
import DropdownHeader from "../components/DropdownHeader";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import axios from "axios";
import Button from '@material-ui/core/Button';
import TableData from "../contexts/TableData";
import HostingURLContext from "../contexts/HostingURLContext";
import { MdFileDownload, MdCropFree } from "react-icons/md";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import {URL, PORT} from '.././Config'



const TotalPlanning = (props) => {
  let hostingLink = React.useContext(HostingURLContext);
  let [data, setData] = useState([]);
  const [waterfallChartData, setWaterFallChartData] = useState([]);
  let [open, setOpen] = useState(false);
  let [saveClicked, setSaveClicked] = useState(false)
  let filterData = sessionStorage.getItem("IbpStore_allFiltersSelected");
  const [selectedViewBy, setSelectedViewBy] = useState(null);
  const [selectedCurrentNDF, setSelectedCurrentNDF] = useState(null);
  const [selectedPrior, setSelectedPrior] = useState(null);
  const [dropdownData, setDropDownData] = useState(null);
  const [selectedDrillDown, setSelectedDrillDown] = useState(null);
  // const [editedData, setEditedData]= React.useState([])
  const [isFullScreen, setIsFullScreen] = React.useState(false)
  const handle_waterfall = useFullScreenHandle();

  const FullScreenWaterfall=()=>{
    !isFullScreen? handle_waterfall.enter(): handle_waterfall.exit()
    setIsFullScreen(!isFullScreen)
  }

  useEffect(() => {
    axios({
      method: "post",
      url: `${URL}/plan/trend_table`,
      data: sessionStorage["IbpStore_allFiltersSelected"],
      headers: {
        "Access-Control-Allow-Origin": "*",
        // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      },
    })
      .then((response) => {
        // console.log(response.data);
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setData([]);
    };
  }, [filterData]);

  useEffect(() => {
    axios
      .post(
        `${URL}/waterfall`,
        sessionStorage["IbpStore_allFiltersSelected"],
        { headers: { "Access-Control-Allow-Origin": "*" } }
      )
      .then((response) => {
        setWaterFallChartData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      // second, we return an anonymous clean up function
      setWaterFallChartData([]);
    };
  }, [filterData]);

  useEffect(() => {
    axios
      .get(`${URL}/plan/topfilter`)
      .then((resp) => setDropDownData(resp.data))
      .catch((err) => console.log(err));
  }, []);




  let editedData = [];
  //this function is getting called when the SAVE button is clicked
  const onSave = () => {

    setOpen(!open);
      
    
  };

  const handleClose = () => {
    setSaveClicked(false)
    setOpen(false);
  };
  const HandleSaveConfirm=()=>{
    console.log("llllllllllllll")
    const newData = [...data];
    setSaveClicked(!saveClicked)

    newData.forEach(function iter(a) {
      if (a.isEdited) {
        editedData.push(a);
      } else {
      }
      Array.isArray(a.subRows) && a.subRows.forEach(iter);
    });

    console.log(editedData);

    if(editedData.length!=0)
    axios
      .post(
        `${URL}/planning/write_back`,
        {"filterData":JSON.parse(sessionStorage["IbpStore_allFiltersSelected"]),
        "tableData":editedData},
        { headers: { "Access-Control-Allow-Origin": "*" } }
      )
      .then((response) => {
        if(response.data =="success"){
          editedData=[]
          axios({
            method: "post",
            url: `${URL}/plan/trend_table`,
            data: sessionStorage["IbpStore_allFiltersSelected"],
            headers: {
              "Access-Control-Allow-Origin": "*",
              // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            },
          })
            .then((response) => {
              console.log(response.data);
              setData(response.data);
              setSaveClicked(false)
              setOpen(false);
            })
            .catch((error) => {
              console.log(error);
            });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  
  return (
    <TableData.Provider value={{ setData }}>
      <div className="planning-container">
        
        
        <TopHeader pageName={props.location.pathname} />
        <DropdownHeader
          dropdownData={dropdownData}
          pageName={props.location.pathname}
          selectedViewBy={selectedViewBy}
          selectedCurrentNDF={selectedCurrentNDF}
          selectedPrior={selectedPrior}
          setSelectedViewBy={setSelectedViewBy}
          setSelectedCurrentNDF={setSelectedCurrentNDF}
          setSelectedPrior={setSelectedPrior}
          selectedDrillDown={selectedDrillDown}
          setSelectedDrillDown={setSelectedDrillDown}
        />
        <DialogBox open={open} handleClose={handleClose} HandleSaveConfirm={HandleSaveConfirm} saveClicked={saveClicked}/> 
          <div className="table-content">
          <div className="content-header">
            <div className="title-styl">
              <label>
                Volume Decompositon - <b>{JSON.parse(filterData).level}</b>
              </label>
            </div>
            <div
              className="planning-header-btn-styl"
              style={{ display: "flex" }}
            >
              <button style={{ marginRight: "15px" }} onClick={onSave}>
                <MdSave style={{ marginRight: "10px" }} size={"1.1rem"} />
                Save
              </button>
              <button disabled style={{ color: "#dddddd" }}>
                <IoCloudDoneSharp
                  style={{ marginRight: "10px" }}
                  size={"1.2rem"}
                />
                Publish
              </button>
            </div>
          </div>
          {data.length > 0 ? (
            <>
            <Table
              path={props.location.pathname}
              data={data}
              setData={setData}
            />
            </>
          ) : (
            <h3>Loading...</h3>
          )}
        </div>
        
        {props.location.pathname == "/planning/plans" ? (
          <FullScreen handle={handle_waterfall}>
          <div className="chart">
            <div className="header">
              <span>VARIANCE DECOMPOSITION</span>
              <span className="fullscreen-download" style={{ cursor: "pointer" }}>
                  <MdFileDownload
                    style={{ marginRight: "10px" }}
                    color={"#005CB9"}
                  />
                  <MdCropFree color={"#005CB9"} onClick={FullScreenWaterfall}/>
            </span>
            </div>
            
            <WaterfallChart
              data={waterfallChartData}
              aspectRatio={4.0 / 1}
              divideBy={1000}
            />
          </div>
          </FullScreen>
        ) : null}
      </div>
    </TableData.Provider>
  );
};

export const DialogBox = ({open, handleClose, HandleSaveConfirm, saveClicked}) => {
  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        {/* <DialogTitle id="alert-dialog-title">
          {"Use Google's location service?"}
        </DialogTitle> */}
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            DO YOU WANT TO SAVE CHANGES ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={HandleSaveConfirm} color="primary" disabled={saveClicked}>
            {saveClicked ? "Saving..." : "SAVE"}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default TotalPlanning;
