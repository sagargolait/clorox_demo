import React from "react";
import Tippy from "@tippyjs/react";
import styled from "@emotion/styled";

const TooltipWrapper = styled(Tippy)`
  &.tooltip {
    display: inline-flex;
    position: relative;
  }

  .tooltip-container {
    background-color: red;
    color: white;
  }
`;

const Tooltip = (props) => {
  const { children, showTooltip, isDisabled } = props;
  console.log("showToolTip ***", props);
  console.log("isDisabled ***", isDisabled);
  return (
    <TooltipWrapper
      className="tooltip"
      content={<div className="tooltip-container">{props.content}</div>}
      role="tooltip"
      trigger="mouseenter focus"
      zIndex={9999}
      maxWidth="130px"
      placement="top"
    >
      {isDisabled ? <span disabled>{children}</span> : <span>{children}</span>}
    </TooltipWrapper>
  );
};

export default Tooltip;
