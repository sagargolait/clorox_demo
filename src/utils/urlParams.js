import * as qs from "query-string";
import history from "./history";

export const handleURLChange = (obj) => {
  try {
    let urlSearch = qs.parse(window.location.search);
    const objKeys = Object.keys(obj);
    if (objKeys.length > 0) {
      for (let key of objKeys) {
        if (obj[key] !== undefined) {
          urlSearch[key] = obj[key];
        }
      }
    }
    const urlStr = qs.stringify(urlSearch);
    history.replace({
      search: urlStr,
    });
  } catch (e) {
    console.error(e);
  }
};
