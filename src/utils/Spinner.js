import React from "react";
import styled from "styled-components";

const Div = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const Spinner = (props) => {
  let my_style = {
    zIndex: "10",
    // background:"rgba(250,250,250,0.1)",
    transition: "0s",
    textAlign: "center",
    opacity: "0.7",
    color: props.color || "rgba(33,33,33,1)",
  };
  return (
    <Div>
      <div style={my_style}>
        <div className="spinner-border"></div>
        <div style={{}}>{props.msg}</div>
      </div>
    </Div>
  );
};

export default Spinner;
