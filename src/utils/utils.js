export const splitFilters = (allFilters) => {
  //remove all null values keys
  let allFitlersUpd = {};
  Object.keys(allFilters).map((key) => {
    if (allFilters[key]) {
      allFitlersUpd[key] = allFilters[key];
    }
  });

  let timeFilterNames = ["calendarType", "viewType", "year", "selected_list"];
  let fil = {};
  let timeFilter = {};
  Object.keys(allFitlersUpd).map((key) => {
    if (timeFilterNames.includes(key)) {
      timeFilter[key] = allFilters[key];
    } else {
      fil[key] = allFitlersUpd[key];
    }
  });

  return {
    fil,
    timeFilter,
  };
};

export const updateLocalStorage = (key, data, varName) => {
  let updJson;
  if (key) {
    updJson = {
      ...JSON.parse(window.sessionStorage.getItem(varName)),
      [key]: data,
    };
  } else {
    updJson = {
      ...JSON.parse(window.sessionStorage.getItem(varName)),
      ...data,
    };
  }
  window.sessionStorage.setItem(varName, JSON.stringify(updJson));
};

export const applyFilters = (allFilters, setFil, setTimeFilter) => {
  //remove all null values keys
  let allFitlersUpd = {};
  Object.keys(allFilters).map((key) => {
    if (allFilters[key]) {
      allFitlersUpd[key] = allFilters[key];
    }
  });

  let timeFilterNames = ["calendarType", "viewType", "year", "selected_list"];
  let fil = {};
  let timeFilter = {};
  Object.keys(allFitlersUpd).map((key) => {
    if (timeFilterNames.includes(key)) {
      timeFilter[key] = allFilters[key];
    } else {
      fil[key] = allFitlersUpd[key];
    }
  });
  setFil(fil);
  setTimeFilter(timeFilter);
};

export const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const getAllChildren = (
  allFilters,
  currData,
  selectedData,
  type,
  flag
) => {
  currData.map((obj) => {
    if (flag) {
      if (type !== "time") {
        selectedData[obj["labelCat"]] = allFilters[obj["labelCat"]]
          ? allFilters[obj["labelCat"]].concat(obj["label"])
          : [obj["label"]];
        allFilters[obj["labelCat"]] = allFilters[obj["labelCat"]]
          ? allFilters[obj["labelCat"]].concat(obj["label"])
          : [obj["label"]];
      } else {
        console.log(selectedData);
        selectedData[obj["labelCat"]] = allFilters[obj["labelCat"]]
          ? allFilters[obj["labelCat"]].concat(obj["label"])
          : [obj["label"]];
        allFilters[obj["labelCat"]] = allFilters[obj["labelCat"]]
          ? allFilters[obj["labelCat"]].concat(obj["label"])
          : [obj["label"]];
      }
    } else {
      if (type !== "time") {
        selectedData[obj["labelCat"]] =
          allFilters[obj["labelCat"]] !== undefined
            ? allFilters[obj["labelCat"]].filter(
                (item) => item !== obj["label"]
              )
            : allFilters[obj["labelCat"]];
        selectedData[obj["labelCat"]] = selectedData[obj["labelCat"]]
          ? selectedData[obj["labelCat"]].length === 0
            ? undefined
            : selectedData[obj["labelCat"]]
          : undefined;

        allFilters[obj["labelCat"]] =
          allFilters[obj["labelCat"]] !== undefined
            ? allFilters[obj["labelCat"]].filter(
                (item) => item !== obj["label"]
              )
            : allFilters[obj["labelCat"]];
        allFilters[obj["labelCat"]] = allFilters[obj["labelCat"]]
          ? allFilters[obj["labelCat"]].length === 0
            ? undefined
            : allFilters[obj["labelCat"]]
          : undefined;
      } else {
        selectedData[obj["labelCat"]] =
          allFilters[obj["labelCat"]] !== undefined
            ? allFilters[obj["labelCat"]].filter(
                (item) => item !== obj["label"]
              )
            : allFilters[obj["labelCat"]];
        selectedData[obj["labelCat"]] = selectedData[obj["labelCat"]]
          ? selectedData[obj["labelCat"]].length === 0
            ? undefined
            : selectedData[obj["labelCat"]]
          : undefined;

        allFilters[obj["labelCat"]] =
          allFilters[obj["labelCat"]] !== undefined
            ? allFilters[obj["labelCat"]].filter(
                (item) => item !== obj["label"]
              )
            : allFilters[obj["labelCat"]];
        allFilters[obj["labelCat"]] = allFilters[obj["labelCat"]]
          ? allFilters[obj["labelCat"]].length === 0
            ? undefined
            : allFilters[obj["labelCat"]]
          : undefined;
      }
    }
    selectedData = obj.children
      ? getAllChildren(allFilters, obj.children, selectedData, type, flag)
      : selectedData;
  });

  return selectedData;
};

export const capitalize = (s) => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

export const updateFilterData = ({
  filterName,
  filterData,
  key,
  data,
  loading,
  nodesSelected,
  display,
  defaultCheck,
  nodesToggled,
}) => ({
  [filterName]: {
    ...filterData[filterName],
    filters: {
      ...filterData[filterName].filters,
      [key]: {
        ...filterData[filterName].filters[key],
        data: data
          ? updateChildNodes(
              data,
              nodesSelected[filterName],
              null,
              defaultCheck,
              nodesToggled[filterName],
              true
            )
          : null,
        loading,
        display,
      },
    },
  },
});

export const updateChildNodes = (
  data,
  nodesSelected,
  parentChecked,
  defaultCheck,
  nodesToggled,
  parentToggled
) => {
  // console.log(
  //   data,
  //   nodesSelected,
  //   parentChecked,
  //   defaultCheck,
  //   nodesToggled,
  //   parentToggled
  // );
  nodesToggled = [
    { label: "All", checked: false, children: undefined, expanded: true },
    ...nodesToggled,
  ];
  // nodesToggled=[{label:"All",checked:false,children:undefined,expanded:true}]
  data = data.map((obj) => ({
    ...obj,
    checked: parentChecked
      ? true
      : populateChecked(obj.level_id, nodesSelected, obj.checked, defaultCheck),
    expanded: parentToggled
      ? nodesToggled.map((obj) => obj.level_id).includes(obj.level_id)
      : false,
    children: obj.children
      ? updateChildNodes(
          obj.children,
          nodesSelected,
          parentChecked
            ? true
            : populateChecked(obj.level_id, nodesSelected, obj.checked)
            ? true
            : null,
          defaultCheck,
          nodesToggled,
          parentToggled
            ? nodesToggled.map((obj) => obj.level_id).includes(obj.level_id)
            : false
        )
      : null,
  }));
  return data;
};

export const populateChecked = (
  level_id,
  nodesSelected,
  obj_checked,
  defaultCheck
) => {
  let checked = false;

  // if no nodes are selected that means All is selected (set using defaultCheck parameter)
  //     if(!nodesSelected.length){
  //         checked = obj_checked
  //  }

  //checking if label is present in our nodesSelected list
  if (nodesSelected !== undefined) {
    nodesSelected.forEach((node) => {
      if (node.level_id === level_id) {
        checked = true;
      }
    });
  }
  return checked;
};
