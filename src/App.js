import { useState } from "react";
import Homepage from "./components/Homepage";
import { Router, Switch, Route, Link } from "react-router-dom";
import history from "./utils/history";
import NavigationBar from "./components/NavigationBar";
import TotalPlanning from "./pages/TotalPlanning";
import Innovation from "./pages/Innovation";
import Page404 from "./pages/Page404";
import { splitFilters, updateLocalStorage } from "./utils/utils";

//Import Contexts
import UserFilters from "./contexts/UserFilters";
import TimeFilter from "./contexts/TimeFilter";
import HostingURLContext from "./contexts/HostingURLContext"

//Import Initial Data
import initialNodesSelected from "./components/Filter/data/initialNodesSelected.json";
import initialNodesToggled from "./components/Filter/data/initialNodesToggled.json";
import InnovationDetails from "./pages/InnovationDetails";
import PlanningSummary from "./pages/PlanningSummary";
import AmCharts from "./sharedComponents/AmCharts";
import ConsumptionAndShipmentChart from "./components/charts/ConsumptionAndShipmentChart";

const NavRoute = ({ exact, path, component: Component }) => (
  <Route
    exact={exact}
    path={path}
    render={(props) => (
      <div>
        <NavigationBar />
        <Component {...props} />
      </div>
    )}
  />
);

function App() {
  const [defaultvalue, setDefaultValue] = useState([
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
  ]);
  const [selectedQuarter, setSelectedQuarter] = useState([]);
  const [year, setYear] = useState([]);

  let allFilters =
    window.sessionStorage.getItem("IbpStore_allFiltersSelected") !== null
      ? JSON.parse(sessionStorage.IbpStore_allFiltersSelected)
      : undefined;
  let nodesSelectedFromStorage = JSON.parse(
    window.sessionStorage.getItem("IbpStore_nodesSelected")
  );
  let nodesToggledFromStorage = JSON.parse(
    window.sessionStorage.getItem("IbpStore_nodesToggled")
  );
  let initialTimeFilter;
  let initialFil;

  //Split filters
  if (allFilters) {
    let splittedFilters = splitFilters(allFilters);
    initialFil = splittedFilters["fil"];
    initialTimeFilter = splittedFilters["timeFilter"];
  } else {
    initialTimeFilter = {
      calendarType: "FY",
      viewType: "Monthly",
      year: year,
      selected_quarter: selectedQuarter,
      selected_list: defaultvalue,
    };

    initialFil = {
      drilldown: "",
      Account:
        window.sessionStorage.getItem("account") !== null
          ? [window.sessionStorage.getItem("account")]
          : ["Walmart"],
      unit_of_measure: "Stat Case",
      display_unit: "MSC",
      GBC_NAME: ["Food", "Charcoal Products", "SP-Single Division"],
      DIVISION_NAME: ["Food Products"],
      view_by: "MFF",
      currentndf: "FY22 BUDGET #2",
      base: "FY21 May/June NDF",
      level: "Drivers",
      customerType: "account",
      BRAND_NAME:
        window.sessionStorage.getItem("brand") !== null
          ? [window.sessionStorage.getItem("brand")]
          : undefined,
      MFF_NAME: undefined,
      EAN_UPC: undefined,
      SUB_MFF_NAME: undefined,
      PURPOSE_NAME: undefined,
      TYPE_NAME: undefined,
      PACK_SIZE_NAME: undefined,
      EAN_UPC_Index: undefined,
      EAN_DESC: undefined,
    };
  }

  const [timeFilter, setTimeFilter] = useState(initialTimeFilter);
  const [fil, setFil] = useState(initialFil);

  updateLocalStorage(
    null,
    { ...fil, ...timeFilter },
    "IbpStore_allFiltersSelected"
  );
  updateLocalStorage(
    null,
    { ...fil, ...timeFilter },
    "IbpStore_FiltersSelected"
  );

  updateLocalStorage(
    null,
    nodesSelectedFromStorage || {
      ...initialNodesSelected,
      ...{
        time: [
          {
            checked: true,
            disabled: false,
            expanded: false,
            label: "Q1",
            labelCat: "selected_quarter",
            level_id: "Q1",
          },
          {
            checked: true,
            disabled: false,
            expanded: false,
            label: "Q2",
            labelCat: "selected_quarter",
            level_id: "Q2",
          },
          {
            checked: true,
            disabled: false,
            expanded: false,
            label: "Q3",
            labelCat: "selected_quarter",
            level_id: "Q3",
          },
          {
            checked: true,
            disabled: false,
            expanded: false,
            label: "Q4",
            labelCat: "selected_quarter",
            level_id: "Q4",
          },
        ],
      },
    },
    "IbpStore_nodesSelected"
  );

  updateLocalStorage(
    null,
    nodesToggledFromStorage || initialNodesToggled,
    "IbpStore_nodesToggled"
  );

  return (
    <>
        <UserFilters.Provider value={{ fil, setFil }}>
          <TimeFilter.Provider value={{ timeFilter, setTimeFilter }}>
            <Router history={history}>
              <Switch>
                <Route exact path="/" component={Homepage} />
                <NavRoute
                  exact
                  component={TotalPlanning}
                  path="/planning/plans"
                />
                <NavRoute
                  exact
                  component={TotalPlanning}
                  path="/planning/opportunity_risk"
                />
                <NavRoute
                  exact
                  component={TotalPlanning}
                  path="/planning/upside_downside"
                />

                <NavRoute exact component={Innovation} path="/innovation" />

                <NavRoute
                  exact
                  component={PlanningSummary}
                  path="/planning/summary"
                />

                <NavRoute
                  exact
                  component={Innovation}
                  path="/innovation/allprojects"
                />
                <NavRoute
                  exact
                  component={InnovationDetails}
                  path="/innovation/allprojects/:id"
                />
                <NavRoute
                  exact
                  component={ConsumptionAndShipmentChart}
                  path="/chart"
                />
                <NavRoute component={Page404} />
              </Switch>
            </Router>
          </TimeFilter.Provider>
        </UserFilters.Provider>
    </>
  );
}

export default App;
